<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
		@import url('https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900');
		*{
			font-family: 'Raleway', sans-serif;
		}

        .card{
            width: 98%;
            margin: 44px auto;
            background: #EEEEEE;
            border: 1px solid #565656;
            border-radius: 15px;
        }

        .card-header{
            background: #565656;
            font-weight: bold;
            padding: 20px;
            color: #ffffff;
            border-radius: 15px 15px 0px 0px;
            font-size: 16px;
            text-align: center;
            text-transform: uppercase;
        }

        .card-body{
            padding: 40px 30px;
            background: #fff;
            height: 100%;
            border-radius: 0px 0px 15px 15px;
        }
		.card-body h2{
            text-align: center;
			color: #565656;
			margin-top: 0px;
			margin-bottom: 0px;
        }
		.card-body p{
			margin-bottom: 40px;
            margin-top: 10px;
            font-size: 16px;
        }
        .card-body .subject,
        .card-body .name,
        .card-body .email,
        .card-body .message{
			text-align: center;
		}
		.card-body h4, .card-body h6 {
			color: #666;
			margin-bottom: 10px;
		}
		.card-body h3, .card-body h6 {
			margin: 0px;
		}
        .card-body .subject .subject-div,
        .card-body .name .name-div,
        .card-body .email .email-div,
        .card-body .message .message-div{
			background: #eeefff;
			padding: 15px;
        }
        .card-body .message .message-div{
            text-align: justify;
        }
        .btn{
            text-align: center;
            background-color: #01a595;
            padding: 10px 20px;
            border-radius: 5px;
            font-size: 16px;
            font-weight: 900;
            color: white !important;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <section class="email-template">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Login Information
                        </div>
                        <div class="card-body">
                            {{-- <div class="name">
								<h4>Name</h4>
								<div class="name-div">
									<h3>{{ $name }}</h3>
								</div>
							</div> --}}
                            <div class="email">
								<h4>Email Address</h4>
								<div class="email-div">
									<h3>{{ $email }}</h3>
								</div>
							</div>
                            <div class="email">
								<h4>Password</h4>
								<div class="email-div">
									<h3>{{ $password }}</h3>
								</div>
							</div>
                            <div class="message">
								<div class="message-div" style="text-align: center;">
									<a href="{{ route('login') }}" class="btn">LOGIN NOW</a>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>
</html>
