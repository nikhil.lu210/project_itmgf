<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    {{-- Dashboard --}}
                    <li class="{{ Request::is('editor/dashboard*') ? 'nav-active' : '' }}">
                        <a href="{{ route('editor.dashboard.index') }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    {{-- Tournaments --}}
                    <li class="nav-parent {{ Request::is('editor/tournaments/*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fab fa-battle-net" aria-hidden="true"></i>
                            <span>Tournaments</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('editor/tournaments/ongoing*') ? 'nav-active' : '' }}">
                                <a href="{{ route('editor.tournament.ongoing.index') }}">Ongoing Tournaments</a>
                            </li>
                            <li class="{{ Request::is('editor/tournaments/upcoming*') ? 'nav-active' : '' }}">
                                <a href="{{ route('editor.tournament.upcoming.index') }}">Upcoming Tournaments</a>
                            </li>
                            <li class="{{ Request::is('editor/tournaments/completed*') ? 'nav-active' : '' }}">
                                <a href="{{ route('editor.tournament.completed.index') }}">Completed Tournaments</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>

    </div>

</aside>
<!-- end: sidebar -->
