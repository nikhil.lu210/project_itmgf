<script src="{{ asset('custom/assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/nanoscroller/nanoscroller.js') }}"></script>

{{-- Font Awesome 5 --}}
<script src="https://kit.fontawesome.com/b9bdb37ea9.js"></script>

@yield('script_links')

{{-- <!-- Theme Base, Components and Settings --> --}}
<script src="{{ asset('custom/assets/javascripts/theme.js') }}"></script>

{{-- <!-- Theme Custom --> --}}
<script src="{{ asset('custom/assets/javascripts/theme.custom.js') }}"></script>

{{-- <!-- Theme Initialization Files --> --}}
<script src="{{ asset('custom/assets/javascripts/theme.init.js') }}"></script>

{{-- <!-- custom js --> --}}
<script src="{{ asset('custom/js/script.js') }}"></script>
<script src="{{ asset('custom/js/responsive.js') }}"></script>

@yield('scripts')
