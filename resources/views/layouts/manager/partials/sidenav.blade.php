<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                Navigation
            </div>
            <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">
                    <ul class="nav nav-main">
                        {{-- Dashboard --}}
                        <li class="{{ Request::is('manager/dashboard*') ? 'nav-active' : '' }}">
                            <a href="{{ route('manager.dashboard.index') }}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        {{-- Tournaments --}}
                        <li class="nav-parent {{ Request::is('manager/tournaments/*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fab fa-battle-net" aria-hidden="true"></i>
                                <span>Tournaments</span>
                            </a>
                            <ul class="nav nav-children">
                                <li class="{{ Request::is('manager/tournaments/all*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.tournament.all.index') }}">All Tournaments</a>
                                </li>
                                <li class="{{ Request::is('manager/tournaments/ongoing*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.tournament.ongoing.index') }}">Ongoing Tournaments</a>
                                </li>
                                <li class="{{ Request::is('manager/tournaments/create') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.tournament.create') }}">Create New Tournament</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Editors --}}
                        <li class="nav-parent {{ Request::is('manager/editor/*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fas fa-chalkboard-teacher" aria-hidden="true"></i>
                                <span>Editors</span>
                            </a>
                            <ul class="nav nav-children">
                                <li class="{{ Request::is('manager/editor/all*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.editor.index') }}">All Editors</a>
                                </li>
                                <li class="{{ Request::is('manager/editor/create') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.editor.create') }}">Assign New Editor</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Players --}}
                        <li class="nav-parent {{ Request::is('manager/player/*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fas fa-child" aria-hidden="true"></i>
                                <span>Players</span>
                            </a>
                            <ul class="nav nav-children">
                                <li class="{{ Request::is('manager/player/non_approved_player*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.player.need_approval') }}">Non-Approved Players</a>
                                </li>
                                <li class="{{ Request::is('manager/player/approved_players*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('manager.player.index') }}">All Approved Players</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>

            </div>

        </div>

    </aside>
    <!-- end: sidebar -->
