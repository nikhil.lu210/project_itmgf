<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    {{-- Dashboard --}}
                    <li class="{{ Request::is('superadmin/dashboard*') ? 'nav-active' : '' }}">
                        <a href="{{ route('superadmin.dashboard.index') }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    {{-- Tournaments --}}
                    <li class="{{ Request::is('superadmin/tournament*') ? 'nav-active' : '' }}">
                        <a href="{{ route('superadmin.tournament.index') }}">
                            <i class="fab fa-battle-net" aria-hidden="true"></i>
                            <span>All Tournaments</span>
                        </a>
                    </li>

                    {{-- Palyers --}}
                    <li class="{{ Request::is('superadmin/player*') ? 'nav-active' : '' }}">
                        <a href="{{ route('superadmin.player.index') }}">
                            <i class="fas fa-users" aria-hidden="true"></i>
                            <span>All Players</span>
                        </a>
                    </li>

                    {{-- Administration --}}
                    <li class="nav-parent {{ Request::is('superadmin/administration/*') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fas fa-user-shield" aria-hidden="true"></i>
                            <span>Administration</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ Request::is('superadmin/administration/superadmin*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.administration.superadmin.index') }}">All Super Admins</a>
                            </li>
                            <li class="{{ Request::is('superadmin/administration/manager*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.administration.manager.index') }}">All Managers</a>
                            </li>
                            <li class="{{ Request::is('superadmin/administration/editor*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.administration.editor.index') }}">All Editors</a>
                            </li>
                            <li class="{{ Request::is('superadmin/administration/create*') ? 'nav-active' : '' }}">
                                <a href="{{ route('superadmin.administration.create') }}">Assign New Member</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>

    </div>

</aside>
<!-- end: sidebar -->
