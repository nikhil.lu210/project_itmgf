<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="Veechi Technologies" />
		<meta name="description" content="Veechi Technologies">
		<meta name="author" content="Veechi Technologies">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap/css/bootstrap.css') }}" />
		<link rel="stylesheet" href="{{ asset('custom/assets/vendor/font-awesome/css/font-awesome.css') }}" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('custom/assets/stylesheets/theme.css') }}" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('custom/assets/stylesheets/skins/default.css') }}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('custom/assets/stylesheets/theme-custom.c') }}ss">

		<!-- Head Libs -->
		<script src="{{ asset('custom/assets/vendor/modernizr/modernizr.js') }}"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<!-- <img src="assets/images/logo.png" height="54" alt="Porto Admin" /> -->
					<h2>ITMGF</h2>
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>
					<div class="panel-body">
						<form method="POST" action="{{ route('register') }}">
                        @csrf
							<div class="form-group">
								<label>First Name</label>
								<input name="first_name" type="text" class="form-control input-lg {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name *" value="{{ old('first_name') }}" required autofocus/>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
							</div>
							
							<div class="form-group">
								<label>Last Name</label>
								<input name="last_name" type="text" class="form-control input-lg {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Last Name *" value="{{ old('last_name') }}" required />

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
							</div>

							{{-- <div class="form-group">
								<label>Username</label>
								<input name="username" type="text" class="form-control input-lg {{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Username *" value="{{ old('username') }}" required />

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
							</div> --}}

							<div class="form-group">
								<label>E-mail Address</label>
								<input name="email" type="email" class="form-control input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email *" value="{{ old('email') }}" required />

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<label>Password</label>
										<input type="password" class="form-control input-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password *" required/>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
									<div class="col-sm-6">
										<label>Password Confirmation</label>
										<input type="password" class="form-control input-lg" name="password_confirmation" placeholder="Confirm Password *" required/>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="AgreeTerms" name="agreeterms" type="checkbox"/>
										<label for="AgreeTerms">I agree with <a href="#">terms of use</a></label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Sign Up</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign Up</button>
								</div>
							</div>

							<span class="mt-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							<p class="text-center">Already have an account? <a href="{{ route('login') }}">Sign In!</a>

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2019. All rights reserved. Developed by <a target="_blank" href="https://veechitechnologies.com">VEECHI TECHNOLOGIES</a></p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="{{ asset('custom/assets/vendor/jquery/jquery.js') }}"></script>
		<script src="{{ asset('custom/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
		<script src="{{ asset('custom/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
		<script src="{{ asset('custom/assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('custom/assets/javascripts/theme.js') }}"></script>
		
		<!-- Theme Custom -->
		<script src="{{ asset('custom/assets/javascripts/theme.custom.js') }}"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{ asset('custom/assets/javascripts/theme.init.js') }}"></script>

	</body>
</html>