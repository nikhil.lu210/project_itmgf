@extends('layouts.superadmin.app')

@section('page_title', '| Super Admins | Details')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ $superadmin->first_name." ".$superadmin->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Administrations</span>
            </li>

            <li>
                <a href="{{ route('superadmin.administration.superadmin.index') }}">
                    <span>All Super Admins</span>
                </a>
            </li>

            <li>
                <span>{{ $superadmin->first_name." ".$superadmin->last_name }}</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" class="rounded img-responsive" alt="Image Not Found">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ $superadmin->first_name }}</span>
                        <span class="thumb-info-type">{{ App\Models\Role\Role::find($superadmin->role_id)->name }}</span>
                    </div>
                </div>

                @if ($superadmin->id == Auth::user()->id)
                    <div class="widget-toggle-expand mb-md">
                        <div class="widget-content-expanded">
                            <ul class="simple-todo-list">
                                {{-- <li><a class="text-muted" href="{{ route('superadmin.profile.show') }}">Update Profile</a></li> --}}
                                <li><a class="text-muted" href="{{ route('superadmin.profile.password') }}">Update Password</a></li>
                            </ul>
                        </div>
                    </div>
                @endif

            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Profile Details</h2>
            </header>

            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Full Name</th>
                        <td>{{ $superadmin->first_name." ".$superadmin->last_name }}</td>
                    </tr>

                    @if ($superadmin->mobile)
                        <tr>
                            <th>Mobile Number</th>
                            <td>{{ $superadmin->mobile }}</td>
                        </tr>
                    @endif

                    <tr>
                        <th>Email Address</th>
                        <td>{{ $superadmin->email }}</td>
                    </tr>

                    @if ($superadmin->id == Auth::user()->id)
                        <tr>
                            <th>Username</th>
                            <td>{{ $superadmin->username }}</td>
                        </tr>
                    @endif

                    <tr>
                        <th>Assigned Date</th>
                        <td>{{ $superadmin->created_at->format('d M Y') }}</td>
                    </tr>
                </table>
            </div>

            {{-- <div class="panel-footer text-right">
                <a href="#" class="mb-xs mt-xs mr-xs btn btn-success">Complete Order</a>
            </div> --}}
        </section>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
