@extends('layouts.superadmin.app')

@section('page_title', '| Tournaments | Editor | Create')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Assign New Administration Member</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Administrations</span>
            </li>

            <li>
                <span>Assign New Member</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12"><h2 class="panel-title">Create An Administration Member</h2></div>
                </div>

            </div>

            <form method="POST" action="{{ route('superadmin.administration.store') }}">
            @csrf
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-12">
                            @if (session('message'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">First Name <span>*</span></label>
                                <input class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name *" value="{{ old('first_name') }}" required autofocus name="first_name" type="text" autocomplete="off">

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Last Name / Surname / Family Name <span>*</span></label>
                                <input name="last_name" type="text" autocomplete="off" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Last Name *" value="{{ old('last_name') }}" required />

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="first_name">Role <span>*</span></label>
                                <select name="role" id="role" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" required>
                                    <option value="">Select Role</option>
                                    <option value="1">Super Admin</option>
                                    <option value="2">Manager</option>
                                    <option value="3">Editor</option>
                                </select>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="col-md-4">
                            <div class="form-group">
								<label>Username <span>*</span></label>
								<input name="username" type="text" autocomplete="off" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Username *" value="{{ old('username') }}" required />

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
							</div>
                        </div> --}}

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">Email <span>*</span></label>
                                <input name="email" type="email" autocomplete="off" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email *" value="{{ old('email') }}" required />

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="mobile">Mobile Number <span>*</span></label>
                                <input name="mobile" type="text" autocomplete="off" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" placeholder="mobile *" value="{{ old('mobile') }}" required />

                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Password <span>*</span></label>
                                <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password *" required/>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Password Confirmation <span>*</span></label>
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password *" required/>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-primary" type="submit">Assign Now</button>
                </div>
            </form>
        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
