@extends('layouts.superadmin.app')

@section('page_title', '| Editors')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .dataTables_wrapper .DTTT.btn-group{
        display: none !important;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>All Editors</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>All Editors</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Editors</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Full Name</th>
                    <th>Mobile Number</th>
                    <th>Email Address</th>
                    <th>Assigned Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($editors as $key => $editor)
                <tr>
                    <td>
                        @if ($key+1 < 10)
                            <b>0{{ $key+1 }}</b>
                        @else
                            <b>{{ $key+1 }}</b>
                        @endif
                    </td>
                    <td>{{ $editor->first_name." ".$editor->last_name }}</td>
                    <td>{{ $editor->mobile }}</td>
                    <td>{{ $editor->email }}</td>
                    <td>{{ $editor->created_at->format('d M Y') }}</td>
                    <td class="action-td">
                        <a href="{{ route('superadmin.administration.editor.destroy', ['id' => $editor->id]) }}" class="btn btn-default btn-round-custom" onclick="return confirm('Are Your Sure Want To Delete This Super Admin..?');"><i class="far fa-trash-alt"></i></a>

                        <a href="{{ route('superadmin.administration.editor.show', ['id' => $editor->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
