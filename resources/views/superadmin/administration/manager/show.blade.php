@extends('layouts.superadmin.app')

@section('page_title', '| Managers | Details')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ $manager->first_name." ".$manager->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Administrations</span>
            </li>

            <li>
                <a href="{{ route('superadmin.administration.manager.index') }}">
                    <span>All Managers</span>
                </a>
            </li>

            <li>
                <span>{{ $manager->first_name." ".$manager->last_name }}</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" class="rounded img-responsive" alt="Image Not Found">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ $manager->first_name }}</span>
                        <span class="thumb-info-type">{{ App\Models\Role\Role::find($manager->role_id)->name }}</span>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Profile Details</h2>
            </header>

            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Full Name</th>
                        <td>{{ $manager->first_name." ".$manager->last_name }}</td>
                    </tr>

                    @if ($manager->mobile)
                        <tr>
                            <th>Mobile Number</th>
                            <td>{{ $manager->mobile }}</td>
                        </tr>
                    @endif

                    <tr>
                        <th>Email Address</th>
                        <td>{{ $manager->email }}</td>
                    </tr>

                    @if ($manager->id == Auth::user()->id)
                        <tr>
                            <th>Username</th>
                            <td>{{ $manager->username }}</td>
                        </tr>
                    @endif

                    <tr>
                        <th>Assigned Date</th>
                        <td>{{ $manager->created_at->format('d M Y') }}</td>
                    </tr>
                </table>
            </div>
        </section>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
