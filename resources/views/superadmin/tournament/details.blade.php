@extends('layouts.superadmin.app')

@section('page_title', '| Tournaments | tournament_name | More Details')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ $tournament->name }} Details</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <a href="{{ route('manager.tournament.all.index') }}">
                    <span>All Tournaments</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <span>tournament_name</span>
                </a>
            </li>

            <li>
                <span>Details</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{ $tournament->name }}</h2>
        <a href="{{ route('graph', ['id'=> $tournament->id]) }}" class="btn btn-dark btn-sm btn-graph">View Graph</a>
    </header>

    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none">
            <tr>
                <th>Player Number</th>
                <td>
                    {{ $tournament->player_number }}
                </td>
            </tr>

            <tr>
                <th>Minimum Age</th>
                <td>
                    {{ $tournament->min_age }}
                </td>
            </tr>

            <tr>
                <th>Maximum Age</th>
                <td>
                    {{ $tournament->max_age }}
                </td>
            </tr>

            <tr>
                <th>Gender</th>
                <td>
                    {{ $tournament->gender }}
                </td>
            </tr>

            <tr>
                <th>Total Round</th>
                <td>
                    {{ $tournament->max_round }}
                </td>
            </tr>

            <tr>
                <th>Created Manager</th>
                <td>
                    <ol>
                        <li>{{ $tournament->manager->first_name." ".$tournament->manager->last_name }}</li>
                        <li>{{ $tournament->manager->email }}</li>
                        @if ($tournament->manager->mobile)
                            <li>{{ $tournament->manager->mobile }}</li>
                        @endif
                    </ol>
                </td>
            </tr>

            <tr>
                <th>Player Lists</th>
                <td>
                    @if(!isset($tournament->assignedPlayers[0]))
                        No Player selected
                    @endif
                    <ol>
                        @foreach($tournament->assignedPlayers as $player)
                        <li>{{ $player->assignedPlayer->first_name." ".$player->assignedPlayer->last_name }} ({{ $player->assignedPlayer->player->age }})</li>
                        @endforeach
                    </ol>
                </td>
            </tr>

            <tr>
                <th>Editor Lists</th>
                <td>
                    @if(!isset($tournament->assignedEditors[0]))
                        No Editor selected
                    @endif
                    <ol>
                        @foreach($tournament->assignedEditors as $editor)
                        <li>{{ $editor->editor->first_name." ".$editor->editor->last_name }}</li>
                        @endforeach
                    </ol>
                </td>
            </tr>
        </table>
    </div>

    {{-- <div class="panel-footer text-right">
        <a href="#" class="mb-xs mt-xs mr-xs btn btn-success">Complete Order</a>
    </div> --}}
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
