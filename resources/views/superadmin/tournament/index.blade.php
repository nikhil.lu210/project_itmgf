@extends('layouts.superadmin.app')

@section('page_title', '| Tournaments')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .b-top{
            border-top: 1px solid #dddddda1;
            padding-top: 10px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>All Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <span>All Tournaments</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="tab-content">
            <div id="overview" class="tab-pane active">
                @if (session('message'))
                    <div class="alert alert-success" role="alert">
                        {{ session('message') }}
                    </div>
                @endif

                <h4 class="mb-xlg">All Tournaments</h4>

                <div class="timeline timeline-simple mt-xlg mb-md">
                    @foreach ($tournaments as $date => $tournament)
                        <div class="tm-body">
                            <div class="tm-title">
                                <h3 class="h5 text-uppercase">{{ $date }}</h3>
                            </div>
                            @foreach ($tournament as $data)
                                <ol class="tm-items">
                                    <li>
                                        <div class="tm-box">
                                            <h4 class="mb-none title">
                                                <b>
                                                    {{ $data->name }}
                                                    ( @if($data->status == -1 && $data->starting_date <= date('Y-m-d'))Ongoing
                                                    @elseif($data->status == -1 && $data->starting_date > date('Y-m-d'))Upcomming
                                                    @elseif($data->status == 1) Completed
                                                    @endif )
                                                </b>
                                            </h4>
                                            <?php
                                                $dateTime = new DateTime($data->starting_date);
                                            ?>
                                            <p class="text-muted mb-none date">Date: {{ $dateTime->format('d M Y') }}</p>
                                            <hr>
                                            <p>{{ $data->note }}</p>
                                            <a href="{{ route('superadmin.tournament.show', ['id' => $data->id]) }}" class="btn btn-primary btn-xs">Details</a>
                                        </div>
                                    </li>
                                </ol>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
