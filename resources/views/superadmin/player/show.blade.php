@extends('layouts.superadmin.app')

@section('page_title', '| Players | Details')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ $player->first_name." ".$player->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Players</span>
            </li>

            <li>
                <a href="{{ route('superadmin.player.index') }}">
                    <span>All Players</span>
                </a>
            </li>

            <li>
                <span>{{ $player->first_name." ".$player->last_name }}</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" class="rounded img-responsive" alt="Image Not Found">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ $player->first_name }}</span>
                        <span class="thumb-info-type">{{ App\Models\Role\Role::find($player->role_id)->name }}</span>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Profile Details</h2>
            </header>

            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Full Name</th>
                        <td>{{ $player->first_name." ".$player->last_name }}</td>
                    </tr>

                    <tr>
                        <th>Age</th>
                        <td>{{ $player->player->age }}</td>
                    </tr>

                    <tr>
                        <th>Gender</th>
                        <td>{{ $player->player->gender }}</td>
                    </tr>

                    <tr>
                        <th>Mobile</th>
                        <td>{{ $player->player->mobile }}</td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td>{{ $player->player->title }}</td>
                    </tr>
                    <tr>
                        <th>Airlines</th>
                        <td>{{ $player->player->airlines }}</td>
                    </tr>
                    <tr>
                        <th>Flight Number</th>
                        <td>{{ $player->player->flight_number }}</td>
                    </tr>
                    <tr>
                        <th>Arrival Date</th>
                        <td>{{ $player->player->arrival_date }}</td>
                    </tr>
                    <tr>
                        <th>Arrival Time</th>
                        <td>{{ $player->player->arrival_time }}</td>
                    </tr>
                    <tr>
                        <th>Departure Date</th>
                        <td>{{ $player->player->departure_date }}</td>
                    </tr>
                </table>
            </div>
        </section>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
