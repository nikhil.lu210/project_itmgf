@extends('layouts.superadmin.app')

@section('page_title', '| Profile')

@section('stylesheet_links')
{{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ $profile->first_name." ".$profile->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('superadmin.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Profile</span></li>
            <li><span>{{ $profile->first_name." ".$profile->last_name }}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" class="rounded img-responsive" alt="Image Not Found">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ $profile->first_name }}</span>
                        <span class="thumb-info-type">{{ App\Models\Role\Role::find($profile->role_id)->name }}</span>
                    </div>
                </div>

                <div class="widget-toggle-expand mb-md">
                    <div class="widget-content-expanded">
                        <ul class="simple-todo-list">
                            {{-- <li><a class="text-muted" href="{{ route('superadmin.profile.show') }}">Update Profile</a></li> --}}
                            <li><a class="text-muted" href="{{ route('superadmin.profile.password') }}">Update Password</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Profile Details</h2>
            </header>

            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Full Name</th>
                        <td>{{ $profile->first_name." ".$profile->last_name }}</td>
                    </tr>

                    <tr>
                        <th>Username</th>
                        <td>{{ $profile->username }}</td>
                    </tr>

                    <tr>
                        <th>Mobile Number</th>
                        <td>{{ $profile->mobile }}</td>
                    </tr>

                    <tr>
                        <th>Email Address</th>
                        <td>{{ $profile->email }}</td>
                    </tr>

                    <tr>
                        <th>Assigned From</th>
                        <td>{{ $profile->created_at->format('d M Y') }} <sup>at</sup> {{ $profile->created_at->format('g:i A') }}</td>
                    </tr>
                </table>
            </div>

            {{-- <div class="panel-footer text-right">
                <a href="#" class="mb-xs mt-xs mr-xs btn btn-success">Complete Order</a>
            </div> --}}
        </section>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
