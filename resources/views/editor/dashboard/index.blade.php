@extends('layouts.editor.app')

@section('page_title', '| Dashboard')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Dashboard</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fab fa-battle-net"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>All Tournaments</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $all }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-primary">*Tournaments I Was Editor </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-success">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-success">
                            <i class="fas fa-award"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>Completed Tournaments</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $completed }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-success">*Tournaments I Have Completed</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-danger">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-danger">
                            <i class="fas fa-bezier-curve"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>Upcoming Tournaments</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $upcoming }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-danger">*Tournaments I Have to Handle</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-6 col-xl-3">
        <section class="panel panel-featured-left panel-featured-warning">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-warning">
                            <i class="fab fa-angellist"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title"><b>Ongoing Tournaments</b></h4>
                            <div class="info">
                                <strong class="amount">{{ $ongoing }}</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <span class="text-warning">*Ongoing Tournaments's I'm Handling</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
