@extends('layouts.editor.app')

@section('page_title', '| Profile | Update')

@section('stylesheet_links')
{{--  External CSS  --}}
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Profile</span></li>
            <li><span>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</span></li>
            <li><span>Update</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-12 col-lg-12">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-4"><h2 class="panel-title">Profile Update</h2></div>
                </div>

            </div>

            <form action="#" method="post">
                @csrf
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Profile Picture</label>
                                <div class="col-md-12">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="fa fa-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-exists">Change</span>
                                                <span class="fileupload-new">Select file</span>
                                                <input type="file" name="avatar"/>
                                            </span>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input class="form-control" id="first_name" name="first_name" type="text" autocomplete="off" placeholder="First Name">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Last Name / Surname / Family Name</label>
                                <input class="form-control" id="last_name" name="last_name" type="text" autocomplete="off" placeholder="Last Name">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="b_date">Birthdate</label>
                                <input type="text" autocomplete="off" data-plugin-datepicker class="form-control" name="b_date"  placeholder="mm/dd/yyyy">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input type="text" autocomplete="off" class="form-control" name="country" placeholder="Country">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="form-control mb-md" name="gender">
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="jhondoe@mail.com">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Mobile Number</label>
                                <input type="text" autocomplete="off" class="form-control" name="number" placeholder="65464989441313">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-primary" type="submit">Update Profile</button>
                </div>
            </form>
        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}
<script src="{{ asset('custom/assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
