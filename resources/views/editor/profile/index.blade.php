@extends('layouts.editor.app')

@section('page_title', '| Profile')

@section('stylesheet_links')
{{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Profile</span></li>
            <li><span>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}" class="rounded img-responsive" alt="Image Not Found">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ Auth::user()->first_name }}</span>
                        <span class="thumb-info-type">{{ App\Models\Role\Role::find(Auth::user()->role_id)->name }}</span>
                    </div>
                </div>

                <div class="widget-toggle-expand mb-md">
                    <div class="widget-content-expanded">
                        <ul class="simple-todo-list">
                            {{-- <li><a class="text-muted" href="{{ route('editor.profile.show') }}">Update Profile</a></li> --}}
                            <li><a class="text-muted" href="{{ route('editor.profile.password') }}">Update Password</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Profile Details</h2>
            </header>

            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <tr>
                        <th>Full Name</th>
                        <td>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</td>
                    </tr>

                    <tr>
                        <th>Birthdate</th>
                        <td>birth_date</td>
                    </tr>

                    <tr>
                        <th>Age</th>
                        <td>manager_age</td>
                    </tr>

                    <tr>
                        <th>Gender</th>
                        <td>manger_gender</td>
                    </tr>

                    <tr>
                        <th>Mobile Number</th>
                        <td>mobile_number</td>
                    </tr>

                    <tr>
                        <th>Email Address</th>
                        <td>email</td>
                    </tr>

                    <tr>
                        <th>Country</th>
                        <td>country</td>
                    </tr>
                </table>
            </div>

            {{-- <div class="panel-footer text-right">
                <a href="#" class="mb-xs mt-xs mr-xs btn btn-success">Complete Order</a>
            </div> --}}
        </section>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
