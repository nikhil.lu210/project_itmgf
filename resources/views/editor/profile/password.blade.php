@extends('layouts.editor.app')

@section('page_title', '| Profile | Password Update')

@section('stylesheet_links')
{{--  External CSS  --}}
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Profile</span></li>
            <li><span>{{ Auth::user()->first_name." ".Auth::user()->last_name }}</span></li>
            <li><span>Update</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12"><h2 class="panel-title">Password Update</h2></div>
                </div>

            </div>

            <form action="{{ route('editor.profile.password.change') }}" method="post">
                @csrf
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-12">
                            @if (session('message'))
                                <div class="alert alert-info" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="old_password">Old Password <span>*</span></label>
                                <input class="form-control {{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="Old Password" value="{{ old('old_password') }}" required name="old_password" type="password">

                                @if ($errors->has('old_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="new_password">New Password <span>*</span></label>
                                <input class="form-control {{ $errors->has('new_password') ? ' is-invalid' : '' }}" placeholder="New Password" value="{{ old('new_password') }}" required name="new_password" type="password">

                                @if ($errors->has('new_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('new_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="confirm_password">Confirm New Password <span>*</span></label>
                                <input class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" placeholder="Confirm New Password" value="{{ old('confirm_password') }}" required name="confirm_password" type="password">

                                @if ($errors->has('confirm_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-primary" type="submit">Update Password</button>
                </div>
            </form>
        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}
<script src="{{ asset('custom/assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
