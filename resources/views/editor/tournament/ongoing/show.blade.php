@extends('layouts.editor.app')

@section('page_title', '| Tournaments | Ongoing Tournaments')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .t-vs{
            font-size: 8rem;
            color: red;
            margin-top: 9rem;
        }
        .b-top{
            border-top: 1px solid #dddddda1;
            padding-top: 10px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Ongoing Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <span>Ongoing Tournaments</span>
                </a>
            </li>

            <li>
                <span>tournament_name</span>
            </li>

            <li>
                <span>round_name</span>
            </li>

            <li>
                <span>match_name</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-6 col-md-offset-3">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12"><h2 class="panel-title">Who Is The Winner?</h2></div>
                </div>

            </div>

            <form method="POST" action="{{ route('editor.tournament.ongoing.update', ['id' => $match->id]) }}">
            @csrf
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-12">
                            @if (session('message'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="winner">The Winner Is: <span>*</span></label>
                                <select data-plugin-selectTwo class="form-control populate {{ $errors->has('winner') ? ' is-invalid' : '' }}" name="winner" placeholder="Select The Winner">
                                    <option selected>Select The Winner</option>
                                    <option value="{{ $match->player_one }}">{{ $match->playerOne['first_name']." ".$match->playerOne['last_name'] }} ({{ $match->playerOne['player']['country'] }})</option>
                                    <option value="{{ $match->player_two }}">{{ $match->playerTwo['first_name']." ".$match->playerTwo['last_name'] }} ({{ $match->playerTwo['player']['country'] }})</option>    
                                </select>


                                @if ($errors->has('winner'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('winner') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-primary" type="submit">Assign As Winner</button>
                </div>
            </form>
        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
