@extends('layouts.editor.app')

@section('page_title', '| Tournaments | Ongoing Tournaments')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .t-vs{
            font-size: 8rem;
            color: red;
            margin-top: 9rem;
        }
        .b-top{
            border-top: 1px solid #dddddda1;
            padding-top: 10px;
        }

    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Ongoing Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <span>Ongoing Tournaments</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
@if($tournament)
<div class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{ $tournament->name }}</h2>
        <a href="{{ route('graph', ['id'=> $tournament->id]) }}" class="btn btn-dark btn-sm btn-graph">View Graph</a>
    </header>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div class="alert alert-success" role="alert">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="tab-content">
                    <div id="overview" class="tab-pane active">
                        <div class="timeline timeline-simple">
                            @foreach ($matches as $round => $match)
                                <div class="tm-body">
                                    <div class="tm-title">
                                        <h3 class="h5 text-uppercase"><b>Round No: {{ $round }}</b></h3>
                                    </div>
                                    <ol class="tm-items text-center">
                                        @foreach ($match as $data)
                                        <li>
                                            <div class="tm-box">
                                                <h4 class="mb-none title"><b>Match No: {{ $data->match_number }}</b></h4>
                                                <?php $date = new DateTime($data->match_date); ?>
                                                <p class="text-muted mb-none date">Date: {{ $date->format('d M Y') }}</p>
                                                <p class="text-muted mb-none date">Venue: {{ $data->match_venue }}</p>
                                                <p class="text-muted mb-none date">Time: {{ $data->match_time }}</p>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-5 col-xl-5">
                                                        <section class="panel">
                                                            <header class="panel-heading
                                                            @if($data->match_status == -1)bg-info
                                                            @elseif($data->match_status == 1 && $data->player_one == $data->winner) bg-success
                                                            @else bg-danger
                                                            @endif
                                                            ">
                                                                <div class="panel-heading-profile-picture">
                                                                    @if(!$data->player_one)
                                                                        <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}">
                                                                    @else
                                                                        <img src="data:image/png;base64,{{ $data->playerOne['player']['avatar'] }}">
                                                                    @endif
                                                                </div>
                                                            </header>
                                                            <div class="panel-body">
                                                                <h4 class="text-semibold mt-sm">{{ $data->playerOne['player']['country'] }}</h4>
                                                                <h4 class="text-semibold mt-sm">{{ $data->playerOne['first_name']." ".$data->playerOne['last_name'] }}</h4>
                                                                <p>Age: {{ $data->playerOne['player']['age'] }} Years</p>
                                                            </div>
                                                        </section>
                                                    </div>

                                                    <div class="col-md-2 col-xl-2 text-center">
                                                        <h1 class="text-semibold t-vs">VS</h1>
                                                    </div>

                                                    <div class="col-md-5 col-xl-5">
                                                        <section class="panel">
                                                            <header class="panel-heading
                                                            @if($data->match_status == -1)bg-info
                                                            @elseif($data->match_status == 1 && $data->player_two == $data->winner) bg-success
                                                            @else bg-danger
                                                            @endif
                                                            ">
                                                                <div class="panel-heading-profile-picture">
                                                                    @if(!$data->player_two)
                                                                        <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}">
                                                                    @else
                                                                        <img src="data:image/png;base64,{{ $data->playerTwo['player']['avatar'] }}">
                                                                    @endif
                                                                </div>
                                                            </header>
                                                            <div class="panel-body">
                                                                <h4 class="text-semibold mt-sm">{{ $data->playerTwo['player']['country'] }}</h4>
                                                                <h4 class="text-semibold mt-sm">{{ $data->playerTwo['first_name']." ".$data->playerTwo['last_name'] }}</h4>
                                                                <p>Age: {{ $data->playerTwo['player']['age'] }} Years</p>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                                @if($data->player_one && $data->player_two && $data->match_status == -1 && $data->match_number == $lastUpdate+1 )
                                                {{-- && $data->match_date == date('Y-m-d') --}}
                                                <div class="row b-top">
                                                    <div class="col-md-12 text-right">
                                                        <a href="{{ route('editor.tournament.ongoing.show', ['id' => $data->id]) }}" class="btn btn-primary">Update Match Result</a>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </li>
                                        @endforeach
                                    </ol>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
