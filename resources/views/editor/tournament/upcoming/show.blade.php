@extends('layouts.editor.app')

@section('page_title', '| Tournaments | Upcoming Tournaments | Update Information')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .b-top{
            border-top: 1px solid #dddddda1;
            padding-top: 10px;
        }
        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
            z-index: 999999;
        }
        .clr_red{
            color: red;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Update Tournament Information</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <a href="{{ route('editor.tournament.upcoming.index') }}">
                    <span>Upcoming Tournaments</span>
                </a>
            </li>

            <li>
                <span>Update Tournament Information</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">
            <section class="panel">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12"><h2 class="panel-title">{{ $tournament->name }}</h2></div>
                        <a href="{{ route('graph', ['id'=> $tournament->id]) }}" class="btn btn-dark btn-sm btn-graph">View Graph</a>
                    </div>
                </div>
                @if( $tournament->set_matches == -1 )
                <form method="POST" action="{{ route('editor.tournament.upcoming.store',['id'=>$tournament->id]) }}">
                @csrf
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-12">
                                @if (session('message'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('message') }}
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5><b>Choose Players</b></h5>
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="players">Assign Players <span>*</span> <b>You Must be choose Only <strong style="color:blueviolet">{{ $tournament->player_number }}</strong> Player</b></label>
                                                    <select multiple data-plugin-selectTwo class="form-control populate {{ $errors->has('players') ? ' is-invalid' : '' }}" required name="players[]" placeholder="Select Players">
                                                        <optgroup label="You Have Must To Select {{ $tournament->player_number }} Player">
                                                        @foreach($players as $player)
                                                            <?php
                                                                $approved = App\User::find($player->player_id)->approved_at;
                                                                if($approved == null) continue;
                                                            ?>

                                                            <option value="{{ $player->player_id }}">{{ App\User::find($player->player_id)->first_name." ".App\User::find($player->player_id)->last_name }}</option>
                                                        @endforeach
                                                        </optgroup>
                                                    </select>

                                                    @if ($errors->has('players'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('players') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $matchNo = 1; ?>
                        @foreach($roundMatches as $round => $match)
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5><b>Round <span class="clr_red">{{ $round+1 }}</span> Match Scheduling</b></h5>
                                        </div>
                                    @for($i = 1; $i <= $match; $i++)
                                    <input type="hidden" name="roundNo[{{ $matchNo }}]" value = {{ $round+1 }}>
                                        <div class="panel-body">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="match_1_date">Match <b class="clr_red">{{ $matchNo }}</b> Date <span>*</span></label>
                                                    <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('match_date[{{ $matchNo }}]') is-invalid @enderror" name="match_date[{{ $matchNo }}]"  placeholder="yyyy-mm-dd" required>

                                                    @error('match_date[{{ $matchNo }}]')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="match_1_time">Match <b class="clr_red">{{ $matchNo }}</b> Time <span>*</span></label>
                                                    <input type="text" autocomplete="off" data-plugin-timepicker class="form-control @error('match_time[{{ $matchNo }}]') is-invalid @enderror" name="match_time[{{ $matchNo }}]"  placeholder="9:30 PM" required>

                                                    @error('match_time[{{ $matchNo }}]')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="match_1_venue">Match <b class="clr_red">{{ $matchNo }}</b> Venue <span>*</span></label>
                                                    <input type="text" autocomplete="off" class="form-control @error('match_venue[{{ $matchNo }}]') is-invalid @enderror" name="match_venue[{{ $matchNo }}]"  placeholder="Venue Name" required>

                                                    @error('match_venue[{{ $matchNo }}]')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <?php $matchNo++; ?>
                                    @endfor
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit">Update Information</button>
                    </div>
                </form>
                @elseif( $tournament->set_matches == 1 )

                <div class="col-md-12">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="tab-content">
                                <div id="overview" class="tab-pane active">
                                    <div class="timeline timeline-simple">
                                        @foreach ($matches as $round => $match)
                                        <div class="tm-body">
                                            <div class="tm-title">
                                                <h3 class="h5 text-uppercase"><b>Round No: {{ $round }}</b></h3>
                                            </div>
                                            <ol class="tm-items text-center">
                                                @foreach($match as $key => $data)
                                                <li>
                                                    <div class="tm-box">
                                                        <h4 class="mb-none title"><b>Match No: {{ $data->match_number }}</b></h4>
                                                        <?php $matchDate = new DateTime($data->match_date); ?>
                                                        <p class="text-muted mb-none date">Date: {{ $matchDate->format('d M Y') }}</p>
                                                        <p class="text-muted mb-none date">Venue: {{ $data->match_venue }}</p>
                                                        <p class="text-muted mb-none date">Time: {{ $data->match_time }}</p>
                                                        <hr>

                                                        <div class="row">
                                                            <div class="col-md-5 col-xl-5">
                                                                <section class="panel">
                                                                    <header class="panel-heading bg-info">
                                                                        <div class="panel-heading-profile-picture">
                                                                            @if(!$data->player_one)
                                                                                <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}">
                                                                            @else
                                                                                <img src="data:image/png;base64,{{ $data->playerOne['player']['avatar'] }}">
                                                                            @endif
                                                                        </div>
                                                                    </header>
                                                                    <div class="panel-body">
                                                                        <h4 class="text-semibold mt-sm">{{ $data->playerOne['player']['country'] }}</h4>
                                                                        <h4 class="text-semibold mt-sm">{{ $data->playerOne['first_name']. " " .$data->playerOne['last_name'] }}</h4>
                                                                        @if($data->playerOne['player']['age'])
                                                                            <p>Age: {{ $data->playerOne['player']['age'] }} Years</p>
                                                                        @endif
                                                                    </div>
                                                                </section>
                                                            </div>

                                                            <div class="col-md-2 col-xl-2 text-center">
                                                                <h1 class="text-semibold t-vs">VS</h1>
                                                            </div>

                                                            <div class="col-md-5 col-xl-5">
                                                                <section class="panel">
                                                                    <header class="panel-heading bg-info">
                                                                        <div class="panel-heading-profile-picture">
                                                                            @if(!$data->player_two)
                                                                                <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}">
                                                                            @else
                                                                                <img src="data:image/png;base64,{{ $data->playerTwo['player']['avatar'] }}">
                                                                            @endif
                                                                        </div>
                                                                    </header>
                                                                    <div class="panel-body">
                                                                            <h4 class="text-semibold mt-sm">{{ $data->playerTwo['player']['country'] }}</h4>
                                                                            <h4 class="text-semibold mt-sm">{{ $data->playerTwo['first_name']. " " .$data->playerTwo['last_name'] }}</h4>
                                                                            @if($data->playerTwo['player']['age'])
                                                                                <p>Age: {{ $data->playerTwo['player']['age'] }} Years</p>
                                                                            @endif
                                                                        </div>
                                                                </section>
                                                            </div>
                                                        </div>

                                                        <div class="row b-top">
                                                            <div class="col-md-12 text-right">
                                                                <a href="{{ route('editor.tournament.upcoming.edit', ['id' => $data->id]) }}" class="btn btn-primary">Update Match Info</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @endif
            </section>
    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
