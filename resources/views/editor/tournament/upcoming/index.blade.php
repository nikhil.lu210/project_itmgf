@extends('layouts.editor.app')

@section('page_title', '| Tournaments | Upcoming Tournaments')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .b-top{
            border-top: 1px solid #dddddda1;
            padding-top: 10px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Upcoming Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <span>Upcoming Tournaments</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="tab-content">
            <div id="overview" class="tab-pane active">
                @if (session('message'))
                    <div class="alert alert-success" role="alert">
                        {{ session('message') }}
                    </div>
                @endif

                <h4 class="mb-xlg">Upcoming Tournaments</h4>

                <div class="timeline timeline-simple mt-xlg mb-md">
                    @foreach($tournaments as $date => $tournament)
                        <div class="tm-body">
                            <div class="tm-title">
                                <h3 class="h5 text-uppercase">{{ $date }}</h3>
                            </div>
                                <ol class="tm-items">
                                    @foreach ($tournament as $data)
                                    @if(empty($data->assignedEditors[0]) ) <?php continue; ?> @endif
                                    <li>
                                        <div class="tm-box">
                                            <h4 class="mb-none title">
                                                <b>{{ $data->name }}</b>
                                            </h4>
                                            <p class="text-muted mb-none date">Date: {{ $data->starting_date }}</p>
                                            <hr>
                                            <p>{{ $data->note }}</p>
                                            <div class="row b-top">
                                                <div class="col-md-12 text-right">
                                                        <a href="{{ route('editor.tournament.upcoming.show', ['id' => $data->id]) }}" class="btn btn-primary btn-sm">Update Information</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                    
                                </ol>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
