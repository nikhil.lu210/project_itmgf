@extends('layouts.editor.app')

@section('page_title', '| Tournaments | Upcoming Tournaments | Match Info')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .t-vs{
            font-size: 8rem;
            color: red;
            margin-top: 9rem;
        }
        .b-top{
            border-top: 1px solid #dddddda1;
            padding-top: 10px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Upcoming Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('editor.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <a href="#">
                    <span>Upcoming Tournaments</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <span>tournament_name</span>
                </a>
            </li>

            <li>
                <span>Match: match_no</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-6 col-md-offset-3">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12"><h2 class="panel-title">Update Match No: {{ $match->match_number }}</h2></div>
                </div>

            </div>

            <form method="POST" action="{{ route('editor.tournament.upcoming.update', ['id' => $match->id]) }}">
            @csrf
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-12">
                            @if (session('message'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="match_date">Match <b class="clr_red">{{ $match->match_number }}</b> Date <span>*</span></label>
                                <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('match_date') is-invalid @enderror" name="match_date"  placeholder="yyyy-mm-dd" value="{{ $match->match_date }}" required>

                                @error('match_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="match_time">Match <b class="clr_red">{{ $match->match_number }}</b> Time <span>*</span></label>
                                <input type="text" autocomplete="off" data-plugin-timepicker class="form-control @error('match_time') is-invalid @enderror" name="match_time" value="{{ $match->match_time }}"  placeholder="9:30 PM" required>

                                @error('match_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="match_venue">Match <b class="clr_red">{{ $match->match_number }}</b> Venue <span>*</span></label>
                                <input type="text" autocomplete="off" class="form-control @error('match_venue') is-invalid @enderror" name="match_venue"  value="{{ $match->match_venue }}" placeholder="Venue Name" required>

                                @error('match_venue')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-primary" type="submit">Update Information</button>
                </div>
            </form>
        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
