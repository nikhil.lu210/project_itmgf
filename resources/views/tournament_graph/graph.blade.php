<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

{{-- CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="author" content="ThemeSelect">

<title> ITMGF | Tournament Graph </title>
<link rel="shortcut icon" href="{{asset('assets/images/logo.png')}}" type="image/x-icon">

<link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/vendors.min.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap-extended.min.css') }}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->

<style>
    body, .card.card-fullscreen, .card-content.collapse.show {
        background-color: #f0f2f2;
    }   

    .match{
        margin: 1em;
        border-radius: 0.25rem;
        overflow: hidden;
    }

    .winner{
        border-left: 0.25rem solid #60c645 !important;
    }
    .winner h4{
        color: #60c645 !important;
    }
    .looser{
        border-left: 0.25rem solid #e24242 !important;
    }
    .looser h4{
        color: #e24242 !important;
    }

    .plr{
        box-sizing: border-box;
        color: #858585;
        border-left: 0.25rem solid #858585;
        background: white;
        width: 70%;
        height: 3rem;
        /* box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.12); */
    }
    .plr:not(:last-child) {
        border-bottom: thin solid #f0f2f2;
    }
    .plr h4 {
        color: #858585;
        margin: 0 1.25rem;
        line-height: 3;
        font-size: 1rem;
        font-family: "Roboto Slab";
    }
</style>
</head>

<body>

    <section role="main" class="container-fluid" style="margin-top: 20px;">
        <!-- BEGIN: Content-->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="tournament_name"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-outline-dark btn-sm btn-add-new">Back</a>
                                </li>
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                {{-- <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li> --}}
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard h-100" id="match_graph">
                            {{-- <div class="row align-items-center h-100">
                                <div class="col-2 h-100 justify-content-center">
                                    <div class="match">
                                        <div class="plr player-one" data-toggle="tooltip" data-placement="top" title="BANGLADESH | 23">
                                            <h4><b>Player 01</b></h4>
                                        </div>
                                        <div class="plr player-two">
                                            <h4><b>Player 02</b></h4>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content-->
    </section>

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('assets/js/core/app-menu.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/core/app.min.js') }}" type="text/javascript"></script>

    <script>
        $( document ).ready(function() {
            var m_url = window.location.origin;
            var id = {{ $tournament->id }};
            $.ajax({
                method: 'GET',
                url: m_url+'/graph/json/'+id,
                success: function(data) {
                    var tournament = data.tournament;
                    var data = data.matches;
                    var match_graph = $('#match_graph');
                    var str = "<div class='row align-items-center h-100'>";
                    let round = Object.keys(data).length;
                    for(var i =1; i<=round; i++){
                        str += "<div class='col-"+parseInt(12/round)+" h-100 justify-content-center'>";

                        for(var j = 0; j< data[i].length; j++){
                            var country1 = (data[i][j].player_one != null)?(data[i][j].player_one.player.country+ " | " +data[i][j].player_one.player.weight):"";

                            var name1 = (data[i][j].player_one != null)?data[i][j].player_one.first_name:"Not Assigned";
                            var name2 = (data[i][j].player_two != null)?data[i][j].player_two.first_name:"Not Assigned";

                            var country2 = (data[i][j].player_two != null)?(data[i][j].player_two.player.country+ " | " +data[i][j].player_two.player.weight):"";

                            var player1, player2;
                            if(data[i][j].match_status == 1){
                                player1 = (data[i][j].winner == data[i][j].player_one.id) ? "winner":"looser";
                                player2 = (data[i][j].winner == data[i][j].player_two.id) ? "winner":"looser";
                            } else{
                                player1 = "";
                                player2 = "";
                            }

                            str += "<div class='match'><div class='plr player-one "+player1+"' data-toggle='tooltip' data-placement='top' title='"+country1+"'><h4><b>"+name1+"</b></h4></div><div class='plr player-two "+player2+"' data-toggle='tooltip' data-placement='top' title='"+country2+"'><h4><b>"+name2+"</b></h4></div></div>";
                        }

                        str += "</div>"; //close column div
                    }

                    str += "</div>"; //close row div

                    match_graph.empty().append(str);
                    $('#tournament_name').html(tournament.name);
                },
                error: function() {
                    console.log("player not found!!!");
                }
            });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });
    </script>
</body>
</html>
