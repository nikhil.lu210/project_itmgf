@extends('layouts.manager.app')

@section('page_title', '| Tournaments | Editor')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>All Editor</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Editors</span>
            </li>

            <li>
                <span>All Editors</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Editors</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Full Name</th>
                    <th>Email Address</th>
                    <th>Mobile Number</th>
                    <th>Assigned Date</th>
                    <th>Tournament Experience</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($editors as $key => $editor)
                <tr class="gradeX">
                    <td>
                        @if ($key+1 < 10)
                            <b>0{{ $key+1 }}</b>
                        @else
                            <b>{{ $key+1 }}</b>
                        @endif
                    </td>
                    <td>{{ $editor->first_name." ".$editor->last_name }}</td>
                    <td>{{ $editor->email }}</td>
                    <td>{{ $editor->mobile }}</td>
                    <?php
                        $date = new DateTime($editor->created_at);
                    ?>
                    <td>{{ $date->format('d M Y') }}</td>
                    <td>{{ App\Models\AssignedEditor\AssignedEditor::where('editor_id', $editor->id)->count() }}</td>
                    <td class="action-td">
                        <a href="{{ route('manager.editor.show', ['id' => $editor->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
