@extends('layouts.manager.app')

@section('page_title', '| Tournaments | Editor | Details | editor_name')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>editor_name</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Editors</span>
            </li>

            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <span>All Editors</span>
                </a>
            </li>

            <li>
                <span>editor_name</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{ $editor->first_name }}</h2>
    </header>

    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none">
            <tr>
                <th>Full Name</th>
                <td>{{ $editor->first_name." ".$editor->last_name }}</td>
            </tr>

            <tr>
                <th>Email Address</th>
                <td>{{ $editor->email }}</td>
            </tr>

            <tr>
                <th>Mobile Number</th>
                <td>{{ $editor->mobile }}</td>
            </tr>

            <tr>
                <th>Tournament Lists</th>
                <td>
                    @if(!isset($editor->assignedTournamentEditors[0]))New Editor @endif
                    <ol>
                        @foreach($editor->assignedTournamentEditors as $item)
                        <li>{{ App\Models\Tournament\Tournament::find($item->tournament_id)->name }}</li>
                        @endforeach
                    </ol>
                </td>
            </tr>
        </table>
    </div>

    {{-- <div class="panel-footer text-right">
        <a href="#" class="mb-xs mt-xs mr-xs btn btn-success">Complete Order</a>
    </div> --}}
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
