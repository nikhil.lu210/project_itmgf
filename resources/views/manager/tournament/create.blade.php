@extends('layouts.manager.app')

@section('page_title', '| Tournaments | Create Tournament')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />

    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
            z-index: 999999;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Create New Tournament</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <span>Create New Tournament</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-4"><h2 class="panel-title">Create A Tournament</h2></div>
                </div>

            </div>

            <form method="POST" action="{{ route('manager.tournament.store') }}">
            @csrf
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-12">
                            @if (session('message'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="name">Tournament Name <span>*</span></label>
                                <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Tournament Name *" value="{{ old('name') }}" required autofocus name="name" type="text" autocomplete="off">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="starting_date">Starting Date <span>*</span></label>
                                <input class="form-control {{ $errors->has('starting_date') ? ' is-invalid' : '' }}" placeholder="Maximum Age *" value="{{ old('starting_date') }}" required name="starting_date" type="text" autocomplete="off" data-plugin-datepicker >

                                @if ($errors->has('starting_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('starting_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="gender">Gender <span>*</span></label>
                                <select class="form-control mb-md @error('gender') is-invalid @enderror" id="gender_create" name="gender" required>
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="category">Category <span>*</span></label>
                                <select class="form-control mb-md @error('category') is-invalid @enderror" id="category_create" name="category" required>
                                    <option value="">Select Category</option>
                                    <option value="Junior Cadet (8-10yr)">Junior Cadet (8-10yr)</option>
                                    <option value="Cadet (11-13yr)">Cadet (11-13yr)</option>
                                    <option value="Junior Youth (14-15yr)">Junior Youth (14-15yr)</option>
                                    <option value="Youth (16-18yr)">Youth (16-18yr)</option>
                                    <option value="Senior (19+yr)">Senior (19+yr)</option>
                                    <option value="Pro-AM (19+yr)">Pro-AM (19+yr)</option>
                                    <option value="Professional (19+yr)">Professional (19+yr)</option>
                                    <option value="Dab-Thai">Dab-Thai</option>
                                </select>
                                
                                @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="weight">Weight <span>*</span></label>
                                <select class="form-control mb-md @error('weight') is-invalid @enderror" id="weight_create" name="weight" required>
                                    <option value="">Select Weight</option>
                                </select>
                                
                                @error('weight')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="total_player">Total Player <span>*</span></label>
                                <select class="form-control mb-md {{ $errors->has('total_player') ? ' is-invalid' : '' }}" id="selectedPlayer" required name="total_player">
                                    <option value="">Select Total Player</option>
                                </select>

                                @if ($errors->has('total_player'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('total_player') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="editors">Assign Editors <span>*</span></label>
                                <select multiple data-plugin-selectTwo class="form-control populate {{ $errors->has('editors') ? ' is-invalid' : '' }}" required name="editors[]" placeholder="Select Editors">
                                    {{-- <optgroup label="Alaskan/Hawaiian Time Zone">
                                        <option value="AK">Alaska</option>
                                        <option value="HI">Hawaii</option>
                                    </optgroup> --}}
                                    @foreach($editors as $editor)
                                    <option value="{{ $editor->id }}">{{ $editor->first_name." ".$editor->last_name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('editors'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('editors') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="note">Note <span>*</span></label>
                                <textarea class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" rows="3" id="note" name="note" required placeholder="Write Something About This Tournament"></textarea>

                                @if ($errors->has('note'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button class="btn btn-primary" type="submit">Create Tournament</button>
                </div>
            </form>
        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}
<script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

<script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
<script>
    $( document ).ready(function() {
        $( "#gender_create" ).change(function() {
            getavailavle();
        });

        $( "#category_create" ).change(function() {
            getavailavle();
        });

        $( "#weight_create" ).change(function() {
            getavailavle();
        });

        function getavailavle(){
            let category = $('#category_create').val();
            let weight = $('#weight_create').val();

            let gender = $('#gender_create').val();
            var m_url = window.location.origin;

            if(category != '' && weight != '' && gender != ''){

                $.ajax({
                    method: 'GET',
                    url: m_url+'/manager/tournaments/availavleplayer/'+ gender +'/' + category + '/' +weight,
                    success: function(data) {
                        let range = data;
                        let htm = "<option value=''>Select Total Player</option>"
                        for(var i=1; i<=range; i++){
                            htm += "<option value='"+ i +"'>"+ i +"</option>";
                        }
                        var a = $('#selectedPlayer');
                        a.empty();
                        a.append(htm);
                    },
                    error: function() {
                        console.log("player not found!!!");
                    }
                });

            }
        }
    });
</script>

<script>
    $(document).ready(function(){
        var juniorCadetMale = [20,22,24,26,28,31,33,35,38,42,45,48,51,53.5,57];
        var juniorCadetFemale = [20,22,24,26,28,31,33,35,38,42,45,48,51,53.5,57];

        var cadetMale = [28,31,33,35,38,42,45,48,51,53.5,57,60,63.5,67];
        var cadetFemale = [28,31,33,35,38,42,45,48,51,53.5,57,60,63.5,67];

        var juniorYouthMale = [41,43,45,48,51,53.5,57,60,63.5,67,71,75,81];
        var juniorYouthFemale = [41,43,45,48,51,53.5,57,60,63.5,67,71];

        var youthMale = [48,51,53.5,57,60,63.5,67,71,75,81,86];
        var youthFemale = [45,48,51,53.5,57,60,63.5,67,71];

        var seniorMale = [48,51,53.5,57,60,63.5,67,71,75,81,86,91];
        var seniorFemale = [45,48,51,53.5,57,60,63.5,67,71];
        
        var proAmMale = [48,51,53.5,57,60,63.5,67,71,75,81,86,91];
        var proAmFemale = [45,48,51,53.5,57,60,63.5,67];

        var professionalMale = [45,48,50,52,54,56,58,61,63.5,67,69.5,72.5,76,79.5,83,86,90,95];
        var professionalFemale = [45,48,50,52,54,56,58,61,63.5,67,69.5,72.5];



        var gender_create = $('#gender_create');
        var category_create = $('#category_create');
        var weight_create = $('#weight_create');

        category_create.attr('disabled', true);
        weight_create.attr('disabled', true);

        $(gender_create).change(function(){
            if(gender_create.val() != '' ) category_create.removeAttr('disabled');
            else category_create.attr('disabled', true);

            var cat = category_create.val();
            var gender = gender_create.val();

            if(cat != '' && gender != ''){
                var options = getoptions(cat, gender);
                weight_create.empty().append(options);
                weight_create.removeAttr('disabled');
            }
            else weight_create.attr('disabled', true);
        });

        $(category_create).change(function(){
            var cat = category_create.val();
            var gender = gender_create.val();

            var weight = null;

            if(cat != '' && gender != ''){
                var options = getoptions(cat, gender);
                weight_create.empty().append(options);
                weight_create.removeAttr('disabled');
            }
            else weight_create.attr('disabled', true);
            
        });

        function getoptions(cat, gender){
            var weight = null;

            var options = '<option value="">Select Category</option>';

            if(cat != '' && gender != ''){
                if(cat == 'Junior Cadet (8-10yr)' && gender == 'Male') weight =  juniorCadetMale;
                else if(cat == 'Cadet (11-13yr)' && gender == 'Male') weight =  cadetMale;
                else if(cat == 'Junior Youth (14-15yr)' && gender == 'Male') weight =  juniorYouthMale;
                else if(cat == 'Youth (16-18yr)' && gender == 'Male') weight =  youthMale;
                else if(cat == 'Senior (19+yr)' && gender == 'Male') weight =  seniorMale;
                else if(cat == 'Pro-AM (19+yr)' && gender == 'Male') weight =  proAmMale;
                else if(cat == 'Professional (19+yr)' && gender == 'Male') weight =  professionalMale;
                else if(cat == 'Dab-Thai' && gender == 'Male') weight =  professionalMale;


                else if(cat == 'Junior Cadet (8-10yr)' && gender == 'Female') weight =  juniorCadetFemale;
                else if(cat == 'Cadet (11-13yr)' && gender == 'Female') weight =  cadetFemale;
                else if(cat == 'Junior Youth (14-15yr)' && gender == 'Female') weight =  juniorYouthFemale;
                else if(cat == 'Youth (16-18yr)' && gender == 'Female') weight =  youthFemale;
                else if(cat == 'Senior (19+yr)' && gender == 'Female') weight =  seniorFemale;
                else if(cat == 'Pro-AM (19+yr)' && gender == 'Female') weight =  proAmFemale;
                else if(cat == 'Professional (19+yr)' && gender == 'Female') weight =  professionalFemale;
                else if(cat == 'Dab-Thai' && gender == 'Female') weight =  professionalFemale;

                for(var i=0; i<weight.length; i++){
                    options += "<option value = '"+weight[i]+"'>"+weight[i]+" Kg's</option>"
                }
            }
            return options;
        }
    });
</script>

@endsection
