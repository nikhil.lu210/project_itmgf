@extends('layouts.manager.app')

@section('page_title', '| tournament Details')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .t-vs{
            font-size: 8rem;
            color: red;
            margin-top: 9rem;
        }
        .btn-details{
            float: right;
            margin-top: -25px;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>tournament_name</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <a href="{{ route('manager.tournament.ongoing.index') }}">
                    <span>Ongoing Tournaments</span>
                </a>
            </li>

            <li>
                <span>tournament_name</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{ App\Models\Tournament\Tournament::find($tournament_id)->name }}</h2>
        <a href="{{ route('manager.tournament.ongoing.details', ['id'=>$tournament_id]) }}" class="btn btn-primary btn-details">More Details</a>
    </header>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="tab-content">
                    <div id="overview" class="tab-pane active">
                        <div class="timeline timeline-simple mt-xlg mb-md">
                            @foreach($matches as $round => $match)
                            <div class="tm-body">
                                <div class="tm-title">
                                    <h3 class="h5 text-uppercase"><b>Round No: {{ $round }}</b></h3>
                                </div>
                                <ol class="tm-items text-center">
                                    @foreach($match as $data)
                                    <li>
                                        <div class="tm-box">
                                            <h4 class="mb-none title"><b>Match No: {{ $data->match_number }}</b></h4>
                                            <?php $date = new DateTime($data->match_date);?>
                                            <p class="text-muted mb-none date">Date: {{ $date->format('d M Y') }}</p>
                                            <p class="text-muted mb-none date">Venue: {{ $data->match_venue }}</p>
                                            <p class="text-muted mb-none date">Time: {{ $data->match_time }}</p>
                                            <hr>
        
                                            <div class="row">
                                                <div class="col-md-5 col-xl-5">
                                                    <section class="panel">
                                                        <header class="panel-heading
                                                        @if($data->match_status == -1)bg-info
                                                        @elseif($data->match_status == 1 && $data->player_one == $data->winner) bg-success
                                                        @else bg-danger
                                                        @endif
                                                        ">
                                                            <div class="panel-heading-profile-picture">
                                                                @if(!$data->player_one)
                                                                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}">
                                                                @else
                                                                    <img src="data:image/png;base64,{{ $data->playerOne['player']['avatar'] }}">
                                                                @endif
                                                            </div>
                                                        </header>
                                                        <div class="panel-body">
                                                            <h4 class="text-semibold mt-sm">{{ $data->playerOne['player']['country'] }}</h4>
                                                            <h4 class="text-semibold mt-sm">{{ $data->playerOne['first_name']." ".$data->playerOne['last_name'] }}</h4>
                                                            <p>Age: {{ $data->playerOne['player']['age'] }} Years</p>
                                                        </div>
                                                    </section>
                                                </div>
        
                                                <div class="col-md-2 col-xl-2 text-center">
                                                    <h1 class="text-semibold t-vs">VS</h1>
                                                </div>
        
                                                <div class="col-md-5 col-xl-5">
                                                    <section class="panel">
                                                        <header class="panel-heading
                                                        @if($data->match_status == -1)bg-info
                                                        @elseif($data->match_status == 1 && $data->player_two == $data->winner) bg-success
                                                        @else bg-danger
                                                        @endif
                                                        ">
                                                            <div class="panel-heading-profile-picture">
                                                                @if(!$data->player_two)
                                                                    <img src="{{ asset('custom/assets/images/!logged-user.jpg') }}">
                                                                @else
                                                                    <img src="data:image/png;base64,{{ $data->playerTwo['player']['avatar'] }}">
                                                                @endif
                                                            </div>
                                                        </header>
                                                        <div class="panel-body">
                                                            <h4 class="text-semibold mt-sm">{{ $data->playerTwo['player']['country'] }}</h4>
                                                            <h4 class="text-semibold mt-sm">{{ $data->playerTwo['first_name']." ".$data->playerTwo['last_name'] }}</h4>
                                                            <p>Age: {{ $data->playerTwo['player']['age'] }} Years</p>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="panel-footer text-right">
        <a href="{{ route('manager.tournament.ongoing.details', 1) }}" class="btn btn-primary">More Details</a>
    </div> --}}
</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
