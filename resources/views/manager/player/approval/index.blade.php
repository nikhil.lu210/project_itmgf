@extends('layouts.manager.app')

@section('page_title', '| Players | Non-Approved')

@section('stylesheet_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .dataTables_wrapper .DTTT.btn-group{
        display: none !important;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Non-Approved Players</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Non-Approved Players</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Non-Approved Players</h2>
    </header>
    <div class="panel-body">
        @if (session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Country</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Email Address</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($players as $key => $player)
                    <?php
                        if($player->player == null) continue;
                    ?>
                    <tr class="gradeX">
                        <td>
                            @if ($key+1 < 10)
                                <b>0{{ $key+1 }}</b>
                            @else
                                <b>{{ $key+1 }}</b>
                            @endif
                        </td>
                        <td>{{ $player->player['country'] }}</td>
                        <td>{{ $player->player['age'] }}</td>
                        <td>{{ $player->player['gender'] }}</td>
                        <td>{{ $player->first_name." ".$player->last_name }}</td>
                        <td>{{ $player->player['mobile'] }}</td>
                        <td>{{ $player->email }}</td>
                        <td class="action-td">
                            <a href="{{ route('manager.player.need_approval_show', ['id' => $player->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>

<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}
    <script src="{{ asset('custom/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('custom/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('custom/assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
