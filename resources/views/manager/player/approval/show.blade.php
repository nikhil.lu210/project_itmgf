@extends('layouts.manager.app')

@section('page_title', '| Players | Non-Approval Players | Details')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Non-Approval Players</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('manager.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Players</span>
            </li>

            <li>
                <a href="{{ route('manager.player.need_approval') }}">
                    <span>Non-Approval Players</span>
                </a>
            </li>

            <li>
                <span>player_name</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{ $player->first_name }} Details</h2>
    </header>

    <div class="panel-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">{{ $player->first_name }} Profile Details</h2>
            </header>
        
            <div class="panel-body">
                <div class="col-md-4">
                    <img src="data:image/png;base64,{{ $player->player->avatar }}" alt="" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-8">
                    <table class="table table-bordered table-striped mb-none">
                        <tr>
                            <th>Player Country</th>
                            <td>{{ $player->player->country }}</td>
                        </tr>
            
                        <tr>
                            <th>Full Name</th>
                            <td>{{ $player->first_name." ".$player->last_name }}</td>
                        </tr>
            
                        <tr>
                            <th>Age</th>
                            <td>{{ $player->player->age }}</td>
                        </tr>
            
                        <tr>
                            <th>Weight</th>
                            <td>{{ $player->player->weight }} KG's</td>
                        </tr>
            
                        <tr>
                            <th>Gender</th>
                            <td>{{ $player->player->gender }}</td>
                        </tr>
            
                        <tr>
                            <th>Mobile</th>
                            <td>{{ $player->player->mobile }}</td>
                        </tr>
                        <tr>
                            <th>Title</th>
                            <td>{{ $player->player->title }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
        
        
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">{{ $player->first_name }} Other Details</h2>
            </header>
        
            <div class="panel-body">
                <div class="col-md-4">
                    <img src="data:image/png;base64,{{ $player->player->passport_avatar }}" alt="" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-8">
                    <table class="table table-bordered table-striped mb-none">
                        <tr>
                            <th>Airlines</th>
                            <td>{{ $player->player->airlines }}</td>
                        </tr>
                        <tr>
                            <th>Flight Number</th>
                            <td>{{ $player->player->flight_number }}</td>
                        </tr>
                        <tr>
                            <th>Arrival Date</th>
                            <td>{{ $player->player->arrival_date }}</td>
                        </tr>
                        <tr>
                            <th>Arrival Time</th>
                            <td>{{ $player->player->arrival_time }}</td>
                        </tr>
                        <tr>
                            <th>Departure Date</th>
                            <td>{{ $player->player->departure_date }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <div class="panel-footer text-right">
        <a onclick="return confirm('Are You Sure to Decline?')" href="{{ route('manager.player.decline', ['id' => $player->id]) }}" class="mb-xs mt-xs mr-xs btn btn-danger">Decline</a>
        
        <a href="{{ route('manager.player.approved', ['id' => $player->id]) }}" class="mb-xs mt-xs mr-xs btn btn-success">Approve</a>
    </div>
</section>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
