@extends('layouts.player.app')

@section('page_title', '| Dashboard')

@section('stylesheet_links')
{{--  External CSS  --}}
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />

<link rel="stylesheet" href="{{ asset('custom/css/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('custom/css/bootstrap-select-country.min.css') }}" />
@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
    .bootstrap-select>.dropdown-toggle{
        padding: 6px;
        height: 34px;
    }
    .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn){
        width: 100%;
    }
    .bootstrap-select .dropdown-toggle:focus {
        outline: none !important;
        outline: none !important;
        outline-offset: 0 !important;
        background: #fff !important;
    }
    </style>
@endsection

@section('content')

<header class="page-header">
    @if (!$player->approved_at)
        <h2>Fillup This Informations</h2>
    @else
        <h2>Update Profile</h2>
    @endif

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="/">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            @if (!$player->approved_at)
                <li><span>Fillup This Informations</span></li>
            @else
                <li><span>Update Profile</span></li>
            @endif
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-12 col-lg-12">
        @if (session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <div class="col-md-12 col-lg-12">

        <section class="panel">
            <div class="panel-heading">
                <div class="row">
                    @if(!$player->approved_at)
                        <div class="col-md-8"><h2 class="panel-title"><b>You Must Have To Fillup This Information.</b></h2></div>
                    @else
                        <div class="col-md-4"><h2 class="panel-title">Profile Update</h2></div>
                    @endif
                </div>

            </div>

            @if($status == 0)
                <form action="{{ route('player.profile.create') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profile Picture <span>*</span></label>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select Image</span>
                                                    <input type="file" name="avatar" class="@error('avatar') is-invalid @enderror" required>
                                                </span>

                                                @error('avatar')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="b_date">Birthdate <span>*</span></label>
                                    <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('b_date') is-invalid @enderror" name="b_date"  placeholder="mm/dd/yyyy" required>

                                    @error('b_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="gender">Gender <span>*</span></label>
                                    <select class="form-control mb-md @error('gender') is-invalid @enderror" id="gender_create" name="gender" required>
                                        <option value="">Select Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    
                                    @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="category">Category <span>*</span></label>
                                    <select class="form-control mb-md @error('category') is-invalid @enderror" id="category_create" name="category" required>
                                        <option value="">Select Category</option>
                                        <option value="Junior Cadet (8-10yr)">Junior Cadet (8-10yr)</option>
                                        <option value="Cadet (11-13yr)">Cadet (11-13yr)</option>
                                        <option value="Junior Youth (14-15yr)">Junior Youth (14-15yr)</option>
                                        <option value="Youth (16-18yr)">Youth (16-18yr)</option>
                                        <option value="Senior (19+yr)">Senior (19+yr)</option>
                                        <option value="Pro-AM (19+yr)">Pro-AM (19+yr)</option>
                                        <option value="Professional (19+yr)">Professional (19+yr)</option>
                                        <option value="Dab-Thai">Dab-Thai</option>
                                    </select>
                                    
                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="weight">Weight <span>*</span></label>
                                    <select class="form-control mb-md @error('weight') is-invalid @enderror" id="weight_create" name="weight" required>
                                        <option value="">Select Weight</option>
                                    </select>
                                    
                                    @error('weight')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- ==========< Country Name With Flag >============== --}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country">Country <span>*</span></label>
                                    <select class="selectpicker countrypicker @error('country') is-invalid @enderror" name="country" data-flag="true" >
                                        {{-- <option value="{{ $profile->country}}" selected>{{ $profile->country}}</option> --}}
                                    </select>

                                    @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country">Country <span>*</span></label>
                                    <input type="text" autocomplete="off" class="form-control @error('country') is-invalid @enderror" name="country" placeholder="Country" required>

                                    @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> --}}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Email <span>*</span></label>
                                    <input type="email" autocomplete="off" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="jhondoe@mail.com">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="number">Mobile Number <span>*</span></label>
                                    <input type="text" autocomplete="off" class="form-control @error('number') is-invalid @enderror" name="number" placeholder="65464989441313" required>

                                    @error('number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <select class="form-control mb-md" name="title" required>
                                        <option value="">Select Title</option>
                                        <option value="VIP">VIP</option>
                                        <option value="Team Manager">Team Manager</option>
                                        <option value="Coach">Coach</option>
                                        <option value="Medical">Medical</option>
                                        <option value="Referee/Judge">Referee/Judge</option>
                                        <option value="Athlete">Athlete</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Passport Photo <span>*</span></label>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" class="@error('passport_avatar') is-invalid @enderror" name="passport_avatar" required/>
                                                </span>

                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                            @error('passport_avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="airline">Airlines <span>*</span></label>
                                    <input type="text" autocomplete="off" class="form-control @error('airline') is-invalid @enderror" name="airline" placeholder="65666989661313" required>

                                    @error('airline')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flight_no">Flight Number <span>*</span></label>
                                    <input type="text" autocomplete="off" class="form-control @error('flight_no') is-invalid @enderror" name="flight_no" placeholder="65464989441313" required>

                                    @error('flight_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="arrival_date">Arrival Date <span>*</span></label>
                                    <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('arrival_date') is-invalid @enderror" name="arrival_date"  placeholder="mm/dd/yyyy" required>

                                    @error('arrival_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="arrival_time">Arrival Time <span>*</span></label>
                                    <input type="text" autocomplete="off" data-plugin-timepicker class="form-control @error('arrival_time') is-invalid @enderror" name="arrival_time"  placeholder="9:30 PM" required>

                                    @error('arrival_time')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="departure_date">Departure Date <span>*</span></label>
                                    <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('departure_date') is-invalid @enderror" name="departure_date"  placeholder="mm/dd/yyyy" required>

                                    @error('departure_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit">Create Profile</button>
                    </div>
                </form>


            @else


                <form action="{{ route('player.profile.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Profile Picture</label>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" name="avatar" class="@error('avatar') is-invalid @enderror">
                                                </span>

                                                @error('avatar')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="b_date">Birthdate</label>
                                <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('b_date') is-invalid @enderror" name="b_date"  placeholder="mm/dd/yyyy" value="{{ $profile->date_of_birth }}">

                                    @error('b_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select class="form-control mb-md" id="gender_update" name="gender">
                                        <option value="" >Select Gender</option>
                                        <option value="Male" @if($profile->gender == 'Male') selected @endif>Male</option>
                                        <option value="Female" @if($profile->gender == 'Female') selected @endif>Female</option>
                                        <option value="Other" @if($profile->gender == 'Other') selected @endif>Other</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="category">Category <span>*</span></label>
                                    <select class="form-control mb-md" id="category_update" name="category">
                                        <option value="">Select Category</option>
                                        <option value="Junior Cadet (8-10yr)" @if($profile->category == 'Junior Cadet (8-10yr)') selected @endif>Junior Cadet (8-10yr)</option>
                                        <option value="Cadet (11-13yr)" @if($profile->category == 'Cadet (11-13yr)') selected @endif >Cadet (11-13yr)</option>
                                        <option value="Junior Youth (14-15yr)" @if($profile->category == 'Junior Youth (14-15yr)') selected @endif >Junior Youth (14-15yr)</option>
                                        <option value="Youth (16-18yr)" @if($profile->category == 'Youth (16-18yr)') selected @endif >Youth (16-18yr)</option>
                                        <option value="Senior (19+yr)" @if($profile->category == 'Senior (19+yr)') selected @endif >Senior (19+yr)</option>
                                        <option value="Pro-AM (19+yr)" @if($profile->category == 'Pro-AM (19+yr)') selected @endif >Pro-AM (19+yr)</option>
                                        <option value="Professional (19+yr)" @if($profile->category == 'Professional (19+yr)') selected @endif >Professional (19+yr)</option>
                                        <option value="Dab-Thai" @if($profile->category == 'Dab-Thai') selected @endif >Dab-Thai</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="weight">Weight <span>*</span></label>
                                    <select class="form-control mb-md @error('weight') is-invalid @enderror" id="weight_update" name="weight">
                                        <option value="{{ $profile->weight }}" selected>{{ $profile->weight }}</option>
                                    </select>
                                    
                                    @error('weight')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- ==========< Country Name With Flag >============== --}}
                            {{-- <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country">Country <span>*</span></label>

                                    <select class="selectpicker countrypicker @error('country') is-invalid @enderror" name="country" data-flag="true">
                                    </select>

                                    @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> --}}

                            {{-- <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country">Country</label>
                                <input type="text" autocomplete="off" class="form-control @error('country') is-invalid @enderror" name="country" placeholder="Country" value="{{ $profile->country}}">

                                    @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> --}}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" autocomplete="off" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="jhondoe@mail.com" value="{{ $profile->email }}">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="number">Mobile Number</label>
                                    <input type="text" autocomplete="off" class="form-control @error('number') is-invalid @enderror" name="number" placeholder="65464989441313" value="{{ $profile->mobile }}">

                                    @error('number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <select class="form-control mb-md" name="title" required>
                                        <option value="">Select Title</option>
                                        <option value="VIP" @if($profile->title == 'VIP') selected @endif>VIP</option>
                                        <option value="Team Manager" @if($profile->title == 'Team Manager') selected @endif>Team Manager</option>
                                        <option value="Coach" @if($profile->title == 'Coach') selected @endif>Coach</option>
                                        <option value="Medical" @if($profile->title == 'Medical') selected @endif>Medical</option>
                                        <option value="Referee/Judge" @if($profile->title == 'Referee/Judge') selected @endif>Referee/Judge</option>
                                        <option value="Athlete" @if($profile->title == 'Athlete') selected @endif>Athlete</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Passport Photo</label>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fa fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Change</span>
                                                    <span class="fileupload-new">Select file</span>
                                                    <input type="file" class="@error('passport_avatar') is-invalid @enderror" name="passport_avatar"/>
                                                </span>

                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                            @error('passport_avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="airline">Airlines</label>
                                    <input type="text" autocomplete="off" class="form-control @error('airline') is-invalid @enderror" name="airline" placeholder="65666989661313" value="{{ $profile->airlines }}">

                                    @error('airline')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="flight_no">Flight Number</label>
                                    <input type="text" autocomplete="off" class="form-control @error('flight_no') is-invalid @enderror" name="flight_no" placeholder="65464989441313" value="{{ $profile->airlines }}">

                                    @error('flight_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="arrival_date">Arrival Date</label>
                                    <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('arrival_date') is-invalid @enderror" name="arrival_date"  placeholder="mm/dd/yyyy" value="{{ $profile->arrival_date }}">

                                    @error('arrival_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="arrival_time">Arrival Time</label>
                                    <input type="text" autocomplete="off" data-plugin-timepicker class="form-control @error('arrival_time') is-invalid @enderror" name="arrival_time"  placeholder="9:30 PM" value="{{ $profile->arrival_time }}">

                                    @error('arrival_time')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="departure_date">Departure Date</label>
                                    <input type="text" autocomplete="off" data-plugin-datepicker class="form-control @error('departure_date') is-invalid @enderror" name="departure_date"  placeholder="mm/dd/yyyy" value="{{ $profile->departure_date }}">

                                    @error('departure_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit">Update Profile</button>
                    </div>
                </form>
            @endif

        </section>

    </div>
</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}
<script src="{{ asset('custom/assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('custom/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

<script src="{{ asset('custom/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('custom/js/bootstrap-select-country.min.js') }}"></script>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
    $('.countrypicker').countrypicker();
    </script>
    <script>
        $(document).ready(function(){
            var juniorCadetMale = [20,22,24,26,28,31,33,35,38,42,45,48,51,53.5,57];
            var juniorCadetFemale = [20,22,24,26,28,31,33,35,38,42,45,48,51,53.5,57];

            var cadetMale = [28,31,33,35,38,42,45,48,51,53.5,57,60,63.5,67];
            var cadetFemale = [28,31,33,35,38,42,45,48,51,53.5,57,60,63.5,67];

            var juniorYouthMale = [41,43,45,48,51,53.5,57,60,63.5,67,71,75,81];
            var juniorYouthFemale = [41,43,45,48,51,53.5,57,60,63.5,67,71];

            var youthMale = [48,51,53.5,57,60,63.5,67,71,75,81,86];
            var youthFemale = [45,48,51,53.5,57,60,63.5,67,71];

            var seniorMale = [48,51,53.5,57,60,63.5,67,71,75,81,86,91];
            var seniorFemale = [45,48,51,53.5,57,60,63.5,67,71];
            
            var proAmMale = [48,51,53.5,57,60,63.5,67,71,75,81,86,91];
            var proAmFemale = [45,48,51,53.5,57,60,63.5,67];

            var professionalMale = [45,48,50,52,54,56,58,61,63.5,67,69.5,72.5,76,79.5,83,86,90,95];
            var professionalFemale = [45,48,50,52,54,56,58,61,63.5,67,69.5,72.5];



            var gender_create = $('#gender_create');
            var category_create = $('#category_create');
            var weight_create = $('#weight_create');

            category_create.attr('disabled', true);
            weight_create.attr('disabled', true);

            $(gender_create).change(function(){
                if(gender_create.val() != '' ) category_create.removeAttr('disabled');
                else category_create.attr('disabled', true);

                var cat = category_create.val();
                var gender = gender_create.val();

                if(cat != '' && gender != ''){
                    var options = getoptions(cat, gender);
                    weight_create.empty().append(options);
                    weight_create.removeAttr('disabled');
                }
                else weight_create.attr('disabled', true);
            });

            $(category_create).change(function(){
                var cat = category_create.val();
                var gender = gender_create.val();

                var weight = null;

                if(cat != '' && gender != ''){
                    var options = getoptions(cat, gender);
                    weight_create.empty().append(options);
                    weight_create.removeAttr('disabled');
                }
                else weight_create.attr('disabled', true);
                
            });



            ///this is for update profile
            var gender_update = $('#gender_update');
            var category_update = $('#category_update');
            var weight_update = $('#weight_update');

            category_update.attr('disabled', true);
            weight_update.attr('disabled', true);

            $(gender_update).change(function(){
                if(gender_update.val() != '' ) category_update.removeAttr('disabled');
                else category_update.attr('disabled', true);

                var cat = category_update.val();
                var gender = gender_update.val();

                if(cat != '' && gender != ''){
                    var options = getoptions(cat, gender);
                    weight_update.empty().append(options);
                    weight_update.removeAttr('disabled');
                }
            });

            $(category_update).change(function(){
                var cat = category_update.val();
                var gender = gender_update.val();

                if(cat != '' && gender != ''){
                    var options = getoptions(cat, gender);
                    weight_update.empty().append(options);
                    weight_update.removeAttr('disabled');
                }
                else weight_update.attr('disabled', true);


                
            });


            function getoptions(cat, gender){
                var weight = null;

                var options = '<option value="">Select Category</option>';

                if(cat != '' && gender != ''){
                    if(cat == 'Junior Cadet (8-10yr)' && gender == 'Male') weight =  juniorCadetMale;
                    else if(cat == 'Cadet (11-13yr)' && gender == 'Male') weight =  cadetMale;
                    else if(cat == 'Junior Youth (14-15yr)' && gender == 'Male') weight =  juniorYouthMale;
                    else if(cat == 'Youth (16-18yr)' && gender == 'Male') weight =  youthMale;
                    else if(cat == 'Senior (19+yr)' && gender == 'Male') weight =  seniorMale;
                    else if(cat == 'Pro-AM (19+yr)' && gender == 'Male') weight =  proAmMale;
                    else if(cat == 'Professional (19+yr)' && gender == 'Male') weight =  professionalMale;
                    else if(cat == 'Dab-Thai' && gender == 'Male') weight =  professionalMale;


                    else if(cat == 'Junior Cadet (8-10yr)' && gender == 'Female') weight =  juniorCadetFemale;
                    else if(cat == 'Cadet (11-13yr)' && gender == 'Female') weight =  cadetFemale;
                    else if(cat == 'Junior Youth (14-15yr)' && gender == 'Female') weight =  juniorYouthFemale;
                    else if(cat == 'Youth (16-18yr)' && gender == 'Female') weight =  youthFemale;
                    else if(cat == 'Senior (19+yr)' && gender == 'Female') weight =  seniorFemale;
                    else if(cat == 'Pro-AM (19+yr)' && gender == 'Female') weight =  proAmFemale;
                    else if(cat == 'Professional (19+yr)' && gender == 'Female') weight =  professionalFemale;
                    else if(cat == 'Dab-Thai' && gender == 'Female') weight =  professionalFemale;

                    for(var i=0; i<weight.length; i++){
                        options += "<option value = '"+weight[i]+"'>"+weight[i]+" Kg's</option>"
                    }
                }
                return options;
            }
        });
    </script>

@endsection
