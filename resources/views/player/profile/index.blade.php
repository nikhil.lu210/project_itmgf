@extends('layouts.player.app')

@section('page_title', '| Dashboard')

@section('stylesheet_links')
{{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Blank Page</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="/">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Blank Page</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->

<div class="row">
    <div class="col-md-4 col-lg-4">

        <section class="panel">
            <div class="panel-body">
                <div class="thumb-info mb-md">
                    @if($player)
                    <img src="data:image/png;base64,{{ $player->avatar }}" class="rounded img-responsive" alt="Image Not Found">
                    @endif
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">{{ Auth::user()->first_name." ".Auth::user()->last_name }}</span>
                        @if($player)<span class="thumb-info-type">Age: {{ $player->age }} Year's</span>@endif
                    </div>
                </div>

                <div class="widget-toggle-expand mb-md">
                    <div class="widget-content-expanded">
                        <ul class="simple-todo-list">
                            <li><a class="text-muted" href="{{ route('player.profile.show') }}">Update Profile</a></li>
                            <li><a class="text-muted" href="{{ route('player.profile.password') }}">Update Password</a></li>
                        </ul>
                    </div>
                </div>

                <hr class="dotted short">

                <h6 class="text-muted">About</h6>
                @if($player)
                <ul>
                    <li>Full Name: {{ Auth::user()->first_name." ".Auth::user()->last_name }}</li>
                    <li>Date Of Birth: {{ $player->date_of_birth }}</li>
                    <li>Age: {{ $player->age }} Year's</li>
                    <li>Gender: {{ $player->gender }}</li>
                    <li>Country: {{ $player->country }}</li>
                </ul>
                @endif
            </div>
        </section>

    </div>

    <div class="col-md-8 col-lg-8">
        <div class="tab-content">
            <div id="overview" class="tab-pane active">

                <h4 class="mb-xlg">Timeline</h4>

                <div class="timeline timeline-simple mt-xlg mb-md">
                        @foreach($tournaments as $date => $tournament)
                        <div class="tm-body">
                            <div class="tm-title">
                                <h3 class="h5 text-uppercase">{{ $date }}</h3>
                            </div>
                            <ol class="tm-items">
                                <?php $count = false;?>
                                @foreach ($tournament as $data)
                                @if(empty($data->assignedPlayers[0]) ) <?php continue; ?> @endif
                                @if(!empty($data->assignedPlayers[0]) ) <?php $count = true; ?> @endif
                                <li>
                                    <div class="tm-box">
                                        <h4 class="mb-none title">
                                            <b>{{ $data->name }}</b>
                                        </h4>
                                        <p class="text-muted mb-none date">Date: {{ $data->starting_date }}</p>
                                        <hr>
                                        <p>{{ $data->note }}</p>
                                        <div class="row b-top">
                                            <div class="col-md-12 text-left">
                                                    <a href="{{ route('player.tournament.my.show', ['id' => $data->id]) }}" class="btn btn-primary btn-sm">Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @if($count == false)
                                <li>
                                    <div class="tm-box">
                                        <h4 class="mb-none title">
                                            <b>Don't have any Tournament in this month for You</b>
                                        </h4>
                                    </div>
                                </li>
                                @endif

                            </ol>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
{{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
