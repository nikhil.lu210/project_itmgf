@extends('layouts.player.app')

@section('page_title', '| Tournaments | My Tournaments')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>My Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('player.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <span>My Tournaments</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="tab-content">
            <div id="overview" class="tab-pane active">

                <h4 class="mb-xlg">My Tournaments</h4>

                <div class="timeline timeline-simple mt-xlg mb-md">
                    @foreach($tournaments as $date => $tournament)
                        <div class="tm-body">
                            <div class="tm-title">
                                <h3 class="h5 text-uppercase">{{ $date }}</h3>
                            </div>
                            <ol class="tm-items">

                                <?php $count = false;?>
                                @foreach ($tournament as $data)
                                    @if(empty($data->assignedPlayers[0]) )
                                        <?php continue; ?>
                                    @endif
                                    @if(!empty($data->assignedPlayers[0]) && $count == false )
                                        <?php $count = true; ?>
                                    @endif
                                    <li>
                                        <div class="tm-box">
                                            <h4 class="mb-none title">
                                                <b>{{ $data->name }}</b>
                                            </h4>
                                            <p class="text-muted mb-none date">Date: {{ $data->starting_date }}</p>
                                            <hr>
                                            <p>{{ $data->note }}</p>
                                            <div class="row b-top">
                                                <div class="col-md-12 text-left">
                                                    <a href="{{ route('player.tournament.my.show', ['id' => $data->id]) }}" class="btn btn-primary btn-sm">Details</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach

                                @if($count == false)
                                    <li>
                                        <div class="tm-box">
                                            <h4 class="mb-none title">
                                                <b>Don't have any Tournament in this month for You</b>
                                            </h4>
                                        </div>
                                    </li>
                                @endif

                            </ol>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
