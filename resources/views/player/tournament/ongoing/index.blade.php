@extends('layouts.player.app')

@section('page_title', '| Tournaments | Ongoing Tournaments')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')

<header class="page-header">
    <h2>Ongoing Tournaments</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('player.dashboard.index') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>

            <li>
                <span>Tournaments</span>
            </li>

            <li>
                <span>Ongoing Tournaments</span>
            </li>
        </ol>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="tab-content">
            <div id="overview" class="tab-pane active">

                <h4 class="mb-xlg">Ongoing Tournaments</h4>

                <div class="timeline timeline-simple mt-xlg mb-md">
                    @foreach ($tournaments as $month => $tournament)
                        <div class="tm-body">
                            <div class="tm-title">
                                <h3 class="h5 text-uppercase">{{ $month }}</h3>
                            </div>
                            <ol class="tm-items">
                                @foreach($tournament as $data)
                                <li>
                                    <div class="tm-box">
                                        <h4 class="mb-none title"><b>{{ $data->name }}</b></h4>
                                        <?php $date = new DateTime($data->starting_date); ?>
                                        <p class="text-muted mb-none date">Date: {{ $date->format('d M Y') }}</p>
                                        <hr>
                                        <p>{{ $data->note }}</p>
                                        <a href="{{ route('player.tournament.ongoing.show', ['id' => $data->id]) }}" class="btn btn-primary btn-xs">Details</a>
                                    </div>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
<!-- end: page -->

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

@endsection
