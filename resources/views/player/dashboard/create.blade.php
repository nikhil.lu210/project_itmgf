@extends('layouts.player.app')

@section('page_title', '| Dashboard')

@section('stylesheet_links')
    {{--  External CSS  --}}

@endsection

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .mt-2{
            margin-top: 2%;
        }
        .mt-12{
            margin-top: 12%;
        }
        .mt-52{
            margin-top: 52%;
        }
        .h-100vh {
            height: 100vh;
            position: relative;
        }

        .ver-cen {
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }

        .plr{
            background-color: #dddddd;
            border-left: 3px solid #aaaaaa;
            padding: 5px 10px;
            margin: 5px;
        }

        .match{
            margin-top: 10%;
            margin-bottom: 10%;
        }
        .match:last-child{
            margin-bottom: 0px;
        }
        .match:first-child{
            margin-top: 0px;
        }

        .winner{
            border-left: 3px solid green !important;
        }
        .looser{
            border-left: 3px solid red !important;
        }
    </style>
@endsection

@section('content')

<header class="page-header">
    <h2>Blank Page</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="/">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Blank Page</span></li>
        </ol>
    </div>
</header>

<section class="section mt-52">
    <div class="container">
        <div class="row text-center h-100vh" id="match_graph">
            {{-- <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 ver-cen">
                <div class="match">
                    <div class="plr player-one" data-toggle="tooltip" data-placement="top" title="BANGLADESH | 23">
                        <h4><b>Player 01</b></h4>
                    </div>
                    <div class="plr player-two">
                        <h4><b>Player 02</b></h4>
                    </div>
                </div>
                <div class="match">
                    <div class="plr player-one">
                        <h4><b>Player 01</b></h4>
                    </div>
                    <div class="plr player-two">
                        <h4><b>Player 02</b></h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 ver-cen">
                <div class="match">
                    <div class="plr player-one" data-toggle="tooltip" data-placement="top" title="BANGLADESH | 23">
                        <h4><b>Player 01</b></h4>
                    </div>
                    <div class="plr player-two">
                        <h4><b>Player 02</b></h4>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>

@endsection


@section('script_links')
    {{--  External Javascript  --}}

@endsection

@section('scripts')
    {{--  External Javascript  --}}

    
    <script>
        $( document ).ready(function() {
            $.ajax({
                method: 'GET',
                url: 'test/json',
                success: function(data) {
                    var match_graph = $('#match_graph');
                    var str = "<div class='row'>";
                    let round = Object.keys(data).length;
                    for(var i =1; i<=round; i++){
                        str += "<div class='col-sm-"+parseInt(12/round)+" col-md-"+parseInt(12/round)+" col-lg-"+parseInt(12/round)+" col-xl-"+parseInt(12/round)+" ver-cen'>";

                        for(var j = 0; j< data[i].length; j++){
                            console.log(data[i][j]);
                            var country1 = (data[i][j].player_one != null)?(data[i][j].player_one.player.country+ " | " +data[i][j].player_one.player.weight):"";

                            var name1 = (data[i][j].player_one != null)?data[i][j].player_one.first_name:"Not Assigned";
                            var name2 = (data[i][j].player_two != null)?data[i][j].player_two.first_name:"Not Assigned";

                            var country2 = (data[i][j].player_two != null)?(data[i][j].player_two.player.country+ " | " +data[i][j].player_two.player.weight):"";

                            var player1, player2;
                            if(data[i][j].match_status == 1){
                                player1 = (data[i][j].winner == data[i][j].player_one.id) ? "winner":"looser";
                                player2 = (data[i][j].winner == data[i][j].player_two.id) ? "winner":"looser";
                            } else{
                                player1 = "";
                                player2 = "";
                            }

                            str += "<div class='match'><div class='plr player-one "+player1+"' data-toggle='tooltip' data-placement='top' title='"+country1+"'><h4><b>"+name1+"</b></h4></div><div class='plr player-two "+player2+"' data-toggle='tooltip' data-placement='top' title='"+country2+"'><h4><b>"+name2+"</b></h4></div></div>";
                        }

                        str += "</div>"; //close column div
                    }

                    str += "</div>"; //close row div

                    match_graph.empty().append(str);
                },
                error: function() {
                    console.log("player not found!!!");
                }
            });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });
    </script>

@endsection
