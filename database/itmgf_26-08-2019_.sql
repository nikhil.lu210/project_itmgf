CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '4',
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `players` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` bigint(20) unsigned NOT NULL,
  `date_of_birth` date NOT NULL,
  `weight` decimal(4,2) NOT NULL,
  `age` int(11) NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `airlines` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flight_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_date` date NOT NULL,
  `arrival_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departure_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `avatar` mediumblob,
  `passport_avatar` mediumblob,
  PRIMARY KEY (`id`),
  KEY `players_player_id_foreign` (`player_id`),
  CONSTRAINT `players_player_id_foreign` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `tournaments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_number` tinyint(4) NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` decimal(4,2) NOT NULL,
  `starting_date` date NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_round` tinyint(4) NOT NULL,
  `manager_id` bigint(20) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '-1',
  `set_matches` tinyint(4) NOT NULL DEFAULT '-1',
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournaments_manager_id_foreign` (`manager_id`),
  CONSTRAINT `tournaments_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `assigned_editors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `editor_id` bigint(20) unsigned NOT NULL,
  `tournament_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_editors_editor_id_foreign` (`editor_id`),
  KEY `assigned_editors_tournament_id_foreign` (`tournament_id`),
  CONSTRAINT `assigned_editors_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_editors_tournament_id_foreign` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `matches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` bigint(20) unsigned NOT NULL,
  `player_one` bigint(20) unsigned DEFAULT NULL,
  `player_two` bigint(20) unsigned DEFAULT NULL,
  `match_number` tinyint(4) NOT NULL,
  `round_number` tinyint(4) NOT NULL,
  `round_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `match_status` tinyint(4) NOT NULL DEFAULT '-1',
  `match_date` date DEFAULT NULL,
  `match_time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `match_venue` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winner` int(11) DEFAULT NULL,
  `looser` int(11) DEFAULT NULL,
  `editor_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `matches_tournament_id_foreign` (`tournament_id`),
  KEY `matches_player_one_foreign` (`player_one`),
  KEY `matches_player_two_foreign` (`player_two`),
  KEY `matches_editor_id_foreign` (`editor_id`),
  CONSTRAINT `matches_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matches_player_one_foreign` FOREIGN KEY (`player_one`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matches_player_two_foreign` FOREIGN KEY (`player_two`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matches_tournament_id_foreign` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `assigned_players` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` bigint(20) unsigned NOT NULL,
  `player_id` bigint(20) unsigned NOT NULL,
  `editor_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_players_tournament_id_foreign` (`tournament_id`),
  KEY `assigned_players_player_id_foreign` (`player_id`),
  KEY `assigned_players_editor_id_foreign` (`editor_id`),
  CONSTRAINT `assigned_players_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_players_player_id_foreign` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_players_tournament_id_foreign` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE
);





  



CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);




INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_100000_create_password_resets_table', 1),
	(2, '2019_07_30_133808_create_roles_table', 1),
	(3, '2019_07_31_000000_create_users_table', 1),
	(4, '2019_08_03_093541_create_players_table', 1),
	(5, '2019_08_03_093654_create_tournaments_table', 1),
	(6, '2019_08_03_094149_create_assigned_editors_table', 1),
	(7, '2019_08_03_094327_create_assigned_players_table', 1),
	(8, '2019_08_03_094442_create_matches_table', 1);
  


CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
);




  



INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Super_Admin', 'super_admin', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL),
	(2, 'Manager', 'manager', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL),
	(3, 'Editor', 'editor', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL),
	(4, 'Player', 'player', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL);
  


  
INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `email`, `mobile`, `email_verified_at`, `password`, `remember_token`, `approved_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Super', 'Admin', 'superadmin@mail.com', NULL, NULL, '$2y$10$4PFZKnf3P94cyZLPFZ8M8.8T/uvUfpoazEt4UKSdD53fA5fyq5P.m', NULL, '2019-08-26 09:19:14', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL),
	(2, 2, 'Manager', 'Jhon', 'manager@mail.com', NULL, NULL, '$2y$10$cz7tfiga4Iq5jrNXHB9emeFRuAf4S35i6EJOVNJPp3/dkiDnWuqGK', NULL, '2019-08-26 09:19:14', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL),
	(3, 3, 'Editor', 'Jhon', 'editor@mail.com', NULL, NULL, '$2y$10$cdI.H2bogkos3AVGIyMVxO5fq9K4KThgbB7tirVS30dH0xHPpv.oq', NULL, '2019-08-26 09:19:14', '2019-08-26 09:19:14', '2019-08-26 09:19:14', NULL),
	(4, 4, 'Player', 'Jhon', 'player@mail.com', NULL, NULL, '$2y$10$xG32rC/bJxwbdsXu/STJueFF9y9Esb7nx8WFtpgfC6DjnkxMBeJIK', NULL, '2019-08-26 09:41:11', '2019-08-26 09:19:15', '2019-08-26 09:41:11', NULL),
	(5, 4, 'Richie Hansen Sr.', 'Mr. Theodore Simonis PhD', 'darwin.carroll@example.com', NULL, '2019-08-26 09:24:54', '$2y$10$/sNTAshtjK6Y0BjlpeXOPO.BQnQFkFMcASfSt1mJD2B6RhZwrRPaS', 'MB0fytc7Y4', '2019-08-26 09:35:50', '2019-08-26 09:24:56', '2019-08-26 09:35:50', NULL),
	(6, 4, 'Edward Bogisich', 'Queen Zieme', 'rau.sylvester@example.net', NULL, '2019-08-26 09:24:54', '$2y$10$kDbWqtUy9q8C79bI7icj8O/KdgIuXxDl32cozYXF2RYI9jWSxDcTu', 'vjM4PCGdH9', '2019-08-26 09:24:54', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(7, 4, 'Prof. Jarrett Runolfsson', 'Miss Angelita Beier', 'fklocko@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$CaNg6Kqf.JUbGvFsfiu3COzezRmpxYTYl17/NNbrBV11CmhBNGns6', 'j3X4PFYCD8', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(8, 4, 'Erling Goldner', 'Prof. Gerson Gottlieb Sr.', 'augustus.schmitt@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$DouYcS72pqMEq.A63sQKMOphRj88avTXApq4ThDXBMsng6QLeeiNO', 'mAvYqTitcw', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(9, 4, 'Mrs. Jordane Reichert', 'Sigurd Barton', 'gregorio93@example.org', NULL, '2019-08-26 09:24:55', '$2y$10$BIIsJE2Mso2mw/uiWdZzHeEaJVUCjs7LglQW8MdG6IJLpOYmlKeKy', 'hOQm9v0950', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(10, 4, 'Pete Torp', 'Wiley Kuhic', 'leonardo.bednar@example.com', NULL, '2019-08-26 09:24:55', '$2y$10$twGVa2AQdjh5r5ArWTL8O.jdZqlDjGjs7Ipem1FT7eYyFMh8tRXbu', 'YzI3zqdDBb', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(11, 4, 'Dr. Loraine OKon', 'Prof. Johnnie Wyman I', 'ken.lesch@example.org', NULL, '2019-08-26 09:24:55', '$2y$10$Lx0EmzNkzoqTLGKzH1lIJeRvF0spCkwxd.1iRus0p6pckNm3m6EfG', 'E5izCCVJQL', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(12, 4, 'Ollie Barton', 'Tre Treutel', 'kmayer@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$9L1f3q2zs.iDA.DXEVLMKOhxURtF.9Ysnme8n.S2Q7M1p6b/cucsW', '7ngvPCEpQu', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(13, 4, 'Ally Heidenreich', 'Yolanda Orn DDS', 'kris.sienna@example.org', NULL, '2019-08-26 09:24:55', '$2y$10$BOsYHctqdzHXPF.ElvUHbuhG6f3nYJi43gPBGzC8ZVKcoTjjS0bsa', '3hCzzHyKv5', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(14, 4, 'Meaghan Monahan', 'Carolyn Cole', 'reid.effertz@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$D650PRfKpg9x6klNP3dn8ed9qK8owXDbETfCd33BqZSHovD9Tz8by', 'KjhGF2nKYD', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(15, 4, 'Ms. Mabelle Kling V', 'Lyric Spinka', 'kihn.abagail@example.org', NULL, '2019-08-26 09:24:55', '$2y$10$DWCwDmLYplBNI6m/LvmaCuf33qPq/PEO3L2PRuCBHHJ4VfIFrdXPG', 'W8FiTcBly0', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(16, 4, 'Thomas Tromp V', 'Prof. Marley Hilpert', 'retha27@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$vRHRCGjLyrQeYO7D6Kz.eO5ZvIijD2N0IGBZ0wMbcGlpCvtJ9fjQK', 'r46HFlvlrP', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(17, 4, 'Prof. Maxwell Stokes DDS', 'Brando Johnson', 'demarcus.prosacco@example.com', NULL, '2019-08-26 09:24:55', '$2y$10$Gcff.S4KiRRlIztpDY6O4ub/Q8Kma.8NbWl8i7wtmCZiDFl5r9zv.', 'qFTgMsusl7', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(18, 4, 'Mr. Kelvin Daugherty', 'Mr. Elwin Eichmann V', 'lakin.keyshawn@example.com', NULL, '2019-08-26 09:24:55', '$2y$10$GOxS8XBDVzsImj3mSuV3juT4lVBp.gbwqkHhLby.2rkHRvYsFh0CW', '2Bor9luXTV', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(19, 4, 'Carrie Bartell IV', 'Hugh Keebler', 'towne.naomi@example.com', NULL, '2019-08-26 09:24:55', '$2y$10$6cbLYOR1XIFDDvvadE8j2.9khgKU5ULRCqSR5/BoxWsEfE9K9Xiu6', '82tXgZFb0M', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(20, 4, 'Carlos Rippin', 'Abigayle Lemke', 'lind.mary@example.org', NULL, '2019-08-26 09:24:55', '$2y$10$u6FapLuOMeT3tPFqw2Clje8hglnLFfyY20aYpudFPYxZruZUPCKGq', '8G4vlycEFH', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(21, 4, 'Jaida Bayer', 'Marques Cassin', 'shields.blaise@example.org', NULL, '2019-08-26 09:24:55', '$2y$10$snfAiTPVxLcztikVe2CGneH9JKhmR/khs09oP5FMidPXNm2sHe40u', '9GdxmQG852', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(22, 4, 'Prof. Conrad Bailey', 'Jordyn Lehner DVM', 'royal.heidenreich@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$/GQrSr85BNZ0f4wdcZ8c9eOnsYL8rY1BpJesFgezHrsST51QEKIEC', 'lwfD8jkeYO', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL),
	(23, 4, 'Miss Karina Feest', 'Providenci Boyer', 'nbogisich@example.net', NULL, '2019-08-26 09:24:55', '$2y$10$nGcDpiNUTMCMGjNzWqcANOSQZnUHw020o5vM5pMbc0occMytb1jui', '1jbPhakMlI', '2019-08-26 09:24:55', '2019-08-26 09:24:56', '2019-08-26 09:24:56', NULL);



INSERT INTO `players` (`id`, `player_id`, `date_of_birth`, `weight`, `age`, `category`, `country`, `gender`, `mobile`, `email`, `title`, `airlines`, `flight_number`, `arrival_date`, `arrival_time`, `departure_date`, `created_at`, `updated_at`, `deleted_at`, `avatar`, `passport_avatar`) VALUES
	(1, 4, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:08', '2019-08-26 09:25:08', NULL, NULL, NULL),
	(2, 5, '2019-08-26', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:08', '2019-08-26 09:25:08', NULL, NULL, NULL),
	(3, 6, '2019-08-26', 22.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:08', '2019-08-26 09:25:08', NULL, NULL, NULL),
	(4, 7, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:08', '2019-08-26 09:25:08', NULL, NULL, NULL),
	(5, 8, '2019-08-26', 22.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(6, 9, '2019-08-26', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(7, 10, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(8, 11, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(9, 12, '2019-08-26', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(10, 13, '2019-08-26', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(11, 14, '2019-08-26', 22.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(12, 15, '2019-08-26', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(13, 16, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(14, 17, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(15, 18, '2019-08-26', 22.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(16, 19, '2019-08-26', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(17, 20, '2019-08-26', 22.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(18, 21, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(19, 22, '2019-08-26', 22.00, 10, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL),
	(20, 23, '2019-08-26', 22.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Male', '01737864642', NULL, 'VIP', '6541546sgsdgd', '6541546sgsdgd', '2019-08-26', '05:08 PM', '2019-08-26', '2019-08-26 09:25:09', '2019-08-26 09:25:09', NULL, NULL, NULL);

  
  


INSERT INTO `tournaments` (`id`, `name`, `player_number`, `category`, `weight`, `starting_date`, `gender`, `max_round`, `manager_id`, `status`, `set_matches`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Demo Tournament 2019', 16, 'Junior Cadet (8-10yr)', 22.00, '2019-08-27', 'Male', 4, 2, -1, 1, 'This is a demo Tournament', '2019-08-26 09:34:35', '2019-08-26 09:43:48', NULL);


INSERT INTO `assigned_editors` (`id`, `editor_id`, `tournament_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 3, 1, '2019-08-26 09:34:35', '2019-08-26 09:34:35', NULL);
  


INSERT INTO `matches` (`id`, `tournament_id`, `player_one`, `player_two`, `match_number`, `round_number`, `round_name`, `match_status`, `match_date`, `match_time`, `match_venue`, `winner`, `looser`, `editor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 4, 5, 1, 1, NULL, -1, '2019-08-27', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(2, 1, 6, 7, 2, 1, NULL, -1, '2019-08-28', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(3, 1, 8, 9, 3, 1, NULL, -1, '2019-08-29', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(4, 1, 10, 11, 4, 1, NULL, -1, '2019-08-30', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(5, 1, 12, 13, 5, 1, NULL, -1, '2019-08-31', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(6, 1, 14, 15, 6, 1, NULL, -1, '2019-09-01', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(7, 1, 16, 17, 7, 1, NULL, -1, '2019-09-02', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(8, 1, 18, 19, 8, 1, NULL, -1, '2019-09-03', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(9, 1, NULL, NULL, 9, 2, NULL, -1, '2019-09-04', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(10, 1, NULL, NULL, 10, 2, NULL, -1, '2019-09-05', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(11, 1, NULL, NULL, 11, 2, NULL, -1, '2019-09-06', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(12, 1, NULL, NULL, 12, 2, NULL, -1, '2019-09-07', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:48', '2019-08-26 09:43:48', NULL),
	(13, 1, NULL, NULL, 13, 3, NULL, -1, '2019-09-08', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:48', '2019-08-26 09:43:48', NULL),
	(14, 1, NULL, NULL, 14, 3, NULL, -1, '2019-09-09', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:48', '2019-08-26 09:43:48', NULL),
	(15, 1, NULL, NULL, 15, 4, NULL, -1, '2019-09-10', '3:45 PM', 'Demo', NULL, NULL, 3, '2019-08-26 09:43:48', '2019-08-26 09:43:48', NULL);


INSERT INTO `assigned_players` (`id`, `tournament_id`, `player_id`, `editor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 4, 3, '2019-08-26 09:43:46', '2019-08-26 09:43:46', NULL),
	(2, 1, 5, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(3, 1, 6, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(4, 1, 7, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(5, 1, 8, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(6, 1, 9, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(7, 1, 10, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(8, 1, 11, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(9, 1, 12, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(10, 1, 13, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(11, 1, 14, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(12, 1, 15, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(13, 1, 16, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(14, 1, 17, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(15, 1, 18, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL),
	(16, 1, 19, 3, '2019-08-26 09:43:47', '2019-08-26 09:43:47', NULL);


