<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'first_name' => 'Super',
            'last_name' => 'Admin',
            // 'username' => 'superadmin',
            'email' => 'superadmin@mail.com',
            'password' => bcrypt('12345678'),
            'approved_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'first_name' => 'Manager',
            'last_name' => 'Jhon',
            // 'username' => 'manager',
            'email' => 'manager@mail.com',
            'password' => bcrypt('12345678'),
            'approved_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '3',
            'first_name' => 'Editor',
            'last_name' => 'Jhon',
            // 'username' => 'editor',
            'email' => 'editor@mail.com',
            'password' => bcrypt('12345678'),
            'approved_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '4',
            'first_name' => 'Player',
            'last_name' => 'Jhon',
            // 'username' => 'player',
            'email' => 'player@mail.com',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
