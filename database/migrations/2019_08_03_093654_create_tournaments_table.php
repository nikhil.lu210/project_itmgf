<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->tinyInteger('player_number');
            $table->string('category');
            $table->decimal('weight', 4, 2);
            $table->date('starting_date');
            $table->string('gender');
            $table->tinyInteger('max_round');

            $table->bigInteger('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('users')->onDelete('cascade');

            $table->tinyInteger('status')->default(-1);
            $table->tinyInteger('set_matches')->default(-1);

            $table->text('note');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');

        Schema::table("tournaments", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
