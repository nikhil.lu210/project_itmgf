<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

            $table->bigInteger('player_one')->unsigned()->nullable();
            $table->foreign('player_one')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('player_two')->unsigned()->nullable();
            $table->foreign('player_two')->references('id')->on('users')->onDelete('cascade');

            $table->tinyInteger('match_number');
            $table->tinyInteger('round_number');
            $table->string('round_name')->nullable();
            $table->tinyInteger('match_status')->default(-1);
            $table->date('match_date')->nullable();
            $table->string('match_time', 50)->nullable();
            $table->string('match_venue', 50)->nullable();

            $table->integer('winner')->nullable();
            $table->integer('looser')->nullable();

            $table->bigInteger('editor_id')->unsigned();
            $table->foreign('editor_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
        Schema::table("matches", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
