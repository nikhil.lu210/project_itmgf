<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('player_id')->unsigned();
            $table->foreign('player_id')->references('id')->on('users')->onDelete('cascade');

            $table->date('date_of_birth');
            $table->decimal('weight', 4, 2);
            $table->integer('age');
            $table->string('category');
            $table->string('country');
            $table->string('gender');
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->string('title');
            
            $table->string('airlines');
            $table->string('flight_number');
            $table->date('arrival_date');
            $table->string('arrival_time');
            $table->date('departure_date');

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE players ADD avatar MEDIUMBLOB");
        DB::statement("ALTER TABLE players ADD passport_avatar MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
        Schema::table("players", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
