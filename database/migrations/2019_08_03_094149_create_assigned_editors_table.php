<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedEditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_editors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('editor_id')->unsigned();
            $table->foreign('editor_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_editors');
        Schema::table("assigned_editors", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
