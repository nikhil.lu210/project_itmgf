CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '4',
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS `players` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` bigint(20) unsigned NOT NULL,
  `date_of_birth` date NOT NULL,
  `weight` decimal(4,2) NOT NULL,
  `age` int(11) NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `airlines` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flight_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_date` date NOT NULL,
  `arrival_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departure_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `avatar` mediumblob,
  `passport_avatar` mediumblob,
  PRIMARY KEY (`id`),
  KEY `players_player_id_foreign` (`player_id`),
  CONSTRAINT `players_player_id_foreign` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS `tournaments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_number` tinyint(4) NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` decimal(4,2) NOT NULL,
  `starting_date` date NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_round` tinyint(4) NOT NULL,
  `manager_id` bigint(20) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '-1',
  `set_matches` tinyint(4) NOT NULL DEFAULT '-1',
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tournaments_manager_id_foreign` (`manager_id`),
  CONSTRAINT `tournaments_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS `assigned_editors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `editor_id` bigint(20) unsigned NOT NULL,
  `tournament_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_editors_editor_id_foreign` (`editor_id`),
  KEY `assigned_editors_tournament_id_foreign` (`tournament_id`),
  CONSTRAINT `assigned_editors_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_editors_tournament_id_foreign` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE
);



CREATE TABLE IF NOT EXISTS `matches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` bigint(20) unsigned NOT NULL,
  `player_one` bigint(20) unsigned DEFAULT NULL,
  `player_two` bigint(20) unsigned DEFAULT NULL,
  `match_number` tinyint(4) NOT NULL,
  `round_number` tinyint(4) NOT NULL,
  `round_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `match_status` tinyint(4) NOT NULL DEFAULT '-1',
  `match_date` date DEFAULT NULL,
  `match_time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `match_venue` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winner` int(11) DEFAULT NULL,
  `looser` int(11) DEFAULT NULL,
  `editor_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `matches_tournament_id_foreign` (`tournament_id`),
  KEY `matches_player_one_foreign` (`player_one`),
  KEY `matches_player_two_foreign` (`player_two`),
  KEY `matches_editor_id_foreign` (`editor_id`),
  CONSTRAINT `matches_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matches_player_one_foreign` FOREIGN KEY (`player_one`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matches_player_two_foreign` FOREIGN KEY (`player_two`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matches_tournament_id_foreign` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE
);





CREATE TABLE IF NOT EXISTS `assigned_players` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tournament_id` bigint(20) unsigned NOT NULL,
  `player_id` bigint(20) unsigned NOT NULL,
  `editor_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_players_tournament_id_foreign` (`tournament_id`),
  KEY `assigned_players_player_id_foreign` (`player_id`),
  KEY `assigned_players_editor_id_foreign` (`editor_id`),
  CONSTRAINT `assigned_players_editor_id_foreign` FOREIGN KEY (`editor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_players_player_id_foreign` FOREIGN KEY (`player_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_players_tournament_id_foreign` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE CASCADE
);







CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);






CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
);









INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Super_Admin', 'super_admin', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL),
	(2, 'Manager', 'manager', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL),
	(3, 'Editor', 'editor', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL),
	(4, 'Player', 'player', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL);


INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `email`, `mobile`, `email_verified_at`, `password`, `remember_token`, `approved_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Super', 'Admin', 'superadmin@mail.com', NULL, NULL, '$2y$10$dR/wVyvfrE1K3nt7tOuphes2e7MTKFjOLGhtydYQch8SaKU8wgLwm', NULL, '2019-09-16 08:04:42', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL),
	(2, 2, 'Manager', 'Jhon', 'manager@mail.com', NULL, NULL, '$2y$10$2OuiKbasfDya4vjd4PlwNuiGGH6mAv4MBIB6NoCyO5eh1WCYvowLa', NULL, '2019-09-16 08:04:42', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL),
	(3, 3, 'Editor', 'Jhon', 'editor@mail.com', NULL, NULL, '$2y$10$R75.0FT6dCQwBf6oWQpSUemZKOOr9xxN2eRIxZTiO0mjATjpKDJWm', NULL, '2019-09-16 08:04:42', '2019-09-16 08:04:42', '2019-09-16 08:04:42', NULL),
	(4, 4, 'Player', 'Jhon', 'player@mail.com', NULL, NULL, '$2y$10$0l9HYoJL18rwzzoa5mnanOSQ1BlZSTXfHaEd/iW2C/.pxPEN8vM5G', NULL, '2019-09-16 08:26:16', '2019-09-16 08:04:42', '2019-09-16 08:26:16', NULL),
	(5, 4, 'Gene Miller', 'Mr. Bud Jones', 'colt46@example.net', NULL, '2019-09-16 08:17:10', '$2y$10$F0NfoLA/q9QLqEQRWaIV7.BFD5FcsZK0ZhQxDXZ7JLJvn5qvzXKFu', 'MRK3s8tQJS', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(6, 4, 'Miss Alessandra Dare', 'Toby Schultz', 'windler.lauriane@example.net', NULL, '2019-09-16 08:17:10', '$2y$10$lItF0lxxrKjEvVQpvp7DsOZtHJ3.Xj56SAYPgPqDxGZRuZug/WYVG', '1vmJJDx3vs', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(7, 4, 'Luz Bernhard', 'Mr. Donny Pfannerstill', 'oceane.kuhic@example.net', NULL, '2019-09-16 08:17:10', '$2y$10$D0t/On6lvI3aM.guFVOu2OSN3lofMVesU25ai5cQloGUMe3re.bC6', 'OiRd0e18G9', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(8, 4, 'Dandre Breitenberg DDS', 'Riley Medhurst', 'uzieme@example.com', NULL, '2019-09-16 08:17:10', '$2y$10$bmm/wRBi6inXlHMIRcCEE.pnO49dQY7P6KeIBgof33YtnzZ/vJfVi', 'TFebKCllsU', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(9, 4, 'Dr. Robb Mills DDS', 'Ms. Rachel Adams', 'lindsay.luettgen@example.org', NULL, '2019-09-16 08:17:10', '$2y$10$qgQteRhqEMe28.uf9ewUHuu7N8e9OmkEsNZrgxg6tmfphxLz/Yx2m', '15iqJlGtf5', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(10, 4, 'Rebecca Mills', 'Opal Keeling', 'ferry.keshawn@example.com', NULL, '2019-09-16 08:17:10', '$2y$10$SdsVcS.J1Tag4iGHoNzEy.4vfCLfAC4AAzHBI3Gtpgyi82A8CN/L6', '1yMIaTHBTf', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(11, 4, 'Dr. Brent Bogisich', 'Prof. Dwight Moore MD', 'xhahn@example.net', NULL, '2019-09-16 08:17:10', '$2y$10$hFZwmHsQfvv303EMaZro7eKqDbKAB7p/OVzDwiEdsPwJ4GrWbooZm', 'xqI5YNXdXH', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(12, 4, 'Efrain Lueilwitz MD', 'Prof. Joey Trantow', 'plangworth@example.com', NULL, '2019-09-16 08:17:10', '$2y$10$UV6JDsaj0h1aYIghWV6sg.tmtrU2RQrKyzkIh8NAbV.nAfzcNx3FK', 'PH8kHE9An9', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(13, 4, 'Leanne Johnston III', 'Jalyn Medhurst', 'kstiedemann@example.org', NULL, '2019-09-16 08:17:10', '$2y$10$DTmSoFEAI/O06toctukcX.whWSt9X80Suu3ZJsqsPQV08by3wrxCC', 'nghG2mzoeI', '2019-09-16 08:17:10', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(14, 4, 'Brennon Jakubowski', 'Fredrick Harvey', 'wilkinson.lessie@example.net', NULL, '2019-09-16 08:17:11', '$2y$10$6a9dyVLzIwqhamotKcoiLOoDz/HVBI8eoVTcL07385klK3pamG2f6', 'BYwJ7ZRSw8', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(15, 4, 'Perry Mann III', 'Elisha Cronin', 'ikovacek@example.net', NULL, '2019-09-16 08:17:11', '$2y$10$Ug1AJVAUgx1ZaOIXgnkiXu/79NFW1GugJHJRaM1tvA3mp79tnzHr.', 'NoKbGSCcEe', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(16, 4, 'Dr. Adonis Pfeffer', 'Kelly Eichmann', 'aditya48@example.net', NULL, '2019-09-16 08:17:11', '$2y$10$cSxdOFaJ.CCk7wZ0VrpN9OuGhb6AP5k2JdGXNn1F16QjW4u9QZo/2', 'f0iemjFCa6', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(17, 4, 'Cicero Pfeffer', 'Deven Koelpin', 'delores.lubowitz@example.org', NULL, '2019-09-16 08:17:11', '$2y$10$ctXrxM8tDM.9kFx1JHng9OwaLjShX4uYCAKi34KUSe8YRhxugR9FO', 'k2v91aRX2I', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(18, 4, 'Jillian Hermiston', 'Rey Hodkiewicz DVM', 'kbins@example.net', NULL, '2019-09-16 08:17:11', '$2y$10$W4s5jxW6ip.EubmyUSE3F.xN2vITd/cIr6r27b8lJXUryhV8BP9h.', 'NwBkBjAwIt', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(19, 4, 'Prof. Rosanna Koss V', 'Marshall Howe', 'leif63@example.net', NULL, '2019-09-16 08:17:11', '$2y$10$qefFKdXbdg.SicBx.smFdewPHWywdDDpC94idmnqhZjpSBXDLmkZK', 'YvRCeMCiq6', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(20, 4, 'Clark Von', 'Dr. Kimberly Schowalter', 'brett69@example.com', NULL, '2019-09-16 08:17:11', '$2y$10$ighsAOqNv.CFU7J6jbUgteMGIr4XqMjHpO4Y7KR.l5UrfoTHRJ0qa', 'FY90T8BIEh', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(21, 4, 'Micheal Runolfsson', 'Hubert Goldner IV', 'jbergnaum@example.org', NULL, '2019-09-16 08:17:11', '$2y$10$zfNlVpaB2HSgSHqAwSnPfOVT1fUiEmA8yGryd/bzF1B7Ke.N3JuYS', '3yeO9WiIbv', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(22, 4, 'Daniella Cummings DDS', 'Lucas Gerhold', 'isabell48@example.com', NULL, '2019-09-16 08:17:11', '$2y$10$Hrb6HQU8PUo8O5NwEWgQw.jxzXkQo5PWo/I/NEqERKWLVyHo0YWuO', 'l288jaTGua', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(23, 4, 'Manuela Marvin', 'Rosanna Romaguera', 'mekhi.gaylord@example.com', NULL, '2019-09-16 08:17:11', '$2y$10$VdkIeODnsuqIkiB/MDP.7.kpOTVMiD.NYoxsIvJPqEPcpUejHk7he', 'IINRvdSl7K', '2019-09-16 08:17:11', '2019-09-16 08:17:16', '2019-09-16 08:17:16', NULL),
	(24, 4, 'Shane Gutkowski PhD', 'Fredrick Corkery', 'marilyne89@example.com', NULL, '2019-09-16 08:17:11', '$2y$10$Mf3L7eOxI7s74B5mhYXFH.htpjoGBbIn5HGes3n9OUV3kU79it3cC', 'uz3h7HNhCE', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(25, 4, 'Dr. Humberto Beatty Jr.', 'Torey Braun', 'destini64@example.org', NULL, '2019-09-16 08:17:11', '$2y$10$EuuRQygBVOKnhGXi5XdTou9kxssidzqJH1BEnVsjR3gHzTU.NcDCO', 'JaKhG0f8VW', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(26, 4, 'Prof. Danny Wisozk', 'Danial Murazik', 'ethel83@example.com', NULL, '2019-09-16 08:17:11', '$2y$10$bDB1wJA.ZrJ7lkBza8kEeez7.BlN0fpGvge844ugTWw7cvJ017eoa', 'sRE8P6KCI7', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(27, 4, 'Koby Spencer', 'Eduardo Hilpert', 'cecil.conroy@example.com', NULL, '2019-09-16 08:17:11', '$2y$10$p54112NUzySxSEMuk3GA1u24C2eBjIkTYVw.VPirbvPqYUH4mOF.6', 'lpk73lKANX', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(28, 4, 'Monty Brown', 'Augustus Smith', 'qmorissette@example.org', NULL, '2019-09-16 08:17:11', '$2y$10$bJ7I0GeHP5tiYR5LSi5Aku3Uw.toRmBEqKnZQ6sZrTDJGo88MB4Ta', 'myhU22ledA', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(29, 4, 'Prof. Dorothy Boehm Sr.', 'Ellen Pacocha', 'qdach@example.org', NULL, '2019-09-16 08:17:11', '$2y$10$01Cr8Z9Pg7E29G/hIM3UOuFad2jRFZgKjBLihgFMHth1PRWYWYCt6', 'TjwwV1PRvF', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(30, 4, 'Ryann Mosciski', 'Prof. Maida Koss', 'pmetz@example.net', NULL, '2019-09-16 08:17:11', '$2y$10$WCXlg6BT3Zt9hP5rNW/pHOThFXeDFmfZh3BZfAz/EHlnFgz5zum2W', '5ARBWQUtN2', '2019-09-16 08:17:11', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(31, 4, 'Keshaun Robel', 'Euna Rosenbaum I', 'ryan.marshall@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$cLiqrqR38zzcZ/O2.crEj.u6gbsc6k5kua58lFX2tvBpxOkyOypMS', 'XhoZBamBSF', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(32, 4, 'Dena Veum', 'Mr. Kole Greenholt Jr.', 'brando57@example.org', NULL, '2019-09-16 08:17:12', '$2y$10$L9abS9n4eBCMCS/PqZt5ZuqYwkeIYHU37WZ33HcEBYMmxK.BGOgmm', 's5EOiwUr2C', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(33, 4, 'Annamarie Pollich', 'Dr. Afton Rowe', 'mckenzie.emerald@example.org', NULL, '2019-09-16 08:17:12', '$2y$10$g6cEMvxt1T.bWVHBji2Kke3NVuopSo0wVWRavBXrkex8q0x1yXx5.', 'Ryye8m2Aj9', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(34, 4, 'Alena McKenzie', 'Ms. Zita Schneider', 'brandt.sawayn@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$jsIdFaOoXyUBl0eGgsONBuphNLzulFF3xSXKr4WuwYiU.eylrGfq6', 'Xhzag6ulYO', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(35, 4, 'Arnulfo Hermann', 'Karine Leffler', 'ehessel@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$b5h/D1TEnoD4edQJ0TPBYOK7DrRXg9PgiE4jMeL3NKPUpOKuA0M..', 'yRZOkeYiqM', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(36, 4, 'Dr. Cesar Spencer', 'Kieran Homenick', 'ashlynn.hintz@example.com', NULL, '2019-09-16 08:17:12', '$2y$10$/2STDft9.TSZL1rMVnRB/ec7iBtqQXumSNvQbiOrvX.60fS1LcZqS', 'IsuWf3LBng', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(37, 4, 'Bud Rau', 'Mable Sauer', 'doug.lubowitz@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$opz5nYsBEqWxN/8HntlrKOMxhdvBRJbefIYyqDOdANhK9sQUJWAN2', 'ghlG6Blgkf', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(38, 4, 'Elvis Connelly', 'Prof. Ross Lesch Sr.', 'natalia.borer@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$MGakmoUktzlnLfa2weIMV.qgOBCxQCeGb/9APw34hZND5LJM8DOga', 'Bji5ZWjfUK', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(39, 4, 'Ruth Stehr', 'Pinkie Flatley', 'buddy.streich@example.com', NULL, '2019-09-16 08:17:12', '$2y$10$8pi16jx5z/leYNZRa2XiZu3Hgr7KAeBQZw8OsxcdaPK/kd/OXR6by', 'h2qRwQXzA5', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(40, 4, 'Mr. Stephan Cruickshank I', 'Colleen Wyman', 'jayne02@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$BRWOj0Pm/MqFM8gSEbYrI.G1u.eG1xSyRqTkTxOxcyOzjV1.vOgbG', 'yw4imh9qab', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(41, 4, 'Dr. Ralph Bednar', 'Nicolas Christiansen', 'lindgren.ezequiel@example.net', NULL, '2019-09-16 08:17:12', '$2y$10$T8/mWjwU0ife1wpHKahdnu3D6ANFAyukepjCM3nTvUfEJDzTy3MSq', 'kEHmLMyHKA', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(42, 4, 'Prof. Wendell Ruecker', 'Helmer Kilback', 'mozell.little@example.com', NULL, '2019-09-16 08:17:12', '$2y$10$tKHkPg27EJuVOPd/ZL4hvOPNdywirko7glhgBC0KvxfmgBuCubFo.', 'Oqz79TsQHh', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(43, 4, 'Mr. Bart Labadie', 'Miss Monica Crona', 'joany87@example.org', NULL, '2019-09-16 08:17:12', '$2y$10$GhrekW6bl/AtVhG90/EQVu6R2aXlphK9lFuRF2txqYc.z7jkskHrm', 'KkdZcH0tvB', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(44, 4, 'Trenton Harvey', 'Helena Weber', 'euna39@example.org', NULL, '2019-09-16 08:17:12', '$2y$10$QeRvvMmnaBGlxokTQYkoo.05l1uSSqZWrd8jb4kZkhEAUtH9PA9Eq', '7WOJKduDnA', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(45, 4, 'Maxwell Jenkins Jr.', 'Prof. Adelbert Harber', 'evan.braun@example.com', NULL, '2019-09-16 08:17:12', '$2y$10$OWnCR1REILzESC8RONQNVuOCtXLHLMln8ve6xvEfCue8gQnjISyBO', 'xPUcNEGHae', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(46, 4, 'Cayla Jones', 'Jaquan Brown', 'julien.raynor@example.org', NULL, '2019-09-16 08:17:12', '$2y$10$EvwGY2VcHDVnOnG0vhc/B.VYFeyIdlM/6IPsHg/6tQWr4LNGTSA/a', 'AJmOM4ucI5', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(47, 4, 'Ms. Verdie Cummings', 'Michaela Homenick', 'dbatz@example.com', NULL, '2019-09-16 08:17:12', '$2y$10$ErssUM6379TYLeOHG3xsdephiGa6hxtmkqDK33TCHoArbKW7dVvEy', 'HWrtlZ7OX5', '2019-09-16 08:17:12', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(48, 4, 'Amaya Weber', 'Lee Greenfelder V', 'otreutel@example.com', NULL, '2019-09-16 08:17:13', '$2y$10$ZGBx1fBn84frucU611PXrutaEmh9PkIinFCL2i2Hxoc0bL2u/ZDbC', 'DfITsujOYd', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(49, 4, 'Osbaldo Funk', 'Timmy Halvorson', 'leffler.bell@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$NiQglW/Us10Rnk7koHos8.WD/co8RT06wkxbgm2n5kSLQxc1.KXoi', 'exbNbVCKM4', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(50, 4, 'Prof. Kale Dietrich', 'Thomas Wisoky', 'hills.cayla@example.org', NULL, '2019-09-16 08:17:13', '$2y$10$DeFA90LzuiQtiOtXzVF39.J2mCdP.RtWGn6o9HvX/W/dOJnKoJDfS', 'SmbhNjcQLV', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(51, 4, 'Reese Kassulke', 'Jennings Ziemann', 'alexandria25@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$V4L7X6RwxsHVjkYYhgSQnuPikk229vDDBW0yHqKEkhrWXpCF4U2aG', 'Leb2rydhRp', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(52, 4, 'Brennon Johns', 'Dr. Loraine Fisher III', 'irippin@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$8cU1TgFDw8vJmT/Jz4lw2emJZdkYX2DEOzIMajSmDYpOzvHswVzy.', 'Nu1zoXskGf', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(53, 4, 'Emerald Thiel', 'Carrie Bechtelar', 'alexandro47@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$lkaigm7oIzm.qxHmIGnfuefT3KYSMPRt5pZTTbRy9td5G9Cxgag9y', 'Dqijc5ch01', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(54, 4, 'Dr. Chester Bode', 'Jessy Gorczany', 'jamel.ernser@example.org', NULL, '2019-09-16 08:17:13', '$2y$10$S6lFpR/hLqcLrEYze1F.VerPfyecve3fTEMGquhQ3e5XPLrR21Ua6', 'oZiPo8PF4P', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(55, 4, 'Carolyne Hodkiewicz', 'Prof. Moises Willms V', 'qturner@example.org', NULL, '2019-09-16 08:17:13', '$2y$10$tIsfzJ4K2RFrxrDOVtGIKOluPRmthVLYqXQBe1qJ0SgK2KhcHcVqW', 'eK8KzerbgS', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(56, 4, 'Mr. Newell Price', 'Jaquelin Zieme', 'delia36@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$gTuh0RB.KTKlEOzt62dXfuDTPvHWNSKVRshPzRenyT9rAKrTXlp5C', 'Z1ws4yBcmE', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(57, 4, 'Gertrude Morissette', 'Dahlia Zboncak I', 'zbraun@example.com', NULL, '2019-09-16 08:17:13', '$2y$10$kMvhwXzHGTGT5.TDcEGwZu58ywY8K2Fyqrx6NFXFcRtS5kIxU86A2', 'NuC3sX3ysX', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(58, 4, 'Carroll Smith', 'Mrs. Mallie Boehm Sr.', 'mireya25@example.org', NULL, '2019-09-16 08:17:13', '$2y$10$MuyPLXe3.SGFywqG7Q0sRud8tkSQAZdPQ3tF2nqWEbR5maa4dZoni', 'lNcUpVuwgZ', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(59, 4, 'Margot Rowe', 'Quincy Murazik', 'stamm.tomas@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$KCGatXhmSSk1Um9EjYKaretnFkU72Lc5Ss4dZ01JlJPr7Fs2E6r0C', '0UOqY9rAto', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(60, 4, 'Juliana Bauch MD', 'Lamar Kassulke', 'elyssa10@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$aU1WYozAoxnDq6oKl3a5Kusj73a641hzHLeJPkcSYF4tTQgLTybVa', 'H3xWTSpIr2', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(61, 4, 'Ms. Tyra Weimann', 'Prof. Sandra Kunde DVM', 'katelyn31@example.org', NULL, '2019-09-16 08:17:13', '$2y$10$0iYhycEM2bz/e/MlCxpt6.oWj.VPnZcLRuZUPI16Qj/zS91ilARby', 'PcPjxJgicB', '2019-09-16 08:17:13', '2019-09-16 08:17:17', '2019-09-16 08:17:17', NULL),
	(62, 4, 'Johnathon Bahringer', 'Mr. Tyrese Watsica', 'nova97@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$nhW6uJvsRw3GvDZrCQSEjO.U.Py/LU7sfQ4FSa9mmoMtXEnKDGbzq', 'PxoFrd8cad', '2019-09-16 08:17:13', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(63, 4, 'Austin Denesik', 'Rylee Barton', 'kkshlerin@example.org', NULL, '2019-09-16 08:17:13', '$2y$10$J9dxn/uUPfS/avw.A9eO5OlzEIcBer.QYn91AmZRBcRgN6N6loKGS', 'MIHuzujuFt', '2019-09-16 08:17:13', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(64, 4, 'Imogene Block', 'Mrs. Jacklyn Rolfson V', 'helen.dicki@example.net', NULL, '2019-09-16 08:17:13', '$2y$10$peij7rpRW2ebfSbIMT9bbuu4nN5L/0JkiPITnX6ZmAj0x29t72aE2', 'AWY7vokjYM', '2019-09-16 08:17:13', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(65, 4, 'Elva Tillman', 'Abraham Buckridge', 'nhane@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$MItsiufM9BKO/.y4u/l8xeia.srKv/6xNfqaJD0ucZCY7IzcyD4rS', 'MEzrexg7xA', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(66, 4, 'Daniela Lindgren', 'Dr. Haleigh Pacocha IV', 'zrogahn@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$/dVD1iG19hEsfli5QW4U1.YapjOG.6Gu39xjnazPsi9Miq/9lvKjC', 'OlhivJWVws', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(67, 4, 'Genevieve Olson', 'Guillermo Homenick III', 'jamaal61@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$aa/oXmnhp8XTRx00bcUefOkpPMJTgngQ84WlMOkBWC3Al8JyAERVK', 'qg8CAcsFcd', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(68, 4, 'Kevin Rau', 'Benny Donnelly', 'ngaylord@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$t6eX8LHHm6K5UkXJVRoKtuohOUBE5.8.vMI9sDb.0jp6x3B69u39G', 'IJhEzYEYX1', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(69, 4, 'Donna Lakin I', 'Dr. Elisa Deckow Jr.', 'ernestina.hauck@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$yEJIWPiyheFixI8zGZ2k4.0rGWM9vxfbNvBODoHh8LBte4jdgHOb6', 'i4fZ64HQiV', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(70, 4, 'Prof. Mariana Schowalter', 'Dr. Brice Rohan', 'alessandro.bauch@example.org', NULL, '2019-09-16 08:17:14', '$2y$10$jmt/0F91TGVpa1r.SRKhN.l3nUr0ePLUDjIiB2.cfY1Pvg5J68SrC', 'cqeVQCR2N2', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(71, 4, 'Prof. Francesca Cummerata', 'Rowan Gaylord', 'jamey55@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$ospfmIXdWQpe22GQv4vV4eyTzdJf2q5s1ESq8mffXWCXuOio6aUc6', 'O7UPMqEKMY', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(72, 4, 'Mr. Adolph Huel', 'Miss Betsy Pollich', 'owen.gottlieb@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$h6lwHPFjaINuhUfzGA1sCOiVOuVqUO2ZvVl2ciLLqEQquZiw511ke', 'oLfEQxkC1G', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(73, 4, 'Helene Auer', 'Lon Reilly', 'grimes.dortha@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$OWonaf0x8tTe6ANRgAtJsOEQYumwJyTNJz7t72Wekx3AAcI6eXmpq', 'E1LGGahA6j', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(74, 4, 'Meredith OHara', 'Barton Koss', 'carolanne32@example.org', NULL, '2019-09-16 08:17:14', '$2y$10$9jIKWyB9Wu6bxUuScdv9A.XvAuSelxJIbzkvLnyzVBsKuwx9vfFXK', 'jDOH2b5THs', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(75, 4, 'Marcelo Volkman DVM', 'Aaliyah Bednar', 'chandler42@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$TWSt2IEWeloPrXxVeg/2TuYO5eQ.U5dYkWu5ZLdJSLkEL7jR8c0ci', 'fL7W3FTnjR', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(76, 4, 'Mrs. Queen Little', 'Prof. Desiree Schultz MD', 'huels.hadley@example.com', NULL, '2019-09-16 08:17:14', '$2y$10$j16n7gJQ9cHDySqf7eCJwu1C0YkJJJy1KcH4PjnLjLqo/dxit0o3m', 'QHRst7UF2T', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(77, 4, 'Anne Kris', 'Hugh Mann', 'wyman.geovanni@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$yT8InE01IkyCsSgXOEMrRuhD8NEE2YrMDlLlJLqFfgz4gj5gLUUNu', 'rPXGnvIM9h', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(78, 4, 'Esmeralda Pfeffer', 'Karen Botsford PhD', 'velva05@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$/nUjPbMk.JxRGVdBXvtFd.8/Hvj2wTWjSvOEgX8bIm7IUmV//GGYa', '3JOswcu5z7', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(79, 4, 'Mack Fisher', 'Prof. Abdullah Jaskolski', 'freichel@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$uuTVCXpj1Cg25KX2HMQaDOBe6fFkkq06VmcewvRsCI3D1TppIeUyC', 'cqF4QKuJHO', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(80, 4, 'Justus Greenfelder', 'Irma Kilback Sr.', 'eklocko@example.net', NULL, '2019-09-16 08:17:14', '$2y$10$xVcMdWxgz5ujt1L7uGyfKOmfzz2VZZ4ZwYxSWy0oAsPUJF9H44CWO', '1b1zaM52qc', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(81, 4, 'Turner Carter', 'Ms. Chelsea Macejkovic', 'berge.danny@example.org', NULL, '2019-09-16 08:17:14', '$2y$10$BVXBXvJbPn4VAkNB5KmaXuQVvevHdJGkn7mtrSN4W3oDzC7Af8Yja', 'nZKfKYLDBr', '2019-09-16 08:17:14', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(82, 4, 'Jacey Koss', 'Dashawn Cronin', 'milford21@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$yWp.3AWTknF5swVEqDBQfO1hOc.mGi2muhAAYKS5fbZ5l1ZR4.eju', 'BEfuoNHWiq', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(83, 4, 'Shana Durgan', 'Alexandra Rosenbaum', 'quinten.corkery@example.net', NULL, '2019-09-16 08:17:15', '$2y$10$njc4Ccn3A/yloxFY5TW5V.C3.ey05Kalo/61pBlspwWbh2zIVOB9m', 'kw69HHAFWz', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(84, 4, 'Bryce Aufderhar', 'Mr. Kane Roberts', 'pfeffer.darryl@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$NWjj2/hvaE48wxrUHQkrhudirnGNAK/576sdeS4aXJDakIjUj0EKe', 'o5B42FQ8Q0', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(85, 4, 'Mr. Celestino Hackett', 'Dagmar Mayert', 'miracle76@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$ZYnn1cwppdjAKZi.V0UP/OgBhSxDPAoLJxNhDXMORoRpupd7jFJ5e', '5owV3o20kx', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(86, 4, 'Mariela Nikolaus', 'Joaquin Wisoky Jr.', 'edgardo.langworth@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$UdX9MWFM4m8cmjQBZSfqn.EWq5Kn8CzZqF.RFpwhh.Cy97sxkk8Q.', 'BLqOa56Wug', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(87, 4, 'Prof. Walton Schuster III', 'Granville Rolfson', 'shea37@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$j7htq77bKmwIIDwtg1Lbf.gKGj7wEet62tJE01s/QLz9iYdnI9u0C', 'ZSBPatcUNw', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(88, 4, 'Mollie Schowalter', 'Dr. Marshall Connelly IV', 'schultz.evelyn@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$mUYaeOFTo7MjQ/Xcbzh14.drQihGXC0MAjL93NNbMcQJmWdaSdCoa', 'rC0pkAhc4O', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(89, 4, 'Theodora Kirlin', 'Penelope Hand', 'zmetz@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$77IzGk5wTIu2TlREUhy4jub3tfv8B8VoqeryYZw.8TLgzGwWXzOPi', 'ZM4kJbDVZw', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(90, 4, 'Prof. Terrill Stracke III', 'Dr. Gloria Bogan', 'lwalter@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$J/AuFbRM0VD5ya3.GlTEm.12b4QggjCZfGyjU1zJ5XknuQwCgxL.C', 'dBHA0ZhAgi', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(91, 4, 'Prof. Rosa Gottlieb', 'Nicolas Lebsack Jr.', 'aubrey.hane@example.net', NULL, '2019-09-16 08:17:15', '$2y$10$.iOtzNKjsH3zWfzAqT2X/.jVXLSXF4C4tB0TKaLuOqKzSjFxI8klO', 'f1jkx1zQji', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(92, 4, 'Dr. Santa Lehner II', 'Bridgette Zboncak', 'brigitte93@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$5fodqUN3qLhZXrE4HTJYy.LBYdgOs0hEYIopqfsFyK1364fLxXGMm', 'hjTKRRBIlK', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(93, 4, 'Maximo Krajcik', 'Hassan Pacocha', 'angelica.leuschke@example.com', NULL, '2019-09-16 08:17:15', '$2y$10$va3akzQwpl0fktO3FzkUxOn8TYSrZ05Zksd6.Hcjno3JufcxzwLlm', 'JwW6tAR2em', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(94, 4, 'Citlalli Moen Jr.', 'Ludie Krajcik', 'isabelle.strosin@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$Dw5SBapcAEwhm5oOZ2g3ZuL2nGXuRK6E.yfw0u8jJRtwPd3eGnMMu', 'gpw0oXg1MC', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(95, 4, 'Brooklyn Torphy II', 'Cordell Jast', 'hirthe.audie@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$QsnayQ7RqIwkq9WdgMNgu.ty0w6rmM5qq2dyABtmoRfRvjgFxCV92', 'dxvkBlj44f', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(96, 4, 'Kara Torphy', 'Prof. Will Kovacek', 'hagenes.shyann@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$H1L/Ej7xSSXFhyP/CXZSb.XA3q20irh7WgCVwWCteOJ5s951.D0Da', 'Gp0ERezdez', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(97, 4, 'Bart Rath', 'Dana Walter PhD', 'mtromp@example.net', NULL, '2019-09-16 08:17:15', '$2y$10$C.Ar2oP4mRrCRcA5L5p8D.COdn8U5Onbu8wZ6qk4yvG.Cgl4CrI0W', 'L3gdCepaGf', '2019-09-16 08:17:15', '2019-09-16 08:17:18', '2019-09-16 08:17:18', NULL),
	(98, 4, 'Dr. Amelia Ankunding DDS', 'Miss Dannie Ankunding Jr.', 'berneice46@example.org', NULL, '2019-09-16 08:17:15', '$2y$10$oTFQDuSsgHZ1unkTWA9SROmM3p6277zQxtrk9CcRmq3mwQde/u5A.', 'VDEVEqpxRG', '2019-09-16 08:17:15', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL),
	(99, 4, 'Matt Dicki', 'Mr. Tom Little', 'haley.dewayne@example.net', NULL, '2019-09-16 08:17:16', '$2y$10$1eSZmTwrOPYxXaduXSsud.SEPtJvxAZw5n8pWFmIcWElZICeTSV62', '8ZgcNSe2n8', '2019-09-16 08:17:16', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL),
	(100, 4, 'Zachariah Dickinson', 'Levi Bogan', 'helga88@example.com', NULL, '2019-09-16 08:17:16', '$2y$10$kJXz6LzJWOMfpA2igc1PbePrNnmFSIkeXzds6atVJNmzJsxpRYmy6', 'hHMC2Khb5P', '2019-09-16 08:17:16', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL),
	(101, 4, 'Oma Greenholt', 'Russ Dare', 'bo97@example.com', NULL, '2019-09-16 08:17:16', '$2y$10$OuH9hx777QTPdSiClBPcFuf97VRX7T7gx3h6xffHq3XZsmVoXyRne', 'qHSiLhoQgF', '2019-09-16 08:17:16', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL),
	(102, 4, 'Houston Harris PhD', 'Mollie Koch', 'vallie59@example.org', NULL, '2019-09-16 08:17:16', '$2y$10$S5NstBCM7tagrrRnXEyZxedMEvDy6E2NQxUSRESvzlpGr5a6xPOB6', 'UsYbJD7G6c', '2019-09-16 08:17:16', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL),
	(103, 4, 'Tatyana Orn III', 'Dr. Eleazar Tromp', 'alice.von@example.net', NULL, '2019-09-16 08:17:16', '$2y$10$fgzXEGqhkUnFmHzt9PyWmeW.YUkN5nCUEjjZxtBd2PeYD52Nvkhwa', 't2g6u8M9Ic', '2019-09-16 08:17:16', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL),
	(104, 4, 'Jevon Weimann III', 'Dr. Velva Schmidt', 'hrolfson@example.net', NULL, '2019-09-16 08:17:16', '$2y$10$2dbyxFPphp6CtSTXzgCwFus/9lyzRNpAWj.XmH2HVfBm9geLbx57i', 'xzeiqGjVaH', '2019-09-16 08:17:16', '2019-09-16 08:17:19', '2019-09-16 08:17:19', NULL);


INSERT INTO `players` (`id`, `player_id`, `date_of_birth`, `weight`, `age`, `category`, `country`, `gender`, `mobile`, `email`, `title`, `airlines`, `flight_number`, `arrival_date`, `arrival_time`, `departure_date`, `created_at`, `updated_at`, `deleted_at`, `avatar`, `passport_avatar`) VALUES
	(1, 4, '1995-10-27', 23.00, 24, 'Junior Cadet (8-10yr)', 'Belgium', 'Male', '01737864642', 'nikhil.lu210@gmail.com', 'Athlete', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '5:08 PM', '2019-09-16', '2019-09-16 08:18:12', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(2, 5, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Belarus', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(3, 6, '2019-09-16', 21.00, 9, 'Junior Cadet (8-10yr)', 'Argentina', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(4, 7, '2019-09-16', 21.00, 9, 'Junior Cadet (8-10yr)', 'Belgium', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(5, 8, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Albania', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(6, 9, '2019-09-16', 24.00, 9, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(7, 10, '2019-09-16', 25.00, 9, 'Junior Cadet (8-10yr)', 'Bolivia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:56', NULL, NULL, NULL),
	(8, 11, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Algeria', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(9, 12, '2019-09-16', 24.00, 9, 'Junior Cadet (8-10yr)', 'Brazil', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(10, 13, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Austria', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(11, 14, '2019-09-16', 24.00, 8, 'Junior Cadet (8-10yr)', 'Argentina', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(12, 15, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Anguilla', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(13, 16, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Antarctica', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(14, 17, '2019-09-16', 23.00, 8, 'Junior Cadet (8-10yr)', 'Argentina', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(15, 18, '2019-09-16', 25.00, 9, 'Junior Cadet (8-10yr)', 'Botswana', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(16, 19, '2019-09-16', 23.00, 10, 'Junior Cadet (8-10yr)', 'Bouvet Island', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(17, 20, '2019-09-16', 24.00, 8, 'Junior Cadet (8-10yr)', 'Bosnia and Herzegowina', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(18, 21, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Barbados', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(19, 22, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Bahamas', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(20, 23, '2019-09-16', 25.00, 8, 'Junior Cadet (8-10yr)', 'Belgium', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(21, 24, '2019-09-16', 23.00, 9, 'Junior Cadet (8-10yr)', 'Bouvet Island', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(22, 25, '2019-09-16', 25.00, 9, 'Junior Cadet (8-10yr)', 'Antigua and Barbuda', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(23, 26, '2019-09-16', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bolivia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(24, 27, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Angola', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(25, 28, '2019-09-16', 24.00, 9, 'Junior Cadet (8-10yr)', 'Brazil', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(26, 29, '2019-09-16', 23.00, 8, 'Junior Cadet (8-10yr)', 'Armenia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(27, 30, '2019-09-16', 24.00, 9, 'Junior Cadet (8-10yr)', 'Barbados', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(28, 31, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Aruba', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(29, 32, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Bosnia and Herzegowina', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(30, 33, '2019-09-16', 22.00, 8, 'Junior Cadet (8-10yr)', 'Argentina', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(31, 34, '2019-09-16', 23.00, 10, 'Junior Cadet (8-10yr)', 'Armenia', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(32, 35, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Bosnia and Herzegowina', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(33, 36, '2019-09-16', 21.00, 8, 'Junior Cadet (8-10yr)', 'Anguilla', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(34, 37, '2019-09-16', 23.00, 8, 'Junior Cadet (8-10yr)', 'Bahrain', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(35, 38, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Belize', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(36, 39, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Andorra', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(37, 40, '2019-09-16', 25.00, 8, 'Junior Cadet (8-10yr)', 'Armenia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:13', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(38, 41, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Bahamas', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(39, 42, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Argentina', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(40, 43, '2019-09-16', 22.00, 8, 'Junior Cadet (8-10yr)', 'Belize', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(41, 44, '2019-09-16', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bhutan', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(42, 45, '2019-09-16', 21.00, 8, 'Junior Cadet (8-10yr)', 'Albania', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(43, 46, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Andorra', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(44, 47, '2019-09-16', 24.00, 8, 'Junior Cadet (8-10yr)', 'Australia', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(45, 48, '2019-09-16', 22.00, 10, 'Junior Cadet (8-10yr)', 'Armenia', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(46, 49, '2019-09-16', 22.00, 8, 'Junior Cadet (8-10yr)', 'Antarctica', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(47, 50, '2019-09-16', 23.00, 8, 'Junior Cadet (8-10yr)', 'Botswana', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(48, 51, '2019-09-16', 25.00, 8, 'Junior Cadet (8-10yr)', 'Bolivia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(49, 52, '2019-09-16', 22.00, 10, 'Junior Cadet (8-10yr)', 'Albania', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(50, 53, '2019-09-16', 22.00, 8, 'Junior Cadet (8-10yr)', 'Andorra', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(51, 54, '2019-09-16', 21.00, 8, 'Junior Cadet (8-10yr)', 'Australia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(52, 55, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(53, 56, '2019-09-16', 22.00, 9, 'Junior Cadet (8-10yr)', 'Bahrain', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(54, 57, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Belize', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(55, 58, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:57', NULL, NULL, NULL),
	(56, 59, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Azerbaijan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(57, 60, '2019-09-16', 23.00, 10, 'Junior Cadet (8-10yr)', 'Bouvet Island', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(58, 61, '2019-09-16', 23.00, 9, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(59, 62, '2019-09-16', 21.00, 9, 'Junior Cadet (8-10yr)', 'Bermuda', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(60, 63, '2019-09-16', 23.00, 10, 'Junior Cadet (8-10yr)', 'Bahamas', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(61, 64, '2019-09-16', 21.00, 9, 'Junior Cadet (8-10yr)', 'Belgium', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(62, 65, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Albania', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(63, 66, '2019-09-16', 23.00, 9, 'Junior Cadet (8-10yr)', 'Antigua and Barbuda', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(64, 67, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Armenia', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(65, 68, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Australia', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(66, 69, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Austria', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(67, 70, '2019-09-16', 21.00, 8, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(68, 71, '2019-09-16', 21.00, 8, 'Junior Cadet (8-10yr)', 'Belize', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(69, 72, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Bahrain', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(70, 73, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(71, 74, '2019-09-16', 24.00, 10, 'Junior Cadet (8-10yr)', 'Benin', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:14', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(72, 75, '2019-09-16', 23.00, 10, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(73, 76, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Bolivia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(74, 77, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Azerbaijan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(75, 78, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Argentina', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(76, 79, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Bosnia and Herzegowina', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(77, 80, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Bahamas', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(78, 81, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Armenia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(79, 82, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Bouvet Island', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(80, 83, '2019-09-16', 21.00, 9, 'Junior Cadet (8-10yr)', 'Bahrain', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(81, 84, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Anguilla', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(82, 85, '2019-09-16', 25.00, 8, 'Junior Cadet (8-10yr)', 'Bolivia', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(83, 86, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Belgium', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(84, 87, '2019-09-16', 23.00, 8, 'Junior Cadet (8-10yr)', 'Antigua and Barbuda', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(85, 88, '2019-09-16', 22.00, 10, 'Junior Cadet (8-10yr)', 'Andorra', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(86, 89, '2019-09-16', 22.00, 10, 'Junior Cadet (8-10yr)', 'Antarctica', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(87, 90, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'Barbados', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(88, 91, '2019-09-16', 20.00, 10, 'Junior Cadet (8-10yr)', 'Belize', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(89, 92, '2019-09-16', 24.00, 10, 'Junior Cadet (8-10yr)', 'Austria', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(90, 93, '2019-09-16', 24.00, 8, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(91, 94, '2019-09-16', 22.00, 10, 'Junior Cadet (8-10yr)', 'Afghanistan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(92, 95, '2019-09-16', 23.00, 9, 'Junior Cadet (8-10yr)', 'Algeria', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(93, 96, '2019-09-16', 24.00, 8, 'Junior Cadet (8-10yr)', 'Bolivia', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(94, 97, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Bosnia and Herzegowina', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(95, 98, '2019-09-16', 23.00, 9, 'Junior Cadet (8-10yr)', 'Botswana', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(96, 99, '2019-09-16', 21.00, 10, 'Junior Cadet (8-10yr)', 'American Samoa', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(97, 100, '2019-09-16', 24.00, 8, 'Junior Cadet (8-10yr)', 'Bangladesh', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(98, 101, '2019-09-16', 25.00, 9, 'Junior Cadet (8-10yr)', 'Belarus', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(99, 102, '2019-09-16', 25.00, 10, 'Junior Cadet (8-10yr)', 'Bhutan', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(100, 103, '2019-09-16', 20.00, 8, 'Junior Cadet (8-10yr)', 'Bhutan', 'Female', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL),
	(101, 104, '2019-09-16', 20.00, 9, 'Junior Cadet (8-10yr)', 'Angola', 'Male', '01737864642', NULL, 'Athelet', '6541546sgsdgd', '6541546sgsdgd', '2019-09-16', '05:08 PM', '2019-09-16', '2019-09-16 08:18:15', '2019-09-16 08:33:58', NULL, NULL, NULL);


INSERT INTO `tournaments` (`id`, `name`, `player_number`, `category`, `weight`, `starting_date`, `gender`, `max_round`, `manager_id`, `status`, `set_matches`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES (1, 'Demo Testing Tournament 01', 11, 'Junior Cadet (8-10yr)', 20.00, '2019-09-17', 'Male', 4, 2, -1, 1, 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Ciceros De Finibus Bonorum et Malorum for use in a type specimen book.', '2019-09-16 08:36:09', '2019-09-16 08:39:12', NULL);


INSERT INTO `assigned_editors` (`id`, `editor_id`, `tournament_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 3, 1, '2019-09-16 08:36:09', '2019-09-16 08:36:09', NULL);



INSERT INTO `assigned_players` (`id`, `tournament_id`, `player_id`, `editor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 13, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(2, 1, 97, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(3, 1, 42, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(4, 1, 21, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(5, 1, 22, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(6, 1, 46, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(7, 1, 81, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(8, 1, 72, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(9, 1, 27, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(10, 1, 58, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(11, 1, 86, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL);


INSERT INTO `matches` (`id`, `tournament_id`, `player_one`, `player_two`, `match_number`, `round_number`, `round_name`, `match_status`, `match_date`, `match_time`, `match_venue`, `winner`, `looser`, `editor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 13, 97, 1, 1, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(2, 1, 42, 21, 2, 1, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:11', '2019-09-16 08:39:11', NULL),
	(3, 1, 22, 46, 3, 1, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(4, 1, 81, 72, 4, 1, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(5, 1, 27, 58, 5, 1, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(6, 1, 86, NULL, 6, 2, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(7, 1, NULL, NULL, 7, 2, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(8, 1, NULL, NULL, 8, 2, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(9, 1, NULL, NULL, 9, 3, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(10, 1, NULL, NULL, 10, 3, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL),
	(11, 1, NULL, NULL, 11, 4, NULL, -1, '2019-09-17', '2:45 PM', 'Demo Stadium', NULL, NULL, 3, '2019-09-16 08:39:12', '2019-09-16 08:39:12', NULL);


INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_100000_create_password_resets_table', 1),
	(2, '2019_07_30_133808_create_roles_table', 1),
	(3, '2019_07_31_000000_create_users_table', 1),
	(4, '2019_08_03_093541_create_players_table', 1),
	(5, '2019_08_03_093654_create_tournaments_table', 1),
	(6, '2019_08_03_094149_create_assigned_editors_table', 1),
	(7, '2019_08_03_094327_create_assigned_players_table', 1),
	(8, '2019_08_03_094442_create_matches_table', 1);
