<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'role_id'   => 4,
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'approved_at' => now(),
        'password' => bcrypt('12345678'),
        // 'password' => '$10$aaQrjq1PrIg9kno7hZhiIOh0hYfEGpgG783W6K/YCw/TlkHQJq9vW', // 12345678
        'remember_token' => Str::random(10),
    ];
});


$factory->define(App\Models\Player\Player::class, function (Faker $faker) {
    $gender = $faker->randomElement(['Male', 'Female']);
    $country = $faker->randomElement(['Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia and Herzegowina', 'Botswana', 'Bouvet Island', 'Brazil']);
    return [
        'player_id' => 4,
        'date_of_birth' => date('Y-m-d'),
        'age'   => rand(8,10),
        'country' => $country,
        'gender' => $gender,
        'category' => 'Junior Cadet (8-10yr)',
        'weight' => rand(20,50),
        'mobile' => '01737864642',
        'title' => 'Athelet',
        'airlines' => '6541546sgsdgd',
        'flight_number' => '6541546sgsdgd',
        'arrival_date' => date('Y-m-d'),
        'arrival_time' => '05:08 PM',
        'departure_date' => date('Y-m-d'),
    ];
});
