<?php

// Customer Routes
Route::group([
    'as' => 'editor.',
    'namespace' => 'Editor',
    'prefix' => 'editor',
],
    function(){
        include_once 'dashboard/dashboard.php';
        include_once 'tournament/tournament.php';
        include_once 'profile/profile.php';
    }
);
