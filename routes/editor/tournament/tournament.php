<?php

// Tournaments Routes
Route::group([
    'prefix' => '/tournaments', //url
    'as' => 'tournament.', //route
],
    function(){
        // Ongoing Tournaments
        Route::get('/ongoing', 'TournamentController@ongoing_index')->name('ongoing.index');
        Route::get('/ongoing/show/{id}', 'TournamentController@ongoing_show')->name('ongoing.show');
        Route::post('/ongoing/update/{id}', 'TournamentController@ongoing_update')->name('ongoing.update');

        // upcoming Tournaments
        Route::get('/upcoming', 'TournamentController@upcoming_index')->name('upcoming.index');
        Route::get('/upcoming/show/{id}', 'TournamentController@upcoming_show')->name('upcoming.show');
        Route::post('/upcoming/store/{id}', 'TournamentController@upcoming_store')->name('upcoming.store');
        Route::get('/upcoming/edit/{id}', 'TournamentController@upcoming_edit')->name('upcoming.edit');
        Route::post('/upcoming/update/{id}', 'TournamentController@upcoming_update')->name('upcoming.update');

        // completed Tournaments
        Route::get('/completed', 'TournamentController@completed_index')->name('completed.index');
        Route::get('/completed/show/{id}', 'TournamentController@completed_show')->name('completed.show');
    }
);
