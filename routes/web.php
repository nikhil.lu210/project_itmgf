<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::post('/loginwithuser', 'Auth\LoginController@loginWithEmailOrUsername')->name('loginWithUser');


/*===================================
========< Super Admin Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('/graph/{id}', 'GraphController@graph')->name('graph');
        Route::get('/graph/json/{id}', 'GraphController@graphjson')->name('graphjson');
    }
);

/*===================================
========< Super Admin Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'super_admin'],
    ],
    function () {
        include_once 'superadmin/all.php';
    }
);


/*===================================
========< manager Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'manager'],
    ],
    function () {
        include_once 'manager/all.php';
    }
);


/*===================================
========< editor Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'editor'],
    ],
    function () {
        include_once 'editor/all.php';
    }
);
/*===================================
========< player Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'player'],
    ],
    function () {
        include_once 'player/all.php';
    }
);

