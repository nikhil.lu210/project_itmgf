<?php

// Create Editor
Route::group([
    'prefix' => '/editor', //url
    'as' => 'editor.', //route
],
    function(){
        // All Editor Page
        Route::get('/all', 'EditorController@index')->name('index');

        // Create Editor Page
        Route::get('/create', 'EditorController@create')->name('create');

        // Show Editor Page
        Route::get('/all/show/{id}', 'EditorController@show')->name('show');

        //assign new editor
        Route::post('/all/assign', 'EditorController@store')->name('assign');
    }
);
