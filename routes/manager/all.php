<?php

// Customer Routes
Route::group([
    'as' => 'manager.',
    'namespace' => 'Manager',
    'prefix' => 'manager',
],
    function(){
        include_once 'profile/profile.php';
        include_once 'dashboard/dashboard.php';
        include_once 'tournament/tournament.php';
        include_once 'editor/editor.php';
        include_once 'player/player.php';
    }
);
