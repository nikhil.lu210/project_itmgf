<?php

// Tournaments Routes
Route::group([
    'prefix' => '/tournaments', //url
    'as' => 'tournament.', //route
],
    function(){
        // Ongoing Tournaments
        Route::get('/ongoing', 'TournamentController@ongoing_index')->name('ongoing.index');
        Route::get('/ongoing/show/{id}', 'TournamentController@ongoing_show')->name('ongoing.show');
        Route::get('/ongoing/details/{id}', 'TournamentController@ongoing_details')->name('ongoing.details');

        // all Tournaments
        Route::get('/all', 'TournamentController@all_index')->name('all.index');
        Route::get('/all/show/{id}', 'TournamentController@all_show')->name('all.show');
        Route::get('/all/details/{id}', 'TournamentController@all_details')->name('all.details');

        // Tournament Create
        Route::get('/create', 'TournamentController@create')->name('create');
        Route::post('/store', 'TournamentController@store')->name('store');


        //take availavle player
        Route::get('/availavleplayer/{gender}/{category}/{weight}', 'TournamentController@availavlePlayer')->name('availavleplayer');
    }
);
