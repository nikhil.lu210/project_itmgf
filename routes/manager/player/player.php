<?php

// Players
Route::group([
    'prefix' => '/player', //url
    'as' => 'player.', //route
],
    function(){
        // Approved
        Route::get('/approved_players', 'PlayerController@index')->name('index');
        Route::get('/approved_players/{id}', 'PlayerController@show')->name('show');

        // Non-Approved
        Route::get('/non_approved_players', 'PlayerController@need_approval')->name('need_approval');
        Route::get('/non_approved_players/{id}', 'PlayerController@need_approval_show')->name('need_approval_show');
        Route::get('/non_approved_players/approve/{id}', 'PlayerController@approve')->name('approved');
        Route::get('/non_approved_players/decline/{id}', 'PlayerController@decline')->name('decline');
    }
);
