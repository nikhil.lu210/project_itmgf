<?php

// Manager Profile
Route::group([
    'prefix' => '/profile', //url
    'as' => 'profile.', //route
],
    function(){
        Route::get('', 'ProfileController@index')->name('index');
        Route::get('/password_change', 'ProfileController@password_change')->name('password');

        // Password Changing
        Route::post('/change_password', 'ProfileController@change_password')->name('password.change');
    }
);
