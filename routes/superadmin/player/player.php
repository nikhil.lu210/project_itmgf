<?php

// Player Routes
Route::group([
    'prefix' => '/player', //url
    'as' => 'player.', //route
],
    function(){
        // player index
        Route::get('/player', 'PlayerController@player_index')->name('index');
        // player show
        Route::get('/player/show/{id}', 'PlayerController@player_show')->name('show');
        // player show
        Route::get('/player/destroy/{id}', 'PlayerController@player_destroy')->name('destroy');
    }
);
