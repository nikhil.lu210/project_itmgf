<?php

// Manager Profile
Route::group([
    'prefix' => '/profile', //url
    'as' => 'profile.', //route
],
    function(){
        // Profile Index
        Route::get('', 'ProfileController@index')->name('index');

        // Password Change
        Route::get('/password_change', 'ProfileController@password_change')->name('password');

        // Password Changing
        Route::post('/change_password', 'ProfileController@change_password')->name('password.change');
    }
);
