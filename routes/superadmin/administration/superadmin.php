<?php

// superadmin Routes
Route::group([
    'prefix' => '/administration', //url
    'as' => 'administration.', //route
],
    function(){
        // Super Admin index
        Route::get('/superadmin', 'AdministrationController@superadmin_index')->name('superadmin.index');
        // Super Admin show
        Route::get('/superadmin/show/{id}', 'AdministrationController@superadmin_show')->name('superadmin.show');
        // Super Admin destroy
        Route::get('/superadmin/destroy/{id}', 'AdministrationController@superadmin_destroy')->name('superadmin.destroy');
    }
);
