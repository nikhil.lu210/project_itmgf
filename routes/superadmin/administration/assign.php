<?php

// administration assign
Route::group([
    'prefix' => '/administration', //url
    'as' => 'administration.', //route
],
    function(){
        // Super Admin index
        Route::get('/create', 'AdministrationController@create')->name('create');

        //assign new member
        Route::post('/assign', 'AdministrationController@store')->name('store');
    }
);
