<?php

// manager Routes
Route::group([
    'prefix' => '/administration', //url
    'as' => 'administration.', //route
],
    function(){
        // Super Admin index
        Route::get('/manager', 'AdministrationController@manager_index')->name('manager.index');
        // Super Admin show
        Route::get('/manager/show/{id}', 'AdministrationController@manager_show')->name('manager.show');
        // Manager destroy
        Route::get('/manager/destroy/{id}', 'AdministrationController@manager_destroy')->name('manager.destroy');
    }
);
