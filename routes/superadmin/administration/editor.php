<?php

// editor Routes
Route::group([
    'prefix' => '/administration', //url
    'as' => 'administration.', //route
],
    function(){
        // Super Admin index
        Route::get('/editor', 'AdministrationController@editor_index')->name('editor.index');
        // Super Admin show
        Route::get('/editor/show/{id}', 'AdministrationController@editor_show')->name('editor.show');
        // Editor destroy
        Route::get('/editor/destroy/{id}', 'AdministrationController@editor_destroy')->name('editor.destroy');
    }
);
