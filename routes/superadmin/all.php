<?php

// Customer Routes
Route::group([
    'as' => 'superadmin.',
    'namespace' => 'SuperAdmin',
    'prefix' => 'superadmin',
],
    function(){
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';

        // Administration Assion
        include_once 'administration/assign.php';

        // Administration
        include_once 'administration/superadmin.php';
        include_once 'administration/manager.php';
        include_once 'administration/editor.php';

        // Tournament
        include_once 'tournament/tournament.php';

        // Player
        include_once 'player/player.php';
    }
);
