<?php

// Tournament Routes
Route::group([
    'prefix' => '/tournament', //url
    'as' => 'tournament.', //route
],
    function(){
        // tournament index
        Route::get('/tournament', 'TournamentController@tournament_index')->name('index');
        // tournament show
        Route::get('/tournament/show/{id}', 'TournamentController@tournament_show')->name('show');
        // tournament details
        Route::get('/tournament/details/{id}', 'TournamentController@tournament_details')->name('details');
    }
);
