<?php

// Customer Routes
Route::group([
    'as' => 'player.',
    'namespace' => 'Player',
    'prefix' => 'player',
    'middleware' => ['approved'],
],
    function(){
        include_once 'dashboard/dashboard.php';
        include_once 'tournament/tournament.php';
        include_once 'match/match.php';
    }
);

Route::group([
    'as' => 'player.',
    'namespace' => 'Player',
    'prefix' => 'player',
],
    function(){
        include_once 'profile/profile.php';
    }
);
