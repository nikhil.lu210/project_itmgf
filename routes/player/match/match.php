<?php

// Matches Routes
Route::group([
    'prefix' => '/matches', //url
    'as' => 'match.', //route
],
    function(){
        // Upcoming Matches
        Route::get('/upcoming', 'MatchController@upcoming_index')->name('upcoming.index');
        Route::get('/upcoming/show/{id}', 'MatchController@upcoming_show')->name('upcoming.show');

        // My Matches
        Route::get('/my', 'MatchController@my_index')->name('my.index');
        Route::get('/my/show/{id}', 'MatchController@my_show')->name('my.show');
    }
);
