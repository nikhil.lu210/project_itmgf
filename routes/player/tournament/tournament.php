<?php

// Tournaments Routes
Route::group([
    'prefix' => '/tournaments', //url
    'as' => 'tournament.', //route
],
    function(){
        // Ongoing Tournaments
        Route::get('/ongoing', 'TournamentController@ongoing_index')->name('ongoing.index');
        Route::get('/ongoing/show/{id}', 'TournamentController@ongoing_show')->name('ongoing.show');

        // Upcoming Tournaments
        Route::get('/upcoming', 'TournamentController@upcoming_index')->name('upcoming.index');
        Route::get('/upcoming/show/{id}', 'TournamentController@upcoming_show')->name('upcoming.show');

        // My Tournaments
        Route::get('/my', 'TournamentController@my_index')->name('my.index');
        Route::get('/my/show/{id}', 'TournamentController@my_show')->name('my.show');
    }
);
