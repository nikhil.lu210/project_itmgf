<?php

// Player Routes
Route::group([
    'prefix' => '/profile', //url
    'as' => 'profile.', //route
],
    function(){
        Route::get('', 'ProfileController@index')->name('index');
        Route::get('/show', 'ProfileController@show')->name('show');
        Route::post('/store', 'ProfileController@store')->name('create');
        Route::post('/update', 'ProfileController@update')->name('update');

        // Password Change
        Route::get('/password_change', 'ProfileController@password_change')->name('password');

        // Password Changing
        Route::post('/change_password', 'ProfileController@change_password')->name('password.change');
    }
);
