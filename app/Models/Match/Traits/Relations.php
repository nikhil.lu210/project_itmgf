<?php

namespace App\Models\Match\Traits;

// Model
use App\User;

trait Relations
{
    //relation with tournament one to many relationship
    public function tournament(){
        return $this->belongsTo('App\Models\Tournament\Tournament', 'tournament_id', 'id');
    }


    //relation with playerOne one to many relationship
    public function playerOne(){
        return $this->belongsTo('App\User', 'player_one', 'id');
    }


    //relation with playerTwo one to many relationship
    public function playerTwo(){
        return $this->belongsTo('App\User', 'player_two', 'id');
    }

    //relation with matchEditor one to many relationship
    public function matchEditor(){
        return $this->belongsTo('App\User', 'editor_id', 'id');
    }


}
