<?php

namespace App\Models\Match;

use Illuminate\Database\Eloquent\Model;

// Traits and softdelete
use App\Models\Match\Traits\Relations;
use App\Models\Match\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Match extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
