<?php

namespace App\Models\Role;

use Illuminate\Database\Eloquent\Model;

// Traits and softdelete
use App\Models\Role\Traits\Relations;
use App\Models\Role\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


}
