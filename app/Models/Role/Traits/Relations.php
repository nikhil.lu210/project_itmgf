<?php

namespace App\Models\Role\Traits;

// Model
use App\User;

trait Relations
{
    public function users()
    {
        return $this->hasMany('App\User', 'role_id', 'id');
    }
}
