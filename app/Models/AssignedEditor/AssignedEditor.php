<?php

namespace App\Models\AssignedEditor;

use Illuminate\Database\Eloquent\Model;

// Traits and softdelete
use App\Models\AssignedEditor\Traits\Relations;
use App\Models\AssignedEditor\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedEditor extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
