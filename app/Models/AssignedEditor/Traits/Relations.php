<?php

namespace App\Models\AssignedEditor\Traits;

// Model
use App\User;

trait Relations
{
    //relation with editor one to many relationship
    public function editor(){
        return $this->belongsTo('App\User', 'editor_id', 'id');
    }

    //relation with manager one to many relationship
    public function tournament(){
        return $this->belongsTo('App\Models\Tournament\Tournament', 'tournament_id', 'id');
    }
}
