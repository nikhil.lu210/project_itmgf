<?php

namespace App\Models\Player;

use Illuminate\Database\Eloquent\Model;

// Traits and softdelete
use App\Models\Player\Traits\Relations;
use App\Models\Player\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
