<?php

namespace App\Models\Player\Traits;

// Model
use App\User;

trait Relations
{
    public function player()
    {
        return $this->hasOne('App\User', 'player_id', 'id');
    }
}
