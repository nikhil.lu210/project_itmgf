<?php

namespace App\Models\AssignedPlayer\Traits;

// Model
use App\User;

trait Relations
{
    //relation with tournament one to many relationship
    public function tournament(){
        return $this->belongsTo('App\Models\Tournament\Tournament', 'tournament_id', 'id');
    }


    //relation with player one to many relationship
    public function assignedPlayer(){
        return $this->belongsTo('App\User', 'player_id', 'id');
    }

    //relation with assignedEditor one to many relationship
    public function assignedEditor(){
        return $this->belongsTo('App\User', 'editor_id', 'id');
    }




}
