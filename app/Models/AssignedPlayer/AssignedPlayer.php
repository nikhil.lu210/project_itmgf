<?php

namespace App\Models\AssignedPlayer;

use Illuminate\Database\Eloquent\Model;

// Traits and softdelete
use App\Models\AssignedPlayer\Traits\Relations;
use App\Models\AssignedPlayer\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedPlayer extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
