<?php

namespace App\Models\Tournament;

use Illuminate\Database\Eloquent\Model;

// Traits and softdelete
use App\Models\Tournament\Traits\Relations;
use App\Models\Tournament\Traits\Scopes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tournament extends Model
{
    // Use Relations and Scopes
    use Relations, Scopes, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
