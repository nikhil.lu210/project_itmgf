<?php

namespace App\Models\Tournament\Traits;

// Model
use App\User;

trait Relations
{
    //relation with manager one to many relationship
    public function manager(){
        return $this->belongsTo('App\User', 'manager_id', 'id');
    }

    //relation with AssignedEditor one to many relationship
    public function assignedEditors(){
        return $this->hasMany('App\Models\AssignedEditor\AssignedEditor', 'tournament_id', 'id');
    }

    //relation with AssignedPlayer one to many relationship
    public function assignedPlayers(){
        return $this->hasMany('App\Models\AssignedPlayer\AssignedPlayer', 'tournament_id', 'id');
    }


    //relation with matches one to many relationship
    public function matches(){
        return $this->hasMany('App\Models\Match\Match', 'tournament_id', 'id');
    }

}
