<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssignedEditor\AssignedEditor;
use App\Models\Tournament\Tournament;
use Auth;

class DashboardController extends Controller
{
    public function __construct(){
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignedTournamets = AssignedEditor::where('editor_id', Auth::user()->id)->get();
        $all = sizeof($assignedTournamets);
        $completed = 0;
        $upcoming = 0;
        $ongoing = 0;
        foreach($assignedTournamets as $tt){
            $tournament = Tournament::find($tt->tournament_id);
            if($tournament->status == 1) $completed++;
            elseif($tournament->status == -1){
                if($tournament->starting_date > date('Y-m-d')) $upcoming++;
                else $ongoing++;
            }
        }
        return view('editor.dashboard.index', compact(['all', 'completed', 'upcoming', 'ongoing']));
    }
}
