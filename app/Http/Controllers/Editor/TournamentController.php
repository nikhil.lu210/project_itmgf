<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use App\Models\AssignedPlayer\AssignedPlayer;
use App\Models\Match\Match;

use Carbon\Carbon;

class TournamentController extends Controller
{
    public function __construct()
    {

    }

    /**
     * ===================================================
     * =============< Ongoing Tournaments >===============
     * ===================================================
     */
    // Ongoing Index
    public function ongoing_index()
    {
        $tournament = Tournament::with(['assignedEditors' => function($q){
            $q->where('editor_id', Auth::user()->id)->get();
        }])->where('status', -1)->where('set_matches', 1)->where('starting_date','<=',date('Y-m-d'))->orderBy('starting_date', 'DESC')->first();

        if($tournament){
            $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $tournament->id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');

            $lastUpdate = Match::where('tournament_id', $tournament->id)->where('match_status', 1)->orderBy('match_number', 'DESC')->first();
            if($lastUpdate == null) $lastUpdate = 0;
            else $lastUpdate = $lastUpdate->match_number;
        } else{
            $matches = null;
            $lastUpdate = null;
        }

        return view('editor.tournament.ongoing.index', compact(['tournament', 'matches','lastUpdate']));
    }

    // Ongoing Show
    public function ongoing_show($id)
    {
        $match= Match::with(['playerOne.player', 'playerTwo.player'])->find($id);
        return view('editor.tournament.ongoing.show', compact('match'));
    }

    public function ongoing_update(Request $request, $id)
    {
        $match = Match::find($id);

        $match->match_status = 1;
        $match->editor_id = Auth::user()->id;

        if($match->player_one == $request->winner){
            $match->winner = $request->winner;
            $match->looser = $match->player_two;
        } else {
            $match->winner = $request->winner;
            $match->looser = $match->player_one;
        }

        $match->save();

        $tournament = Tournament::find($match->tournament_id);

        if($match->round_number < $tournament->max_round){
            $roundUp = null;
            $matches = Match::where('tournament_id',$tournament->id)->get();
            
            foreach($matches as $mm){
                if($mm->player_one == null) { $roundUp = $mm; break; }
                if ($mm->player_two == null) { $roundUp = $mm; break; }
            }
            // $roundUp = Match::where('match_number', '>', $match->match_number)->where('tournament_id',$tournament->id)->where('player_one', null)->where('player_two', null)->first();

            if($roundUp->player_one == null) $roundUp->player_one = $match->winner;
            else $roundUp->player_two = $match->winner;

            $roundUp->save();

        }

        if($match->round_number == $tournament->max_round){
            $tournament->status = 1;
            $tournament->save();
        }

        return redirect()->route('editor.tournament.ongoing.index')->withMessage('Match Result Updated Successfully');

    }





    /**
     * ===================================================
     * =============< upcoming Tournaments >===============
     * ===================================================
     */
    // upcoming Index
    public function upcoming_index()
    {
        $tournaments = Tournament::with(['assignedEditors' => function($q){
            $q->where('editor_id', Auth::user()->id)->get();
        }])->where('status', -1)->where('starting_date','>',date('Y-m-d'))->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });

        return view('editor.tournament.upcoming.index', compact('tournaments'));
    }

    // upcoming Show
    public function upcoming_show($id)
    {
        $tournament = Tournament::find($id);

        $players = Player::where('category', $tournament->category)->where('weight',$tournament->weight)->where('gender',$tournament->gender)->get();

        $roundMatches = array();
        $roundNumber = $tournament->max_round;
        $n = $tournament->player_number;
        for($i = 1; $i <= $roundNumber; $i++){
            $nn = (int) ($n/2);
            $n /=2;
            $roundMatches[] = $nn;
            if($n > $nn) $n++;
        }
        if($tournament->set_matches == 1){
            $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');
            // dd($matches);
            return view('editor.tournament.upcoming.show', compact(['roundMatches', 'players','tournament', 'matches']));
        }
        return view('editor.tournament.upcoming.show', compact(['roundMatches', 'players','tournament']));
    }

    public function upcoming_store(Request $request, $id)
    {
        $players = $request->players;

        shuffle($players);

        // dd($players);

        $tournament = Tournament::find($id);
        $matchNo = sizeof($request->match_date);
        if(sizeof($players) != $tournament->player_number) return redirect()->back()->withMessage('Did not select player perfectly');

        // dd($players);

        $this->validate($request, [
            'match_date'    => 'required | array',
            'match_date.*'  => 'required | date | date_format:Y-m-d',
            'match_time'    => 'required | array',
            // 'match_time.*'  => 'required | date | string | max: 30',
            'match_venue'   => 'required | array',
            // 'match_venue.*' => 'required | string | max: 30',
        ]);


        for($i=0;$i<sizeof($players); $i++){
            $assignedPlayer = new AssignedPlayer();

            $assignedPlayer->tournament_id  = $id;
            $assignedPlayer->player_id      = $players[$i];
            $assignedPlayer->editor_id      = Auth::user()->id;

            $assignedPlayer->save();
        }


        

        $aa =0;
        for($i=1; $i<=$matchNo; $i++){

            $match = new Match();

            $match->tournament_id = $id;
            // dd($players);
            // if(sizeof($players) > 0){
            //     $rand1 = rand(0, sizeof($players)-1);
            //     $match->player_one = $players[$rand1];
            //     array_splice($players, $rand1, 1);
            // }
            // // dd($players);

            // if(sizeof($players) > 0){
            //     $rand2 = rand(0, sizeof($players)-1);
            //     $match->player_two = $players[$rand2];
            //     array_splice($players, $match->player_two, 1);
            // }
            // dd($players);

            if($aa < sizeof($players)){
                $match->player_one = $players[$aa];
                $aa++;
            }
            if($aa < sizeof($players)){
                $match->player_two = $players[$aa];
                $aa++;
            }

            $match->match_number = $i;
            $match->round_number = $request->roundNo[$i];
            // $match->round_name = $request->roundName;
            $match->match_status = -1;
            $match->match_date = $request->match_date[$i];
            $match->match_time = $request->match_time[$i];
            $match->match_venue = $request->match_venue[$i];
            $match->editor_id = Auth::user()->id;

            $match->save();
        }

        $tournament->set_matches = 1;
        $tournament->save();




        return redirect()->back()->withMessage('Assigned Player and set Match Successfully');

    }

    public function upcoming_edit($id){
        $match = Match::find($id);
        return view('editor.tournament.upcoming.edit', compact('match'));
    }

    public function upcoming_update(Request $request, $id)
    {
        $this->validate($request, [
            'match_date'  => 'required | date | date_format:Y-m-d',
            'match_venue' => 'required | string | max: 30',
        ]);

        $match = Match::find($id);
        $match->match_date = $request->match_date;
        $match->match_time = $request->match_time;
        $match->match_venue = $request->match_venue;

        $match->save();

        return redirect()->route('editor.tournament.upcoming.show',['id' => $match->tournament_id])->withMessage('Match Info Updated Successfully');
    }



    /**
     * ===================================================
     * =============< completed Tournaments >===============
     * ===================================================
     */
    // completed Index
    public function completed_index()
    {
        $tournaments = Tournament::with(['assignedEditors' => function($q){
            $q->where('editor_id', Auth::user()->id)->get();
        }])->where('status', 1)->where('starting_date','<=',date('Y-m-d'))->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });


        return view('editor.tournament.completed.index', compact('tournaments'));
    }

    // completed Show
    public function completed_show($id)
    {
        $tournament = Tournament::find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $tournament->id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');

        return view('editor.tournament.completed.show', compact(['matches', 'tournament']));
    }
}
