<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Hash;
use App\Model\AssignedEditor\AssignedEditor;
use Mail;

class EditorController extends Controller
{
    public function __construct()
    {

    }

    /**
     * ===================================================
     * =============< Editor Controlling >================
     * ===================================================
     */
    // View Editor
    public function index(){
        $editors = User::where('role_id', 3)->get();
        return view("manager.editor.index", compact('editors'));
    }

    // Editor Create Page
    public function create(){
        return view("manager.editor.create");
    }

    // Editor show Page
    public function show($id){
        $editor = User::with(['assignedTournamentEditors'])->find($id);
        return view("manager.editor.show", compact('editor'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'first_name'    => 'required | string | max:191',
            'last_name'     => 'required | string | max:191',
            'email'         => 'required | string | email | max:191 | unique:users',
            'username'      => 'required | string | max:191 | unique:users',
            'password'      => 'required | string | min:8 | confirmed',
        ]);

        $editor = new User();

        $editor->first_name     =   $request->first_name;
        $editor->last_name      =   $request->last_name;
        $editor->email          =   $request->email;
        $editor->mobile          =   $request->mobile;
        $editor->username       =   $request->username;
        $editor->password       =   Hash::make($request->password);
        $editor->role_id        =   3;

        $editor->save();

        //Account Information Mail Send To the Editor
        $mail_send_data = [
            'name' => $request->first_name. ' ' .$request->last_name,
            'email' => $request->email,
            'password' => $request->password,
        ];

        Mail::send('layouts.send_mail.editor_details', $mail_send_data, function($message) use ($mail_send_data) {
			$message->to($mail_send_data['email'], $mail_send_data['name']);
			$message->subject('Editor Login Details (ITMGF)');
			$message->from('veechimailengine@gmail.com', 'ITMGF');
	    });

        return redirect()->route('manager.editor.index');
    }
}
