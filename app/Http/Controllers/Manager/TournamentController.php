<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use App\Models\Match\Match;
use App\Models\AssignedEditor\AssignedEditor;
use App\Models\AssignedPlayer\AssignedPlayer;

use Carbon\Carbon;


class TournamentController extends Controller
{
    public function __construct()
    {

    }

    /**
     * ===================================================
     * =============< Ongoing Tournaments >===============
     * ===================================================
     */
    // Ongoing Index
    public function ongoing_index()
    {
        $tournaments = Tournament::where('status', -1)->where('starting_date','<=',date('Y-m-d'))->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });
        return view('manager.tournament.ongoing.index', compact('tournaments'));
    }

    // Ongoing Show
    public function ongoing_show($id)
    {
        // $tournament = Tournament::with(['matches' => function($query){
        //     $query->select('*')->get()->groupBy('round_number');
        // }])->find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');
        $tournament_id = $id;
        // dd($matches);
        return view('manager.tournament.ongoing.show', compact(['matches', 'tournament_id']));
    }

    // Ongoing details
    public function ongoing_details($id)
    {
        $tournament = Tournament::with(['manager', 'assignedEditors.editor' => function($q){
            $q->select('id','first_name','last_name', 'email', 'mobile');
        }, 'assignedPlayers.assignedPlayer.player'  => function($q){
            $q->select('*');
        }])->find($id);
        return view('manager.tournament.ongoing.details', compact('tournament'));
    }




    /**
     * ===================================================
     * =================< All Tournaments >===============
     * ===================================================
     */
    // all Index
    public function all_index()
    {
        $tournaments = Tournament::orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });

        return view('manager.tournament.all.index', compact('tournaments'));
    }

    // all Show
    public function all_show($id)
    {
        // $tournament = Tournament::with(['matches' => function($query){
        //     $query->select('*')->get()->groupBy('round_number');
        // }])->find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');
        $tournament_id = $id;
        return view('manager.tournament.all.show', compact(['matches','tournament_id']));
    }

    // all details
    public function all_details($id)
    {
        $tournament = Tournament::with(['manager', 'assignedEditors.editor' => function($q){
            $q->select('id','first_name','last_name', 'email', 'mobile');
        }, 'assignedPlayers.assignedPlayer.player'  => function($q){
            $q->select('*');
        }])->find($id);
        // dd($tournament);
        return view('manager.tournament.all.details', compact('tournament'));
    }


    /**
     * ===================================================
     * =============< Create Tournament >===============
     * ===================================================
     */
    // Tournamnet create
    public function create()
    {
        // $p = Player::all();
        // foreach($p as $pl){
        //     $pl->weight = rand(20,25);
        //     $pl->save();
        // }
        $editors = User::where('role_id', 3)->get();
        return view('manager.tournament.create', compact('editors'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      =>  'required | string | max: 191',
            'starting_date' =>  'required | date | date_format:Y-m-d',
            'gender'      =>  'required | string | max: 191',
            'category'      =>  'required | string',
            'weight'      =>  'required ',
            'editors' => 'required|array',
            'editors.*' => 'integer',
            'note'      =>  'required | string ',
        ]);

        $tournament = new Tournament();

        $tournament->name       =   $request->name;
        $tournament->category    =   $request->category;
        $tournament->weight    =   $request->weight;
        $tournament->gender     =   $request->gender;
        $tournament->starting_date = $request->starting_date;
        $tournament->player_number= $request->total_player;
        $tournament->manager_id =   Auth::user()->id;
        $tournament->note       =   $request->note;
        $round_no=1;
        while($request->total_player >= pow(2,$round_no)){
            if($request->total_player == pow(2,$round_no)) break;
            $round_no++;
        }
        $tournament->max_round  =   $round_no;

        $tournament->save();

        for($i =0; $i<sizeof($request->editors); $i++){
            $assignedEditor = new AssignedEditor();
            $assignedEditor->editor_id = $request->editors[$i];
            $assignedEditor->tournament_id = $tournament->id;
            $assignedEditor->save();
        }
        return redirect()->route('manager.tournament.all.index')->withMessage('Successfully Created a New Tournament');
    }


    //availavlePlayer
    public function availavlePlayer($gender, $category, $weight){
        $players = Player::where('category', $category)->where('weight',$weight)->where('gender',$gender)->get();

        $count =0;
        foreach($players as $player){
            $user = User::find($player->player_id);
            if($user->approved_at == null) continue;
            $count++;
        }

        return response($count);
    }
}
