<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manager.profile.index');
    }


    // Password Change
    public function password_change(){
        return view('manager.profile.password');
    }

    //Change Password
    public function change_password(Request $request){
        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'new_password'          => 'required | string | min:8 | same:confirm_password',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) )
            return redirect()->back()->withMessage('Your Old Password Does not Match.');
        elseif($request->old_password == $request->new_password)
            return redirect()->back()->withMessage('Old Password and New Password must be Different.');
        else{
            $super = Auth::user();

            $super->password = Hash::make($request->new_password);

            $super->save();

            return redirect()->back()->withMessage('Your Password has been Updated Succesfully.');
        }
    }
}
