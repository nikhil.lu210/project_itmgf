<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Player\Player;
use Mail;

class PlayerController extends Controller
{
    public function __construct(){

    }

    /**
     * ===================================================
     * ===============< Approved Player >=================
     * ===================================================
     */
    // View player
    public function index(){
        $players = User::with(['player'])->where('approved_at', '!=', null)->where('role_id', 4)->orderBy('id','asc')->get();
        return view("manager.player.index", compact('players'));
    }

    // player show Page
    public function show($id){
        $player = User::with('player')->find($id);
        return view("manager.player.show", compact('player'));
    }



    /**
     * ===================================================
     * =============< Non-Approved Player >===============
     * ===================================================
     */
    // player approve index
    public function need_approval(){
        $players = User::with('player')->whereNull('approved_at')->where('role_id', 4)->get();

        return view('manager.player.approval.index', compact('players'));
        // return view("manager.player.approval.index");
    }

    // player approve show
    public function need_approval_show($id){
        $player = User::with('player')->find($id);
        return view("manager.player.approval.show", compact('player'));
    }

    public function approve($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->update(['approved_at' => now()]);

        //Successful Message Send To the Player
        $mail_send_data = [
            'name' => $user->first_name. ' ' .$user->last_name,
            'email' => $user->email,
        ];

        Mail::send('layouts.send_mail.player_approval', $mail_send_data, function($message) use ($mail_send_data) {
			$message->to($mail_send_data['email'], $mail_send_data['name']);
			$message->subject('Approval Message (ITMGF)');
			$message->from('veechimailengine@gmail.com', 'ITMGF');
	    });


        return redirect()->route('manager.player.need_approval')->withMessage('User approved successfully');
    }


    public function decline($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->delete();

        $player = Player::where('player_id', $user_id)->first();
        if($player) {
            $player->delete();
        }

        //Rejection Message Send To the Player
        $mail_send_data = [
            'name' => $user->first_name. ' ' .$user->last_name,
            'email' => $user->email,
        ];

        Mail::send('layouts.send_mail.player_reject', $mail_send_data, function($message) use ($mail_send_data) {
			$message->to($mail_send_data['email'], $mail_send_data['name']);
			$message->subject('Player Rejection Message (ITMGF)');
			$message->from('veechimailengine@gmail.com', 'ITMGF');
	    });

        return redirect()->route('manager.player.need_approval')->withMessage('User Decline successfully');
    }
}
