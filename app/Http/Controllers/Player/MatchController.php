<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use App\Models\AssignedPlayer\AssignedPlayer;
use App\Models\Match\Match;
use Carbon\Carbon;

class MatchController extends Controller
{
    public function __construct(){
    }

    /**
     * ===================================================
     * =============< Upcoming Matches >==================
     * ===================================================
     */
    // Upcoming Index
    public function upcoming_index()
    {
        $matches = Match::with('tournament')->where('match_status', -1)->where('player_one', Auth::user()->id)->orWhere('player_two', Auth::user()->id)->orderBy('id', 'ASC')->get()->groupBy('tournament_id');

        return view('player.match.upcoming.index', compact('matches'));
    }

    // Upcoming Show
    public function upcoming_show($id)
    {
        return view('player.match.upcoming.show');
    }



    /**
     * ===================================================
     * =================< My Matches >====================
     * ===================================================
     */
    // My Index
    public function my_index()
    {
        $matches = Match::with('tournament')->where('player_one', Auth::user()->id)->orWhere('player_two', Auth::user()->id)->orderBy('id', 'ASC')->get()->groupBy('tournament_id');

        return view('player.match.my.index', compact('matches'));
    }

    // My Show
    public function my_show($id)
    {
        return view('player.match.my.show');
    }
}
