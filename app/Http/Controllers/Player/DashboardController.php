<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use App\Models\AssignedPlayer\AssignedPlayer;
use App\Models\Match\Match;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __construct(){
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prtiT = AssignedPlayer::where('player_id', Auth::user()->id)->count();
        $prtiM = Match::where('player_one', Auth::user()->id)->orWhere('player_two', Auth::user()->id)->count();
        $winM = Match::where('winner', Auth::user()->id)->count();
        $tourW = 0;

        $abc = Match::where('winner', Auth::user()->id)->get();
        foreach($abc as $ab){
            $t = Tournament::find($ab->tournament_id);
            if($t->max_round == $ab->round_number) $tourW++;
        }

        return view('player.dashboard.index', compact(['prtiT', 'prtiM', 'winM', 'tourW']));
    }
}
