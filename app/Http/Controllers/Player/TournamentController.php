<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use App\Models\AssignedPlayer\AssignedPlayer;
use App\Models\Match\Match;
use Carbon\Carbon;


class TournamentController extends Controller
{
    public function __construct(){
    }

    /**
     * ===================================================
     * =============< Ongoing Tournaments >===============
     * ===================================================
     */
    // Ongoing Index
    public function ongoing_index()
    {
        $tournaments = Tournament::where('status', -1)->where('set_matches', 1)->where('starting_date','<=',date('Y-m-d'))->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });

        return view('player.tournament.ongoing.index', compact('tournaments'));
    }

    // Ongoing Show
    public function ongoing_show($id)
    {
        $tournament = Tournament::find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');

        return view('player.tournament.ongoing.show', compact(['tournament', 'matches']));
    }




    /**
     * ===================================================
     * =============< Upcoming Tournaments >==============
     * ===================================================
     */
    // Upcoming Index
    public function upcoming_index()
    {
        $tournaments = Tournament::where('status', -1)->where('set_matches', 1)->where('starting_date','>',date('Y-m-d'))->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });

        return view('player.tournament.upcoming.index', compact('tournaments'));
    }

    // Upcoming Show
    public function upcoming_show($id)
    {
        $tournament = Tournament::find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');

        return view('player.tournament.upcoming.show', compact(['tournament', 'matches']));
    }




    /**
     * ===================================================
     * ================< My Tournaments >=================
     * ===================================================
     */
    // My Index
    public function my_index()
    {
        $tournaments = Tournament::with(['assignedPlayers' => function($q){
            $q->where('player_id', Auth::user()->id)->get();
        }])->where('status', -1)->where('set_matches', 1)->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });

        return view('player.tournament.my.index', compact('tournaments'));
    }

    // My Show
    public function my_show($id)
    {
        $tournament = Tournament::find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');

        return view('player.tournament.my.show', compact(['tournament', 'matches']));
    }
}
