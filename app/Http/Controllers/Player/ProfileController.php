<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\User;
use Auth;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $player = Player::where('player_id', Auth::user()->id)->first();

        $tournaments = Tournament::with(['assignedPlayers' => function($q){
            $q->where('player_id', Auth::user()->id)->get();
        }])->where('status', -1)->orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });

        return view('player.profile.index', compact(['player', 'tournaments']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'b_date'           =>      'required | date | date_format:Y-m-d',
            'country'          =>      'required | string | max:25',
            'weight'           =>      'required ',
            'gender'           =>      'required | string | max:10',
            'category'         =>      'required | string | max:50',
            'number'           =>      'required | string | max:20',
            'title'            =>      'required | string | max:50',
            'airline'          =>      'required | string | max:191',
            'flight_no'        =>      'required | string | max:191',
            'arrival_date'     =>      'required | date | date_format:Y-m-d',
            'arrival_date'     =>      'required | string | max:15',
            'departure_date'   =>      'required | date | date_format:Y-m-d',
        ]);

        if($request->email){
            $this->validate($request, [
                'email'     =>  'string | email | max:191'
            ]);
        }
        $player = new Player();

        if($request->hasFile('avatar')){
            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $player->avatar = $encoded_image;
        }

        if($request->hasFile('passport_avatar')){
            $image_data=file_get_contents($request->passport_avatar);

            $encoded_image=base64_encode($image_data);

            $player->passport_avatar = $encoded_image;
        }

        $player->player_id      =   Auth::user()->id;
        $player->date_of_birth  =   $request->b_date;
        $player->country        =   $request->country;
        $player->gender         =   $request->gender;
        $player->weight         =   $request->weight;
        $player->category         =   $request->category;
        $bod = date_create($request->b_date);
        $player->age            =  date('Y') - date_format($bod,"Y");
        $player->mobile         =   $request->number;
        if($request->email) $player->email = $request->email;
        $player->title          =   $request->title;
        $player->airlines       =   $request->airline;
        $player->flight_number  =   $request->flight_no;
        $player->arrival_date   =   $request->arrival_date;
        $player->arrival_time   =   $request->arrival_time;
        $player->departure_date =   $request->departure_date;

        $player->save();

        return redirect()->back()->withMessage('your Profile Created successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // $p = Player::all();
        // $i = 4;
        // foreach($p as $pl){
        //     $pl->player_id = $i;
        //     $pl->save();
        //     $i++;
        // }

        $player = Auth::user();
        $status = 1;
        $profile = Player::where('player_id', $player->id)->first();

        if($profile == null){
            $status = 0;
            $profile = Player::find($player->id);
            return view('player.profile.show', compact('player', 'status'));
        }

        return view('player.profile.show', compact('player', 'status', 'profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'b_date'           =>      'required | date | date_format:Y-m-d',
            // 'country'          =>      'required | string | max:25',
            'gender'           =>      'required | string | max:10',
            'number'           =>      'required | string | max:20',
            'title'            =>      'required | string | max:50',
            'airline'          =>      'required | string | max:191',
            'flight_no'        =>      'required | string | max:191',
            'arrival_date'     =>      'required | date | date_format:Y-m-d',
            'arrival_date'     =>      'required | string | max:15',
            'departure_date'   =>      'required | date | date_format:Y-m-d',
        ]);

        if($request->email){
            $this->validate($request, [
                'email'     =>  'string | email | max:191'
            ]);
        }
        $player = Player::where('player_id',Auth::user()->id)->first();

        if($request->hasFile('avatar')){
            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $player->avatar = $encoded_image;
        }

        if($request->hasFile('passport_avatar')){
            $image_data=file_get_contents($request->passport_avatar);

            $encoded_image=base64_encode($image_data);

            $player->passport_avatar = $encoded_image;
        }

        $player->player_id      =   Auth::user()->id;
        $player->date_of_birth  =   $request->b_date;
        // $player->country        =   $request->country;
        $player->gender         =   $request->gender;
        if($request->weight) $player->weight         =   $request->weight;
        if($request->category) $player->category         =   $request->category;
        $bod = date_create($request->b_date);
        $player->age            =  date('Y') - date_format($bod,"Y");
        $player->mobile         =   $request->number;
        if($request->email) $player->email = $request->email;
        $player->title          =   $request->title;
        $player->airlines       =   $request->airline;
        $player->flight_number  =   $request->flight_no;
        $player->arrival_date   =   $request->arrival_date;
        $player->arrival_time   =   $request->arrival_time;
        $player->departure_date =   $request->departure_date;

        $player->save();

        return redirect()->back()->withMessage('your updated Created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // Password Cahnge View Page
    public function password_change(){
        return view('player.profile.password');
    }



    //Change Password
    public function change_password(Request $request){
        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'new_password'          => 'required | string | min:8 | same:confirm_password',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) )
            return redirect()->back()->withMessage('Your Old Password Does not Match.');
        elseif($request->old_password == $request->new_password)
            return redirect()->back()->withMessage('Old Password and New Password must be Different.');
        else{
            $super = Auth::user();

            $super->password = Hash::make($request->new_password);

            $super->save();

            return redirect()->back()->withMessage('Your Password has been Updated Succesfully.');
        }
    }
}
