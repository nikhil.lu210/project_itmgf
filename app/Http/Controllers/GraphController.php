<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tournament\Tournament;
use App\Models\Match\Match;



class GraphController extends Controller
{
    public function graph($id){
        $tournament = Tournament::find($id);
        return view('tournament_graph.graph', compact('tournament'));
    }

    public function graphjson($id){
        $tournament = Tournament::find($id);

        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');

        return response()->json(['matches' => $matches, 'tournament' => $tournament]);
    }
}
