<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Models\Player\Player;
use App\Models\Tournament\Tournament;
use App\Models\Match\Match;
use App\Models\AssignedEditor\AssignedEditor;
use App\Models\AssignedPlayer\AssignedPlayer;

use Carbon\Carbon;

class TournamentController extends Controller
{
    public function __construct(){

    }

    /**
     * ===================================================
     * =============< Tournaments Controling >============
     * ===================================================
     */
    // All Tournaments Index
    public function tournament_index()
    {
        $tournaments = Tournament::orderBy('starting_date', 'ASC')->get()->groupBy(function($val) {
            return Carbon::parse($val->starting_date)->format('M Y');
        });
        return view('superadmin.tournament.index', compact('tournaments'));
    }
    // Tournaments Show
    public function tournament_show($id)
    {
        $matches = Match::with(['playerOne.player', 'playerTwo.player'])->where('tournament_id', $id)->orderBy('match_number', 'ASC')->get()->groupBy('round_number');
        $tournament = Tournament::find($id);
        return view('superadmin.tournament.show', compact(['matches','tournament']));
    }
    // Tournaments details
    public function tournament_details($id)
    {
        $tournament = Tournament::with(['manager', 'assignedEditors.editor' => function($q){
            $q->select('id','first_name','last_name', 'email', 'mobile');
        }, 'assignedPlayers.assignedPlayer.player'  => function($q){
            $q->select('*');
        }])->find($id);
        return view('superadmin.tournament.details', compact('tournament'));
    }
}
