<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Hash;
use Auth;
use Mail;

class AdministrationController extends Controller
{
    public function __construct(){

    }

    /**
     * ===================================================
     * =============< Administration Assign >=============
     * ===================================================
     */
    // Administration Creation
    public function create()
    {
        return view('superadmin.administration.create');
    }
    // Administration Creation
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'    => 'required | string | max:191',
            'last_name'     => 'required | string | max:191',
            'email'         => 'required | string | email | max:191 | unique:users',
            'username'      => 'required | string | max:191 | unique:users',
            'password'      => 'required | string | min:8 | confirmed',
        ]);

        $member = new User();

        $member->first_name     =   $request->first_name;
        $member->last_name      =   $request->last_name;
        $member->role_id        =   $request->role;
        $member->username       =   $request->username;
        $member->email          =   $request->email;
        $member->mobile         =   $request->mobile;
        $member->password       =   Hash::make($request->password);

        $member->save();

        //Account Information Mail Send To the User
        $mail_send_data = [
            'name' => $request->first_name. ' ' .$request->last_name,
            'email' => $request->email,
            'password' => $request->password,
        ];

        Mail::send('layouts.send_mail.administration', $mail_send_data, function($message) use ($mail_send_data) {
			$message->to($mail_send_data['email'], $mail_send_data['name']);
			$message->subject('Login Details (ITMGF)');
			$message->from('veechimailengine@gmail.com', 'ITMGF');
	    });

        return redirect()->back()->withMessage('Successfully Assigned a New Member.');
    }





    /* ======================<| Super Admin |>==================== */
    // All Super Admin Index
    public function superadmin_index()
    {
        $superadmins = User::where('role_id', 1)->orderBy('id', 'DESC')->get();
        return view('superadmin.administration.superadmin.index', compact('superadmins'));
    }

    // Super Admin Show
    public function superadmin_show($id)
    {
        $superadmin = User::find($id);
        return view('superadmin.administration.superadmin.show', compact('superadmin'));
    }

    // Super Admin Show
    public function superadmin_destroy($id)
    {
        $superadmin = User::find($id);
        if($superadmin->id == Auth::user()->id){
            return redirect()->back()->withMessage('You Can Not Delete Yourself.');
        }else{
            $superadmin->delete();
            return redirect()->back()->withMessage('You Have Deleted A Super Admin.');
        }
    }






    /* ======================<| Manager |>==================== */
    // All Manager Index
    public function manager_index()
    {
        $managers = User::where('role_id', 2)->orderBy('id', 'DESC')->get();
        return view('superadmin.administration.manager.index', compact('managers'));
    }

    // Manager Show
    public function manager_show($id)
    {
        $manager = User::find($id);
        return view('superadmin.administration.manager.show', compact('manager'));
    }

    // Manager Destroy
    public function manager_destroy($id)
    {
        $manager = User::find($id);
        $manager->delete();
        return redirect()->back()->withMessage('You Have Deleted A Manager.');
    }







    /* ======================<| Editor |>==================== */
    // All Editor Index
    public function editor_index()
    {
        $editors = User::where('role_id', 3)->orderBy('id', 'DESC')->get();
        return view('superadmin.administration.editor.index', compact('editors'));
    }

    // Editor Show
    public function editor_show($id)
    {
        $editor = User::find($id);
        return view('superadmin.administration.editor.show', compact('editor'));
    }

    // Editor Destroy
    public function editor_destroy($id)
    {
        $editor = User::find($id);
        $editor->delete();
        return redirect()->back()->withMessage('You Have Deleted an Editor.');
    }
}
