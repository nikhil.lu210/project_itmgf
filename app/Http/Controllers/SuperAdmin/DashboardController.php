<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tournament\Tournament;
use Auth;
use App\User;

class DashboardController extends Controller
{
    public function __construct(){
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total = Tournament::all()->count();

        $ongoing = Tournament::where('status', -1)->where('starting_date','<=',date('Y-m-d'))->count();

        $upcoming = Tournament::where('status', -1)->where('starting_date','>',date('Y-m-d'))->count();

        $superadmin = User::where('role_id', 1)->count();
        
        return view('superadmin.dashboard.index', compact(['total', 'ongoing', 'upcoming', 'superadmin' ]));
    }

}
