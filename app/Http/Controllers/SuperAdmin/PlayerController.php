<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class PlayerController extends Controller
{
    public function __construct(){

    }

    /**
     * ===================================================
     * ====================< Players >====================
     * ===================================================
     */
    // All Players Index
    public function player_index()
    {
        $players = User::with(['player'])->where('approved_at', '!=', null)->where('role_id', 4)->get();
        return view('superadmin.player.index', compact('players'));
    }

    // Players Show
    public function player_show($id)
    {
        $player = User::with('player')->find($id);
        return view('superadmin.player.show', compact('player'));
    }
}
