<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'approved_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    //relation with role one to many relationship
    public function role(){
        return $this->belongsTo('App\Models\Role\Role', 'role_id', 'id');
    }

    //relation with playar one to one relation
    public function player(){
        return $this->hasOne('App\Models\Player\Player', 'player_id', 'id');
    }

    //relation with tournament one to many relationship
    public function tournaments(){
        return $this->hasMany('App\Models\Tournament\Tournament', 'manager_id', 'id');
    }

    //relation with AssignedEditor one to many relationship
    public function assignedTournamentEditors(){
        return $this->hasMany('App\Models\AssignedEditor\AssignedEditor', 'editor_id', 'id');
    }

    //relation with AssignedPlayer one to many relationship
    public function assignedPlayers(){
        return $this->hasMany('App\Models\AssignedPlayer\AssignedPlayer', 'player_id', 'id');
    }

    //relation with assignedPlayerEditors one to many relationship
    public function assignedPlayerEditors(){
        return $this->hasMany('App\Models\AssignedPlayer\AssignedPlayer', 'editor_id', 'id');
    }


    //relation with playerOnes one to many relationship
    public function playerOnes(){
        return $this->hasMany('App\Models\Match\Match', 'player_one', 'id');
    }

    //relation with playerTwos one to many relationship
    public function playerTwos(){
        return $this->hasMany('App\Models\Match\Match', 'player_two', 'id');
    }

    //relation with matchEditors one to many relationship
    public function matchEditors(){
        return $this->hasMany('App\Models\Match\Match', 'editor_id', 'id');
    }

}
